<?php
	get_header();
	$searchQuery = $_POST['donorname'];
	if($searchQuery){
		$table_name = 'wp_wbp_donors_data';
		$sql = 'SELECT id, body_first_name, body_last_name, body_status, unic_id FROM '.$table_name.' WHERE body_first_name LIKE "'.$searchQuery.'%" OR  body_last_name LIKE "'.$searchQuery.'%" ';
		$results = $wpdb->get_results($sql, ARRAY_A);
	}

?>

	<div class="wrapper5 page-search-registered">
		<?php wbpGetSmallHeaderBlock(); ?>
        <div class="block_list">
   			<div class="list_head">
            	<div class="list_h_left"></div>
                <div class="list_h_center list_text1">SEARCH REGISTERED</div>
                <div class="list_h_right"></div>
            </div><div class="clear"></div>
			<div class="list_border">
				<div class="form-container">
					<form method="post">
						<div class="db_search_form">
							<div>
								<input class="db_search s_text ui-autocomplete-input" type="text" name="donorname" value="<?php echo $searchQuery?>" placeholder="Search" />
							</div>
							<div>
								<input class="db_subm" type="submit" value=""/>
							</div>
						</div>
					</form>
				</div>

				<?php if($results): ?>
				<?php foreach( $results as $v ): ?>
					<div class="list_sep"></div>
					<div class="list_line">
						<div class="list_line_name list_text2">
							<a class="db_text2" href="<?php bloginfo('url'); ?>/edit?donor_id=<?php echo $v['id']; ?>&tab=donor_information">
								<?php echo $v['body_last_name'].', '.$v['body_first_name']; ?>
							</a>
						</div>
						<?php echo ( $v['unic_id'] > 0 ? '<div class="list_line_id list_text3">ID '.$v['unic_id'].'</div>' : '' ); ?>
					</div>
				<?php endforeach; ?>
				<?php endif; ?>
			</div>
    	</div>
    </div>
<?php get_footer(); ?>