<?php
/*
 * Template Name: Export PDF
 */

$filename = "report_".$_SESSION['report_name'].".pdf";

// Dompdf
require_once("dompdf_config.inc.php");
// $old_limit = ini_set("memory_limit", "192M"); 
$html = '<html><body>';
$html .= $_SESSION['html'];
$html .= '</body></html>';
$dompdf = new DOMPDF();
$dompdf->load_html($html);
$dompdf->render();
$dompdf->stream($filename);
?>