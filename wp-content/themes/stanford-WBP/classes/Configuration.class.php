<?php
/**
 * Theme Configuration Class
 */
class Configuration {

	/**
	 * Constructor, if it's ran dynamically
	 */
	public function __construct() {
		// Initialization general settings
		$this->init();
	}
	
	/**
	 * General Settings
	 */
	public function init() {
		// WP Thumbnail Support
		add_theme_support('post-thumbnails');
		//set_post_thumbnail_size(200, 200);
		
		// Initialize Custom WP Menus
		register_nav_menus();
		
		// EDIT TEXT - Disables WordPress automatic paragraph formatting (add / remove tags <P> and <BR />)
		remove_filter('the_content', 'wpautop');
		remove_filter('comment_text', 'wpautop');
		
		if (is_admin()) {
			// Settings
			add_action('admin_menu', array(__CLASS__, 'backend_page_metaboxes'));		// Configuration of Meta Boxes for PAGE type
			
			// Layout Settings
			add_action('admin_menu', array(__CLASS__, 'backend_remove_menu_items'));		// Remove Menu Items
			//add_action('admin_menu', array(__CLASS__, 'backend_remove_submenus'));		// Remove SubMenu Items
			//add_action('admin_head', array(__CLASS__, 'backend_custom_logo'));			// Set Custom Logo
			add_filter('admin_footer_text', array(__CLASS__, 'backend_footer'));			// Set Custom Footer
		} else {
			
			// Remove not necessary links from the HEAD
			remove_action('wp_head', 'rsd_link');
			remove_action('wp_head', 'feed_links');
			remove_action('wp_head', 'feed_links_extra');
			remove_action('wp_head', 'wlwmanifest_link');
			remove_action('wp_head', 'index_rel_link');
			remove_action('wp_head', 'wp_generator');
			remove_action('wp_head', 'wp_shortlink_wp_head');
			remove_action('wp_head', 'start_post_rel_link');
			remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');

			// Front-End Layout Settings
			add_filter('show_admin_bar', '__return_false');	// Disable admin bar on front-end

			// Login Form
			add_action('login_head', array(__CLASS__, 'backend_login_logo'));			// Set Custom Logo on the Login Form
			add_filter('login_headerurl', array(__CLASS__, 'backend_login_url'));
			add_filter('login_headertitle', array(__CLASS__, 'backend_login_title'));	// Custom Login Screen
		}
	}
	
	/**
	 * Adds / Removes extra meta-boxes for PAGE type
	 */
	public function backend_page_metaboxes() {
		//remove_meta_box('commentstatusdiv', 'page', 'normal');	// Remove Comments
		//remove_meta_box('revisionsdiv', 'page', 'normal');		// Remove Revisions
		
		add_meta_box('postexcerpt', __('Excerpt'), 'post_excerpt_meta_box', 'page', 'normal', 'core');	// Add Excerpt
	}

	/**
	 * Removes unnecessary menu items in the Back-End
	 */
	public function backend_remove_menu_items() {
		// Menu
		global $menu;
		$restricted = array(
			//__('Posts'),
			__('Links'),
			__('Comments'),
			//__('Media'),
			//__('Plugins'),
			__('Tools'),
			//__('Users')
		);
		end($menu);
		while (prev($menu)) {
			$value = explode(' ', $menu[key($menu)][0]);
			if(in_array($value[0] != NULL?$value[0] : "" , $restricted)) {
				unset($menu[key($menu)]);
			}
		}
	}
	
	/**
	 * Removes unnecessary submenu items in the Back-End
	 */
	function backend_remove_submenus() {
		global $submenu;
		unset($submenu['index.php'][10]); // Removes 'Updates'.
		unset($submenu['themes.php'][5]); // Removes 'Themes'.
		unset($submenu['options-general.php'][15]); // Removes 'Writing'.
		unset($submenu['options-general.php'][25]); // Removes 'Discussion'.
		unset($submenu['edit.php'][16]); // Removes 'Tags'.  
	}

	/**
	 * Sets Custom Logo in the Back-End
	 */
	public static function backend_custom_logo() {
		echo '	<style type="text/css">
			#header-logo { background-image: url('. get_bloginfo('template_directory') .'/images/site.admin.logo.png) !important; }
		</style>';
	}

	/**
	 * Sets Custom Logo on the Login Form in the Back-End
	 */
	public static function backend_login_logo() {
		echo '	<style type="text/css">
			#login h1 a { background-image: url('. get_bloginfo('template_directory') .'/images/site.logo.login.png) !important; }
		</style>';
	}

	public static function backend_login_url() {
		echo bloginfo('url');
	}
	
	public static function backend_login_title() {
		echo get_option('blogname');
	}

	/**
	 * Sets Custom Footer in the Back-End
	 */
	public function backend_footer () {
		echo '&copy; 2011'. ( date('Y')>2011 ? '-'.date('Y') : '' ) .' &ndash; '. get_bloginfo('name');
	}
	
}



	/*
	 * Init Configuration
	 */
//Configuration::init();