<?php
/*
 * Template Name: Data
 */
 
wp_enqueue_style('wbp_style_formstyler'); 

get_header();

$n=1;

      
?>


	<div class="wrapper3">
    	
	    <div class="h_bd3">
	    
		<div style="height:73px">
		    <div class="header_left"><a href="<?php bloginfo('url'); ?>"></a></div>
		    <div class="header_right">
			<a name="header"><div class="right_bd"></div></a>
			<div class="header_nav">
			    <div class="nav_back">
				<a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>></a>
			    </div>
			    <div class="nav_home"><a href="<?php bloginfo('url'); ?>"></a></div>
			</div>
		    </div>
		    <div class="header_center"><?php echo strtoupper(get_bloginfo('name')); ?></div>
		</div><div class="clear"></div>
	    
<!--  table title -->       
		<div class="db_title">
		    <table cellpadding="0" cellspacing="0" border="0">
			<thead>
			    <tr>
				<td id="db_table_name" class="db_table_name"><span class="db_text1">LAST, FIRST</span></td>
				<td id="db_table_id" class="db_table_id"><span class="db_text1">ID</span></td>
				<td id="db_table_status" class="db_table_status"><span class="db_text1">BODY STATUS</span></td>
			    </tr>
			</thead>
		    </table>
		</div>
<!-- /table title  -->

	    </div><!-- END div class="h_bd3"-->
	    <div class="header"></div><div class="clear"></div>
	
        <div class="block_database_db">
   <!-- left-->
        	<div class="bd_left">
            	<div class="side_top"></div>
                <div class="side_mid">
					<div>
						<a class="nf_a" href="<?php bloginfo('url'); ?>/edit">
							<input class="nf" type="button" value="NEW FILE" />
						</a>
					</div>

<script type='text/javascript'>

$(document).ready(function(){
//autocomplete

	$.widget( "custom.catcomplete", $.ui.autocomplete, {
		_renderItem: function( ul, item) 

{
			 $( "<li><\/li>" )
		.data( "item.autocomplete", item )
			.append( $( "<a><\/a>" ).html( item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + this.term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1<\/strong>") ) )
			.appendTo( ul );

		}
	});

});
//end document.ready

function pagclick (n,sort) {
		jQuery(document).ajaxStart(function () {
			jQuery("#loading").show();
		});
	jQuery.ajax({
		url: '<?php bloginfo('template_url'); ?>/donors_list2.php',
		cache: false,
		dataType: "json",
		data: { "n" : n , "sort" : sort },
		type: "post",
		success: function(dat){
			jQuery('tbody').empty();
			jQuery('tbody').append(dat.html);
			jQuery('#rec').empty();
			jQuery('#rec').append(dat.rec);
			jQuery('#tot').empty();
			jQuery('#tot').append(dat.tot);
			jQuery('#paging').empty();
			jQuery('#paging').append(dat.pg);
			jQuery('#stype').empty();
			jQuery('#stype').append(dat.stype);

                   scroll(0,0);

		}
	});

		jQuery(document).ajaxComplete(function () {
			jQuery("#loading").hide();
		});




}

function pagclick_sort (n,sort,tags) {

		jQuery(document).ajaxStart(function () {
			jQuery("#loading").show();
		});

	jQuery.ajax({
 	        url: '<?php bloginfo('template_url'); ?>/donors_sortby_list2.php',
		cache: false,
		dataType: "json",
		data: { "n" : n , "sort" : sort , "tags": tags},
		type: "post",
		success: function(dat){
			jQuery('tbody').empty();
			jQuery('tbody').append(dat.html);
			jQuery('#rec').empty();
			jQuery('#rec').append(dat.rec);
			jQuery('#tot').empty();
			jQuery('#tot').append(dat.tot);
			jQuery('#paging').empty();
			jQuery('#paging').append(dat.pg);
			jQuery('#stype').empty();
			jQuery('#stype').append(dat.stype);

                   scroll(0,0);

		}
	});

		jQuery(document).ajaxComplete(function () {
			jQuery("#loading").hide();
		});




}


$(function(){


	
	
	
	// Display donors list with default sort
	jQuery.ajax({
		url: '<?php bloginfo('template_url'); ?>/donors_list2.php',
		cache: false,
		dataType: "json",
		data: { "n" : '<?php echo $n; ?>', "sort" : 'AtoZ' },
		type: "post",
		success: function(dat){
			jQuery('tbody').append(dat.html);
			jQuery('#rec').empty();
			jQuery('#rec').append(dat.rec);
			jQuery('#tot').empty();
			jQuery('#tot').append(dat.tot);
			jQuery('#paging').empty();
			jQuery('#paging').append(dat.pg);
			jQuery('#stype').empty();
			jQuery('#stype').append(dat.stype);

		}
	});
	

	// Display donors list with selectable sorting
	$('#subm_sortby').on("click", function(){

		jQuery(document).ajaxStart(function () {
			jQuery("#loading").show();
		});

	    var sort = $('#sort_by').val();
	    var tags = $('#tags').val();


	    jQuery.ajax({
		    url: '<?php bloginfo('template_url'); ?>/donors_sortby_list2.php',
		    cache: false,
        	    dataType: "json",
  		    data: { "n":'<?php echo $n; ?>', "sort":sort, "tags": tags },
		    type: "post",
		    success: function(dat){
			jQuery('tbody').empty();
			jQuery('tbody').append(dat.html);
			jQuery('#rec').empty();
			jQuery('#rec').append(dat.rec);
			jQuery('#tot').empty();
			jQuery('#tot').append(dat.tot);
			jQuery('#paging').empty();
			jQuery('#paging').append(dat.pg);
			jQuery('#stype').empty();
			jQuery('#stype').append(dat.stype);
		    }
	    });
		jQuery(document).ajaxComplete(function () {
			jQuery("#loading").hide();
		});

	});



	//* Sort By change dropdown value
	$('#sort_by').on("change", function(){

		jQuery(document).ajaxStart(function () {
			jQuery("#loading").show();
		});

		var sort = $('#sort_by option:selected').val();

		jQuery.ajax({
			url: '<?php bloginfo('template_url'); ?>/donors_list2.php',
			cache: false,
          	    dataType: "json",
			data: { "n": '<?php echo $n; ?>', "sort":sort },
			type: "post",
			success: function(dat){
			jQuery('tbody').empty();
			jQuery('tbody').append(dat.html);
			jQuery('#rec').empty();
			jQuery('#rec').append(dat.rec);
			jQuery('#tot').empty();
			jQuery('#tot').append(dat.tot);
			jQuery('#paging').empty();
			jQuery('#paging').append(dat.pg);
			jQuery('#stype').empty();
			jQuery('#stype').append(dat.stype);
			}
		});
		jQuery(document).ajaxComplete(function () {
			jQuery("#loading").hide();
		});

	});


	
	
	// Positioning "div.abc_search"
	/*var wind_height = $(window).height();			// ÐÌÞÔ ×ÓÔ ÏÎ
	var div_height = $("div.abc_search").height();		// ÐÌÞÔ ×ÓÔ ÂÏÁ	var div_top = $("div.abc_search").offset().top;	// ÐÌÞÔ ÏÓÕ Ï ×ÒÁÏÎ Ä ×ÒÁÂÏÁ	var div_left = $("div.abc_search").offset().left;	// ÐÌÞÔ ÏÓÕ Ï Ì×Ç ËÁ Ä Ì×ÊÓÏÏÙÂÏÁ	
	//var common_height = div_top + div_height;		// ÏÝÑ×ÓÔ - ÏÓÕ+ÂÏ
	//var div_bottom = common_height - wind_height;		// ×ÓÔ Î ËÔ ÎÄ ÐÏÒÔÔ ÞÏÙÄÊÉÄ ÎÚ ÂÏÁ
	var tempScrollTop = 0
	var currentScrollTop = 0;
	
	$(window).scroll(function(){

	    currentScrollTop = $(window).scrollTop();
	    //alert(tempScrollTop+' - '+currentScrollTop);
	    if(tempScrollTop < currentScrollTop){
		tempScrollTop = currentScrollTop;
		$("div.abc_search").removeAttr('style');
		$("div.abc_search").attr({style:"position:fixed; bottom:10px; left:"+div_left+"px;"});
	    }else if(tempScrollTop > currentScrollTop){
		tempScrollTop = currentScrollTop;
		$("div.abc_search").removeAttr('style');
		$("div.abc_search").attr({style:"position:fixed; top:"+div_top+"px; left:"+div_left+"px;"});
	    }

	});//*/
	
	
});

</script>
<script>
function searchenter() {
                jQuery(document).ajaxStart(function () {
                        jQuery("#loading").show();
                });

            var sort = $('#sort_by').val();
            var tags = $('#tags').val();


            jQuery.ajax({
                    url: '<?php bloginfo('template_url'); ?>/donors_sortby_list2.php',
                    cache: false,
                    dataType: "json",
                    data: { "n":'<?php echo $n; ?>', "sort":sort, "tags": tags },
                    type: "post",
                    success: function(dat){
                        jQuery('tbody').empty();
                        jQuery('tbody').append(dat.html);
                        jQuery('#rec').empty();
                        jQuery('#rec').append(dat.rec);
                        jQuery('#tot').empty();
                        jQuery('#tot').append(dat.tot);
                        jQuery('#paging').empty();
                        jQuery('#paging').append(dat.pg);
                        jQuery('#stype').empty();
                        jQuery('#stype').append(dat.stype);
                    }
            });
                jQuery(document).ajaxComplete(function () {
                        jQuery("#loading").hide();
                });


}

</script>

					<form action="" method="post" id="searchf" onSubmit="searchenter();return false;">
						<input id="hidd_res" type="hidden" name="hidd_res" value="" />
						<div class="sel_sort">
							<select name="sort_by" id="sort_by">
								<option value="AtoZ">A to Z</option>
								<option value="ZtoA">Z to A</option>
								<option value="date_sign">Date signed up</option>
								<option value="age">Age</option>
								<option value="date_dirth">Date of Birth</option>
								<option value="gender">Gender</option>
								<option value="body_status">Body Status</option>
								<option value="id_num">ID Number</option>
							</select>
						</div>
						<div class="db_search_form">
							<div>
								<input id="tags" class="db_search s_text ui-autocomplete-input" type="text" name="search_val" value="" placeholder="Search" />
							</div>
							<div>
								<input id="subm_sortby" class="db_subm" type="button" value="" />
							</div>
						</div>
						<div class="clear"></div>
						
						<div class="search_res">
							<p><span class="sr_big_red" id=rec><?php echo count($results)-1; ?></span> records / <span class="sr_big" id=tot><?php echo $whole[0][total]; ?></span> total</p>
                                                        <p>&nbsp;</p>
                                                        <span><span class="sr_big">Search:</span> <span class="sr_big" id=stype></span></span>
						</div>
						
						<div class="clear"></div>
						<!--<div class="db_abc">
							<input type="button" value="Abc" />
						</div>-->
					</form>
                </div>
                <div class="side_down"></div>

            </div>
			
              
   <!-- ABC-->
	    <div class="abc_container">       
		<div class="abc_search">
		    <div class="abc_left">
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-a")' >a</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-b")' >b</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-c")' >c</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-d")' >d</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-e")' >e</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-f")' >f</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-g")' >g</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-h")' >h</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-i")' >i</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-j")' >j</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-k")' >k</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-l")' >l</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-m")' >m</a></div>
		     </div>
		     <div class="abc_separator"></div>
		     <div class="abc_right">
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-n")' >n</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-o")' >o</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-p")' >p</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-q")' >q</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-r")' >r</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-s")' >s</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-t")' >t</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-u")' >u</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-v")' >v</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-w")' >w</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-x")' >x</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-y")' >y</a></div>
			<div class="abc_letter"><a href='javascript:void(0)' onClick='pagclick (1,"char-z")' >z</a></div>
		     </div>
		</div> 
           </div>
              
   <!-- right-->
            <div class="bd_right">
            	
               <div class="db_border db_table9">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="sortable">
					 <!-- <thead>
						  <tr class="db_title">
						  <th id="db_table_name" class="db_table_name">
							  <span class="title_arrow_down db_text1">LAST, FIRST</span>
						  </th>
						  <th id="db_table_id" class="db_table_id ZtoA">
							  <span class="title_arrow_down db_text1">ID</span>
						  </th>
						  <th id="db_table_status" class="db_table_status AtoZ">
							  <span class="title_arrow_up db_text1 AtoZ">BODY STATUS</span>
						  </th>
						  </tr>
					  </thead>-->
					  
					   <tbody class="AtoZ"></tbody>

                  </table>
<div align=center id=paging>
</div>

                </div>
			
				
            </div><!-- end div class="bd_right"-->
        </div><!-- end div class="block_database"-->
	<div class="clear"></div>
	</div>

<?php get_footer(); ?>
