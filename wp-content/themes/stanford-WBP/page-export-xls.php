<?php

/*
 * Template Name: Query
 */

$_SESSION = array();
session_destroy();


$_POST['report'] = 'custom';
//print '<pre>';
//print_r ($_POST);
//exit;

function display_status($status) {
    if ($status == "registered") return "Registered";
    else if ($status == "withdrew") return "Withdrew";
    else if ($status == "accepted") return "Accepted";
    else if ($status == "rejected") return "Rejected";
    else if ($status == "reception") return "In Storage";
    else if ($status == "usage") return "In Use";
    else if ($status == "departed") return "Departed";
    else return "";
}

// Function to display the gender
function display_gender($gender) {
    if ($gender == "male") return "Male";
    else if ($gender == "female") return "Female";
    else return "Unknown";
}

function get_rows_bodies_in_fridge($total_results, $result) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;

    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;
        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['unic_id']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['reception']['body_cond']['body_loc']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['reception']['body_cond']['slot_numb']."</span></td>";
        $rows .= "</tr>";
    }
    $rows .= '</table>';
    return $rows;
}

function get_rows_cremated($total_results, $result) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;

    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;
        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['unic_id']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['final']['date']."</span></td>";
        $rows .= "</tr>";
    }
    $rows .= '</table>';
    return $rows;
}

function get_rows_embalmed($total_results, $result) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;
    $total = 0;
    
    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;
        $status = display_status($value['body_status']);
        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['unic_id']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$status."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['reception']['preparation']['total']."</span></td>";
        $rows .= "</tr>";
        $total += $value['reception']['preparation']['total'];
    }
    $rows .= "<tr class='query_table_row' style='margin-top:10px'>";
    $rows .= "<td class='query_table_block'><span class='query_text3'></span></td>";
    $rows .= "<td class='query_table_block'><span class='query_text3'>TOTAL</span></td>";
    $rows .= "<td class='query_table_block'><span class='query_text3'></span></td>";
    $rows .= "<td class='query_table_block'><span class='query_text3'></span></td>";
    $rows .= "<td class='query_table_block'><span class='query_text3'>".$total."</span></td>";
    $rows .= "</tr>";
    $rows .= '</table>';
    return $rows;
}

function get_rows_withdrew($total_results, $result) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;

    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;
        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['date_registration']."</span></td>";
        $rows .= "</tr>";
    }
    $rows .= '</table>';
    return $rows;
}

function get_rows_registered($total_results, $result) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;

    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;
        $status = display_status($value['body_status']);
        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$status."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['date_registration']."</span></td>";
        $rows .= "</tr>";
    }
    $rows .= '</table>';
    return $rows;
}

function get_rows_custom($total_results, $result, $customRows) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;

    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;

        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";

        foreach ($customRows as $index => $column) {
            if ($column == 'name') {
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
            }
            else if ($column == 'body_status') {
                $status = display_status($value['body_status']);
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>".$status."</span></td>";
            }
            else if ($column == 'gender') {
                $gender = display_gender($value['gender']);
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>".$gender."</span></td>";
            }
            else if ($column == 'address') {
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>".$value['address']['street'].", ".$value['address']['city'].", ".$value['address']['state'].", ".$value['address']['zip_code']."</span></td>";
            }


            else if ($column == 'phone') {
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>"."(".$value['address']['first_phone'].")".$value['address']['sec_phone']."-".$value['address']['third_phone']."</span></td>";
            }
            else if ($column == 'email') {
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>".$value['address']['email']."</span></td>";
            }
            else if ($column == 'preparation') {
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>".$value['reception']['preparation']['select']."</span></td>";
            }
            else if ($column == 'body_location') {
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>".$value['reception']['body_cond']['body_loc']."</span></td>";
            }
            else if ($column == 'date_of_cremation') {
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>".$value['final']['date']."</span></td>";

            }
            else if ($column == 'body_usages') {
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>".$value['usages']['res']."</pre></span></td>";

            }
            else if ($column == 'final_disposition') {
            $final = 'Cremated:'.($value['final']['cremated']? 'Yes, Date:'.$value['final']['date'].', Destination:'.$value['final']['cremains_destination'].', Notes:'.$value['final']['notes'] :'No');
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>".$final."</span></td>";

            }
            else {
                $rows .= "<td nowrap class='query_table_block'><span class='query_text2'>".$value[$column]."</span></td>";
            }
        }        
        $rows .= "</tr>";
    }
    $rows .= '</table>';
    return $rows;
}

function custom_header_name($name) {
    switch ($name) {
        case 'unic_id':
            return 'ID';
        case 'date_registration':
            return 'Registration Date';
        case 'body_status':
            return 'Status';
        case 'gender':
            return 'Gender';
        case 'date_of_death':
            return 'Date of Death';
        case 'cause_of_death':
            return 'Cause of Death';
        default:
            return $name;
    }
}

global $wpdb;
$table_data = 'wp_wbp_donors_data';

$sql = 'SELECT';
// $limit = 200;

// if (isset($_GET['page_number']) && $_GET['page_number']) {
//     $page = $_GET['page_number'];
// }
// else $page = 1;

if (isset($_POST['report']) && $_POST['report']) {
    switch($_POST['report']) {
        
        // Custom report
        case 'custom':
            $headerRow = '<td class="query_table_header"><span class="query_text1">No.</span></td>';
            $customRows = array();
            $exists = false;
            if (isset($_POST['custom_name']) && $_POST['custom_name']) {
                if ($exists) $sql .= ',';
                $sql .= ' body_last_name, body_first_name';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Name</span></td>';
                $customRows[] = 'name';
                $exists = true;
            }
            if (isset($_POST['custom_date_registration']) && $_POST['custom_date_registration']) {
                if ($exists) $sql .= ',';
                $sql .= ' date_registration';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Reg.&nbsp;Date</span></td>';
                $customRows[] = 'date_registration';
                $exists = true;
            }
            if (isset($_POST['custom_body_status']) && $_POST['custom_body_status']) {
                if ($exists) $sql .= ',';
                $sql .= ' body_status';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Status</span></td>';
                $customRows[] = 'body_status';
                $exists = true;
            }
            if (isset($_POST['custom_unic_id']) && $_POST['custom_unic_id']) {
                if ($exists) $sql .= ',';
                $sql .= ' unic_id';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">ID</span></td>';
                $customRows[] = 'unic_id';
                $exists = true;
            }
            if (isset($_POST['custom_gender']) && $_POST['custom_gender']) {
                if ($exists) $sql .= ',';
                $sql .= ' gender';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Gender</span></td>';
                $customRows[] = 'gender';
                $exists = true;
            }
            if (isset($_POST['custom_address']) && $_POST['custom_address']) {
                if ($exists) $sql .= ',';
                $sql .= ' address';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Address</span></td>';
                $customRows[] = 'address';
                $exists = true;
            }
            if (isset($_POST['custom_lives_beyond']) && $_POST['custom_lives_beyond']) {
                if ($exists) $sql .= ',';
                $sql .= ' lives_beyond';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Lives&nbsp;Beyond</span></td>';
                $customRows[] = 'lives_beyond';
                $exists = true;
            }
            if (isset($_POST['custom_agree_to_pay']) && $_POST['custom_agree_to_pay']) {
                if ($exists) $sql .= ',';
                $sql .= ' agree_to_pay';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Agree&nbsp;to&nbsp;Pay</span></td>';
                $customRows[] = 'agree_to_pay';
                $exists = true;
            }

            if (isset($_POST['custom_phone']) && $_POST['custom_phone']) {
                if ($exists) $sql .= ',';
                $sql .= ' address';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Phone</span></td>';
                $customRows[] = 'phone';
                $exists = true;
            }
            if (isset($_POST['custom_email']) && $_POST['custom_email']) {
                if ($exists) $sql .= ',';
                $sql .= ' address';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Email</span></td>';
                $customRows[] = 'email';
                $exists = true;
            }
            if (isset($_POST['custom_date_of_death']) && $_POST['custom_date_of_death']) {
                if ($exists) $sql .= ',';
                $sql .= ' date_of_death';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Date&nbsp;of&nbsp;Death</span></td>';
                $customRows[] = 'date_of_death';
                $exists = true;
            }
            if (isset($_POST['custom_cause_of_death']) && $_POST['custom_cause_of_death']) {
                if ($exists) $sql .= ',';
                $sql .= ' cause_of_death';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Cause&nbsp;of&nbsp;Date</span></td>';
                $customRows[] = 'cause_of_death';
                $exists = true;
            }


            if (isset($_POST['custom_date_of_birth']) && $_POST['custom_date_of_birth']) {
                if ($exists) $sql .= ',';
                $sql .= ' date_of_birth';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Date&nbsp;of&nbsp;Birth</span></td>';
                $customRows[] = 'date_of_birth';
                $exists = true;
            }

            if (isset($_POST['custom_sig_card_received']) && $_POST['custom_sig_card_received']) {
                if ($exists) $sql .= ',';
                $sql .= ' sig_card_received';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">RegForm&nbsp;Received</span></td>';
                $customRows[] = 'sig_card_received';
                $exists = true;
            }
            if (isset($_POST['custom_signature_date']) && $_POST['custom_signature_date']) {
                if ($exists) $sql .= ',';
                $sql .= ' signature_date';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Signature&nbsp;Date</span></td>';
                $customRows[] = 'signature_date';
                $exists = true;
            }
            if (isset($_POST['custom_how_about_us']) && $_POST['custom_how_about_us']) {
                if ($exists) $sql .= ',';
                $sql .= ' how_about_us';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">How&nbsp;hear&nbsp;about&nbsp;us</span></td>';
                $customRows[] = 'how_about_us';
                $exists = true;
            }

            if (isset($_POST['custom_appreciation_received']) && $_POST['custom_appreciation_received']) {
                if ($exists) $sql .= ',';
                $sql .= ' appreciation_received';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Appreciation&nbsp;Received</span></td>';
                $customRows[] = 'appreciation_received';
                $exists = true;
            }

            if (isset($_POST['custom_withdrew']) && $_POST['custom_withdrew']) {
                if ($exists) $sql .= ',';
                $sql .= ' withdrew';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Withdrew</span></td>';
                $customRows[] = 'withdrew';
                $exists = true;
            }

            if (isset($_POST['custom_withdrew_reason']) && $_POST['custom_withdrew_reason']) {
                if ($exists) $sql .= ',';
                $sql .= ' reason';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Withdrew&nbsp;Reason</span></td>';
                $customRows[] = 'reason';
                $exists = true;
            }

            if (isset($_POST['custom_contact_information']) && $_POST['custom_contact_information']) {
                if ($exists) $sql .= ',';
                $sql .= ' accept_optional_info';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Contact&nbsp;Information</span></td>';
                $customRows[] = 'accept_optional_info';
                $exists = true;
            }


            if (isset($_POST['custom_preparation']) && $_POST['custom_preparation']) {
                if ($exists) $sql .= ',';
                $sql .= ' reception';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Preparation</span></td>';
                $customRows[] = 'preparation';
                $exists = true;
            }
            if (isset($_POST['custom_body_location']) && $_POST['custom_body_location']) {
                if ($exists) $sql .= ',';
                $sql .= ' reception';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Body&nbsp;Location</span></td>';
                $customRows[] = 'body_location';
                $exists = true;
            }
            if (isset($_POST['custom_date_of_cremation']) && $_POST['custom_date_of_cremation']) {
                if ($exists) $sql .= ',';
                $sql .= ' final';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Date&nbsp;of&nbsp;Cremation</span></td>';
                $customRows[] = 'date_of_cremation';
                $exists = true;
            }

            if (isset($_POST['custom_usages']) && $_POST['custom_usages']) {
                if ($exists) $sql .= ',';
                $sql .= ' usages';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Usages</span></td>';
                $customRows[] = 'body_usages';
                $exists = true;
            }
            if (isset($_POST['custom_final']) && $_POST['custom_final']) {
                if ($exists) $sql .= ',';
                $sql .= ' final';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Final&nbsp;Disposition</span></td>';
                $customRows[] = 'final_disposition';
                $exists = true;
            }
            //print "$sql<br>";
            if ($sql == 'SELECT') {$sql='SELECT *';}
            $sql .= ' FROM '.$table_data;


            if (preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $_POST['filter_from']) )  {
            $from = explode('/',trim($_POST['filter_from']));
            $ffrom = $from[2].'-'.$from[0].'-'.$from[1].' 00:00:00';
            } else {
            $ffrom = '1914-01-01 00:00:00';
            }


            if (preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $_POST['filter_to']) )  {
            $to = explode('/',trim($_POST['filter_to']));
            $fto = $to[2].'-'.$to[0].'-'.$to[1].' 23:59:59';
            } else {
            $fto = date('Y-m-d').' 23:59:59';
            }

            $where = " where date_registration between '".$ffrom."' and '".$fto."' ";


            if (isset($_POST['filter_name']) && $_POST['filter_name']){
            $where .= " and (body_first_name like '%".$_POST['filter_name']."%' or body_last_name like '%".$_POST['filter_name']."%')"; 
            }

                $filter_body_status = $_POST['filter_body_status0'];
                if ($filter_body_status != 'all') {
                $bodys= ' and body_status in(';
                if (isset($_POST['filter_body_status']) && $_POST['filter_body_status'] ) {

                foreach ($_POST['filter_body_status'] as $f) {
                    $bodys .= "'".$f."',";
                  }
		}

                $bodys .= "'nn')";
                $where .= $bodys;
                }

                $filter_gender = $_POST['filter_gender0'];
                if ($filter_gender != 'all') {

                $gens= ' and gender in(';
                if (isset($_POST['filter_gender']) && $_POST['filter_gender'] ) {

                foreach ($_POST['filter_gender'] as $f) {
                    $gens .= "'".$f."',";
                  }
		}
                $gens .= "'nn')";
                $where .= $gens;
                }

            $sql .= $where;
            //print "$sql";

            $result = $wpdb->get_results($sql, ARRAY_A);
            //print "<pre>";
            //print_r ($result);
            
            $total_results = count($result);
            //print "\n\n$total_results";
            // $num_pages = ceil($total_results/$limit);
            // if ($page > $num_pages || $page < 1) $page = 1;

            // Form a new array to export as CSV format
            $export = array();
            $address_array = false;
            $preparation_array = false;
            $final_array = false;
            foreach ($result as $key => $value) {
                foreach ($customRows as $hkey => $hvalue) {
                    if ($hvalue == 'name') {
                        $export[$key]['Last Name'] = $value['body_last_name'];
                        $export[$key]['First Name'] = $value['body_first_name'];
                    }
                    else if ($hvalue == 'body_status') {
                        $export[$key]['Body Status'] = display_status($value[$hvalue]);
                    }
                    else if ($hvalue == 'gender') {
                        $export[$key]['Gender'] = display_gender($value[$hvalue]);
                    }
                    else if ($hvalue == 'address') {
                        $result[$key]['address'] = unserialize(stripslashes($result[$key]['address']));
                        $address_array = true;
                        $export[$key]['Street Address'] = $result[$key]['address']['street'];
                        $export[$key]['City'] = $result[$key]['address']['city'];
                        $export[$key]['State'] = $result[$key]['address']['state'];
                        $export[$key]['ZIP'] = $result[$key]['address']['zip_code'];
                    }

                    else if ($hvalue == 'lives_beyond') {
                        $export[$key]['Lives Beyond'] = $value['lives_beyond'];
                    }
                    else if ($hvalue == 'agree_to_pay') {
                        $export[$key]['Agree to Pay'] = $value['agree_to_pay'];
                    }

                    else if ($hvalue == 'phone') {
                        if (!$address_array) $result[$key]['address'] = unserialize(stripslashes($result[$key]['address']));
                        $address_array = true;
                        $export[$key]['Phone'] = "(".$result[$key]['address']['first_phone'].")".$result[$key]['address']['sec_phone']."-".$result[$key]['address']['third_phone'];
                    }
                    else if ($hvalue == 'email') {
                        if (!$address_array) $result[$key]['address'] = unserialize(stripslashes($result[$key]['address']));
                        $export[$key]['Email'] = $result[$key]['address']['email'];
                    }
                    else if ($hvalue == 'preparation') {
                        $result[$key]['reception'] = unserialize(stripslashes($result[$key]['reception']));
                        $preparation_array = true;
                        $export[$key]['Preparation'] = $result[$key]['reception']['preparation']['select'];
                    }
                    else if ($hvalue == 'body_location') {
                        if (!$preparation_array) $result[$key]['reception'] = unserialize(stripslashes($result[$key]['reception']));
                        $export[$key]['Body Location'] = $result[$key]['reception']['body_cond']['body_loc'];
                    }
                    else if ($hvalue == 'date_of_cremation') {
                        $result[$key]['final'] = unserialize(stripslashes($result[$key]['final']));
                        $final_array = true;
                        $export[$key]['Date of Cremation'] = $result[$key]['final']['date'];
                    }
                    else if ($hvalue == 'date_of_cremation') {
                        $result[$key]['final'] = unserialize(stripslashes($result[$key]['final']));
                        $final_array = true;
                        $export[$key]['Date of Cremation'] = $result[$key]['final']['date'];
                    }
                    else if ($hvalue == 'date_of_birth') {
                        $export[$key]['Date of Birth'] = $result[$key]['date_of_birth'];
                    }
                    else if ($hvalue == 'sig_card_received') {
                        $export[$key]['RegForm Received'] = $result[$key]['sig_card_received'];
                    }
                    else if ($hvalue == 'signature_date') {
                        $export[$key]['Signature Date'] = $result[$key]['signature_date'];
                    }
                    else if ($hvalue == 'how_about_us') {
                        $export[$key]['How Hear About Us'] = $result[$key]['how_about_us'];
                    }
                    else if ($hvalue == 'appreciation_received') {
                        $export[$key]['Appreciation Received'] = $result[$key]['appreciation_received'];
                    }
                    else if ($hvalue == 'withdrew') {
                        $export[$key]['Withdrew'] = $result[$key]['withdrew'];
                    }
                    else if ($hvalue == 'reason') {
                        $export[$key]['Withdrew Reason'] = $result[$key]['reason'];
                    }
                    else if ($hvalue == 'accept_optional_info') {
                        $result[$key]['accept_optional_info'] = unserialize(stripslashes($result[$key]['accept_optional_info']));
                        $export[$key]['Contact Firstname'] = $result[$key]['accept_optional_info']['kin_first_name'] ;
                        $export[$key]['Contact Lastname'] = $result[$key]['accept_optional_info']['kin_last_name'];
                        $export[$key]['Contact Status'] = $result[$key]['accept_optional_info']['kin_status'];
                        $export[$key]['Contact Address'] = $result[$key]['accept_optional_info']['kin_street'];
                        $export[$key]['Contact City'] = $result[$key]['accept_optional_info']['kin_city'];
                        $export[$key]['Contact State'] = $result[$key]['accept_optional_info']['kin_state'];
                        $export[$key]['Contact ZIP'] = $result[$key]['accept_optional_info']['kin_zip_code'];
                        $export[$key]['Contact Phone'] = '('.$result[$key]['accept_optional_info']['kin_first_phone'].')-'.$result[$key]['accept_optional_info']['kin_sec_phone'].'-'.$result[$key]['accept_optional_info']['kin_third_phone'].' add.'.$result[$key]['accept_optional_info']['kin_opt_phone'];
                        $export[$key]['Physical Firstname'] = $result[$key]['accept_optional_info']['phys_first_name'];
                        $export[$key]['Physical Lastname '] = $result[$key]['accept_optional_info']['phys_last_name'];
                        $export[$key]['Physical Phone'] = '('.$result[$key]['accept_optional_info']['phys_first_phone'].')-'.$result[$key]['accept_optional_info']['phys_sec_phone'].'-'.$result[$key]['accept_optional_info']['phys_third_phone'].' add.'.$result[$key]['accept_optional_info']['phys_opt_phone'];
                        $export[$key]['Contact Wants Cremains'] = $result[$key]['accept_optional_info']['kin_want_cremains'];
                        $export[$key]['Contact Firstname Alt'] = $result[$key]['accept_optional_info']['kin_first_name_alt'];
                        $export[$key]['Contact Lastname Alt'] = $result[$key]['accept_optional_info']['kin_last_name_alt'];
                        $export[$key]['Contact Status Alt'] = $result[$key]['accept_optional_info']['kin_status_alt'];
                        $export[$key]['Contact Address Alt'] = $result[$key]['accept_optional_info']['kin_street_alt'];
                        $export[$key]['Contact City Alt'] = $result[$key]['accept_optional_info']['kin_city_alt'];
                        $export[$key]['Contact State Alt'] = $result[$key]['accept_optional_info']['kin_state_alt'];
                        $export[$key]['Contact ZIP Alt'] = $result[$key]['accept_optional_info']['kin_zip_code_alt'];
                        $export[$key]['Contact Phone Alt'] = '('.$result[$key]['accept_optional_info']['kin_first_phone_alt'].')-'.$result[$key]['accept_optional_info']['kin_sec_phone_alt'].'-'.$result[$key]['accept_optional_info']['kin_third_phone_alt'].' add.'.$result[$key]['accept_optional_info']['kin_opt_phone_alt'];

                    }
                    else if ($hvalue == 'final_disposition') {
                        if (!$final_array) $result[$key]['final'] = unserialize(stripslashes($result[$key]['final']));
                        $export[$key]['Cremated'] = ($result[$key]['final']['cremated']? 'Yes' : 'No');
                        $export[$key]['Cremation Date'] = $result[$key]['final']['date'];
                        $export[$key]['Final Destination'] = $result[$key]['final']['cremains_destination'];
                        $export[$key]['Final Notes'] = $result[$key]['final']['notes'];

                    }
                    else if ($hvalue == 'body_usages') {
                        $result[$key]['usages'] = unserialize(stripslashes($result[$key]['usages']));
                        $result[$key]['usages']['res'] = '';
                        $export[$key]['Body Usages Type'] = '';
                        $export[$key]['Body Usages Name'] = '';
                        $export[$key]['Body Usages Date'] = '';

                        if($result[$key]['usages']['primary']['internal']['surg_203a']) {
                        $result[$key]['usages']['res'] = 'Internal - Surg 203A: Fall QTR';
                        $export[$key]['Body Usages Type'] = 'Internal';
                        $export[$key]['Body Usages Name'] = 'Surg 203A: Fall QTR';

                        }
                        else if($result[$key]['usages']['primary']['internal']['surg_203b']) {
                        $result[$key]['usages']['res'] = 'Internal - Surg 203B: Spring QTR';
                        $export[$key]['Body Usages Type'] = 'Internal';
                        $export[$key]['Body Usages Name'] = 'Surg 203B: Spring QTR';
                        }
                        else if($result[$key]['usages']['primary']['internal']['surg_101']) {
                        $result[$key]['usages']['res'] = 'Internal - Surg 101: Winter QTR';
                        $export[$key]['Body Usages Type'] = 'Internal';
                        $export[$key]['Body Usages Name'] = 'Surg 101: Winter QTR';
                        }
                        else if($result[$key]['usages']['primary']['internal']['other_med_student']['checkbox']) {
                        $result[$key]['usages']['res'] = 'Internal - Other Med Student Course: '.$result[$key]['usages']['primary']['internal']['other_med_student']['name'].', Date: '.$result[$key]['usages']['primary']['internal']['other_med_student']['date'];
                        $export[$key]['Body Usages Type'] = 'Internal';
                        $export[$key]['Body Usages Name'] = 'Other Med Student Course: '.$result[$key]['usages']['primary']['internal']['other_med_student']['name'];
                        $export[$key]['Body Usages Date'] = $result[$key]['usages']['primary']['internal']['other_med_student']['date'];

                        }
                        else if($result[$key]['usages']['primary']['internal']['resident']['checkbox']) {
                        $result[$key]['usages']['res'] = 'Internal - Resident Course: '.$result[$key]['usages']['primary']['internal']['resident']['name'].', Date: '.$result[$key]['usages']['primary']['internal']['resident']['date'];
                        $export[$key]['Body Usages Type'] = 'Internal';
                        $export[$key]['Body Usages Name'] = 'Resident Course: '.$result[$key]['usages']['primary']['internal']['resident']['name'];
                        $export[$key]['Body Usages Date'] = $result[$key]['usages']['primary']['internal']['resident']['date'];

                        }
                        else if($result[$key]['usages']['primary']['internal']['other']['checkbox']) {
                        $result[$key]['usages']['res'] = 'Internal - Other: '.$result[$key]['usages']['primary']['internal']['other']['name'].', Date: '.$result[$key]['usages']['primary']['internal']['other']['date'];
                        $export[$key]['Body Usages Type'] = 'Internal';
                        $export[$key]['Body Usages Name'] = 'Other: '.$result[$key]['usages']['primary']['internal']['other']['name'];
                        $export[$key]['Body Usages Date'] = $result[$key]['usages']['primary']['internal']['other']['date'];

                        }
                        else if($result[$key]['usages']['primary']['internal']['prosection']['checkbox']) {
                        $result[$key]['usages']['res'] = 'Internal - Prosection Usage: '.$result[$key]['usages']['primary']['internal']['prosection']['name'];
                        $export[$key]['Body Usages Type'] = 'Internal';
                        $export[$key]['Body Usages Name'] = 'Prosection Usage: '.$result[$key]['usages']['primary']['internal']['prosection']['name'];
                        }
                        else if($result[$key]['usages']['primary']['external']['outside_course']['checkbox']) {
                        $result[$key]['usages']['res'] = 'External - Outside Course: '.$result[$key]['usages']['primary']['external']['outside_course']['name'].', Date: '.$result[$key]['usages']['primary']['external']['outside_course']['date'];
                        $export[$key]['Body Usages Type'] = 'External';
                        $export[$key]['Body Usages Name'] = 'Outside Course: '.$result[$key]['usages']['primary']['external']['outside_course']['name'];
                        $export[$key]['Body Usages Date'] = $result[$key]['usages']['primary']['external']['outside_course']['date'];

 		        }
                        else if($result[$key]['usages']['primary']['external']['other']['checkbox']) {
                        $result[$key]['usages']['res'] = 'External - Other: '.$result[$key]['usages']['primary']['external']['other']['name'].', Date: '.$result[$key]['usages']['primary']['external']['other']['date'];
                        $export[$key]['Body Usages Type'] = 'External';
                        $export[$key]['Body Usages Name'] = 'Other: '.$result[$key]['usages']['primary']['external']['other']['name'];
                        $export[$key]['Body Usages Date'] = $result[$key]['usages']['primary']['external']['other']['date'];
                        }
//                        $export[$key]['Body Usages'] = $result[$key]['usages']['res'];
                    }

                    else {
                        $export[$key][custom_header_name($hvalue)] = $value[$hvalue];
                    }
                }
            }
            $SESS['result'] = $export;
            $SESS['report_name'] = "custom";

            $title = '';
            $rows = '';
            $message = '';

            // $display_page = '<div style="text-align:center; margin:20px"><h2 style="margin:20px; font-family:"NewsGothicSemi"">Page '.$page.' of '.$num_pages.'</h2>';
            $display_page = '';

            // table title       
            $title = '<div style="text-align:center; margin-top:20px; margin-bottom:20px">'.
                        '<table cellspacing="0" width="100%">'.
                            '<thead class="query_title" style="height:50px; margin:20px 50px 20px 50px; text-align:center; font-size:14pt">'.
                            '<tr>'.
                            $headerRow.
                            '</tr>'.
                        '</thead>';

            // Get the rows for that page
            // $rows = get_rows_custom($page, $num_pages, $limit, $total_results, $result, $customRows);
            $rows = get_rows_custom($total_results, $result, $customRows);

            if (count($result) == 0) {
                $message = "<h2 style='text-align:center; margin:30pt'>NNNO RECORDS FOUND FOR THE REQUIRED REPORT</h2>";
            }
            $html .= $display_page.$title.$rows.$message.'</div>';
//            echo $html;

            $SESS['html'] = $html;
            // $next_page = $page + 1;
            // $previous_page = $page - 1;
            break;

        // Default case => No report
        default:
//            echo "<h2 style='text-align:center; margin:30pt'>NO2 REPORT TO BE GENERATED</h2>";
    }
}
else {
//    echo "<h2 style='text-align:center; margin:30pt'>NO3 REPORT TO BE GENERATED</h2>";
}

//print '<pre>';
//print_r ($SESS);
//exit;


if (count($result) == 0) {
header('Location: ' . $_SERVER['HTTP_REFERER']);
exit;
}

$result = $SESS['result'];
$filename = "report_".date('Ymd_His').".xls";

$fp = fopen('php://output', 'w');
if ($fp && $result) {
  header("Content-Type: application/vnd.ms-excel");
    header('Content-Disposition: attachment; filename='.$filename);
$flag = false;
  foreach($result as $row) {
    if(!$flag) {
      // display field/column names as first row
      echo implode("\t", array_keys($row)) . "\r\n";
      $flag = true;
    }
    array_walk($row, 'cleanData');
    echo implode("\t", array_values($row)) . "\r\n";
  }
  exit;



}

function cleanData(&$str)
  {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
  }


?>
