<?php

/*
 * Template Name: Query Admin
 */

wp_enqueue_style('wbp_style_formstyler2');
get_header();
?>
<div class="wrapper3">
    <div class="query_h_bd3">
        <div style="height:73px">
            <div class="header_left"><a href="<?php bloginfo('url'); ?>"></a></div>
            <div class="header_right">
            <a name="header"><div class="right_bd"></div></a>
            <div class="header_nav">
                <div class="nav_back">
                <a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>></a>
                </div>
                <div class="nav_home"><a href="<?php bloginfo('url'); ?>"></a></div>
            </div>
            </div>
            <div class="header_center"><?php echo strtoupper(get_bloginfo('name')); ?></div>
        </div><div class="clear"></div>

    <div style="text-align:center; margin:50px">
        <h2 style="margin:20px; font-family:'NewsGothicSemi'">SELECT TYPE OF REPORT</h2>

        <form action="<?php bloginfo('url'); ?>/query" method="post">
            <div class="tc_line" style="padding-left:0">
            <div class="select200" style="float:center">
            <select id="report_dropdown" name="report" required="required">
                <option value="bodies_in_fridge">Bodies in Fridge</option>
                <option value="cremated">Cremated</option>
                <option value="embalmed">Embalmed</option>
                <option value="withdrew">Withdrew</option>
                <option value="registered">Registered</option>
                <option id="custom_report_option" value="custom">Custom Report</option>
            </select>
            </div></div>

            <!-- Custom options -->
            <div id="custom_options">

            <!-- Custom fields -->
            <!--
            - Name
            - Registration Date
            - Body Status
            - Body ID
            - Gender
            - Phone number
            - Email address
            - Date of death
            - Cause of death
            - Preparation
            - Body location
            - Date of cremation
            -->
                <div class="tc_line">
                    <h2 style="text-align:center; margin-top:20px; font-family:'NewsGothicDemi';">Fields</p>
                </div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_name">
                </div>
                    <p class="tcl_p5">Name</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_date_registration">
                </div>
                    <p class="tcl_p5">Registration Date</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_body_status">
                </div>
                    <p class="tcl_p5">Body Status</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_unic_id">
                </div>
                    <p class="tcl_p5">Body ID</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_gender">
                </div>
                    <p class="tcl_p5">Gender</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_phone">
                </div>
                    <p class="tcl_p5">Phone Number</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_email">
                </div>
                    <p class="tcl_p5">Email Address</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_date_of_death">
                </div>
                    <p class="tcl_p5">Date of Death</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_cause_of_death">
                </div>
                    <p class="tcl_p5">Cause of Death</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_preparation">
                </div>
                    <p class="tcl_p5">Preparation</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_body_location">
                </div>
                    <p class="tcl_p5">Body Location</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_date_of_cremation">
                </div>
                    <p class="tcl_p5">Date of Cremation</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

<!--                 <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_reception">
                </div>
                    <p class="tcl_p5">Body Preparation</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_usages">
                </div>
                    <p class="tcl_p5">Body Usage</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="custom_final">
                </div>
                    <p class="tcl_p5">Final Disposition</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
 -->        
                <!-- Filter options -->
                <div class="tc_line">
                    <h2 style="text-align:center; margin-top:20px; font-family:'NewsGothicDemi';">Filters</p>
                </div><div class="clear"></div>

                <div class="tc_line">
                    <p class="tcl_p5">Body Status:</p>
                    <div class="select200" style="margin-left:20px">
                        <select name="filter_body_status">
                            <option value="all">All</option>
                            <option value="registered">Registered</option>
                            <option value="withdrew">Withdrew</option>
                            <option value="accepted">Accepted</option>
                            <option value="rejected">Rejected</option>
                            <option value="reception">In Storage</option>
                            <option value="usage">In Use</option>
                            <option value="departed">Departed</option>
                        </select>
                    </div>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                    <p class="tcl_p5">Gender:</p>
                    <div class="select200" style="margin-left:60px">
                        <select name="filter_gender">
                            <option value="all">All</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            <option value="unknown">Unknown</option>
                        </select>
                    </div>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

            </div>

            <div class="tc_submit" style="float:center">
                <!-- <input type="hidden" name="page_number" value="1" /> -->
                <input type="submit" id="btn_report_submit" value="generate" name="" style="margin:20px;" />
            </div>
        </form>
    </div>
    </div>
</div>

<script type="text/javascript">
    if($('#custom_report_option').prop('selected')) {
        $('#custom_options').css({'display':'block'});
    }
    else {
        $('#custom_options').css({'display':'none'});
    }

    $('#report_dropdown').change(function() {
        if($('#custom_report_option').prop('selected')) {
            $('#custom_options').css({'display':'block'});
        }
        else {
            $('#custom_options').css({'display':'none'});
        }
    });
</script>

<?php
get_footer();
?>