<?php

/**
 * Sort donors list for page-database
 */

//echo '<pre>'; print_r($_POST);
$ser_results = $_POST['results'];
$results = unserialize(stripslashes($ser_results));
$sort = isset($_POST['sort']) ? $_POST['sort'] : 'AtoZ';

//echo '<pre>'; print_r($results);

function calcStatus($body_status){
	if(isset($body_status) && $body_status){
		switch($body_status){
			case 'registered':{
				return 'Registered';
			}break;
			case 'withdrew':{
				return 'Withdrew';
			}break;
			case 'accepted':{
				return 'Accepted';
			}break;
			case 'rejected':{
				return 'Rejected';
			}break;
			case 'reception':{
				return 'In Storage';
			}break;
			case 'usage':{
				return 'In Use';
			}break;
			case 'departed':{
				return 'Departed';
			}break;
			default: return "";
		} // end switch
	} else {
		return "";
	}		
}

function array_sort_func($a, $b=NULL) { 
	static $keys; 
	if($b === NULL) return $keys = $a; 
	foreach($keys as $k){ 
		if(@$k[0] == '!'){ 
			$k = substr($k, 1); 
			if(@$a[$k] !==  @$b[$k]){
				if(@$a[$k] == "0" && @$b[$k] != "0") return 1;
				if(@$b[$k] == "0" && @$a[$k] != "0") return -1;
				else return strcmp(@$b[$k], @$a[$k]);
			}
		} else if(@$a[$k] !== @$b[$k]) { 
			if(@$b[$k] == "0" && @$a[$k] != "0") return -1;
			if(@$a[$k] == "0" && @$b[$k] != "0") return 1;
			else return strcmp(@$a[$k], @$b[$k]); 
		}
   }
   return 0; 
} 

function array_sort(&$array) { 
   if(!$array) return $keys; 
   $keys = func_get_args(); 
   array_shift($keys); 
   array_sort_func($keys); 
   usort($array, "array_sort_func");        
} 

function sortDonors($results, $sort = 'AtoZ'){
	$wp_load_loc = $_SERVER['DOCUMENT_ROOT']."/wp-load.php";
	require_once($wp_load_loc);
	global $wpdb;

	$table_name = 'wp_wbp_donors_data';
	$sql = 'SELECT id, body_first_name, body_last_name, body_status, unic_id, gender, date_of_birth, reception FROM '.$table_name;

	
	//* ���������� �� ������� �� #SORT_BY
	if( isset($_POST['sort']) && empty($_GET) ){
		switch($sort){
			case 'alphabet':
				$sql .= ' ORDER BY body_last_name ASC';
				$chars = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
				break;
			case 'alphabet_revers':
				$sql .= ' ORDER BY body_last_name DESC';
				$chars = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
				$chars = array_reverse($chars);
				break;
			case 'date_sign':
				$sql .= ' ORDER BY date_registration';
				break;
			case 'age':
				$sql .= ' ORDER BY date_of_birth';
				break;
			case 'date_dirth':
				$sql .= ' ORDER BY date_of_birth';
				break;
			case 'gender':
				$sql .= ' ORDER BY gender';
				break;
			case 'body_status':

				$status_arr = array('registered', 'withdrew', 'accepted', 'rejected', 'reception', 'usage', 'departed');
				$results = array();
				foreach( $status_arr as $stat ){
					$sql = 'SELECT id, body_first_name, body_last_name, body_status, unic_id, gender, date_of_birth, reception
								FROM '.$table_name.' WHERE body_status = "'.$stat.'" ORDER BY body_last_name';
					$tmp_res = $wpdb->get_results($sql, ARRAY_A);
					$results = array_merge($results, $tmp_res);
				}
				$tmp_flag = true;
				break;
			case 'id_num':
				$sql = 'SELECT id, body_first_name, body_last_name, body_status, unic_id, gender, date_of_birth, reception
						  FROM '.$table_name.' 
							WHERE unic_id > 0 ORDER BY unic_id ASC';
				$tmp_res_1 = $wpdb->get_results($sql, ARRAY_A);
				
				$sql = 'SELECT id, body_first_name, body_last_name, body_status, unic_id, gender, date_of_birth, reception
						  FROM '.$table_name.' 
							WHERE unic_id < 1 ORDER BY body_last_name';
				$tmp_res_2 = $wpdb->get_results($sql, ARRAY_A);
				$results = array_merge($tmp_res_1, $tmp_res_2);
				$tmp_flag = true;
				break;
			default: break;
		}
		
		if( !$tmp_flag ){
$sql .= ' LIMIT 10';
print "$sql<br>\n";
			$results = $wpdb->get_results($sql, ARRAY_A);
		}
		$tmp_flag = false;
		
		if(isset($chars) ){
			foreach( $chars as $char ){
?>
				<!--separate for letter-->
<!-- 				<tr>
					<td class="bd_let_line_left" colspan="3">
						<p class="db_letter"><a name="<?php echo $char.'-anchor'; ?>"><?php echo $char; ?></a></p>
					</td>
				</tr>     //-->
				<!--/separate for letter-->
<?php
				foreach( $results as $val ){
				
					if( !is_array($val) ){ continue; }
					if( ucfirst($val['body_last_name']{0}) == $char ){
?>
						<tr>
							<td class="cone sep_v">
								<p class="line_pad db_text2">
									<a class="db_text2" href="<?php echo $val['bloginfo_url']; ?>/info?donor_id=<?php echo $val['id']; ?>&body_status=<?php echo strtolower(calcStatus($val['body_status'])); ?>">
										<?php echo $val['body_last_name'].', '.$val['body_first_name']; ?>
									</a>
								</p>
							</td>
							<td class="ctwo sep_v">
								<?php echo ( isset($val['unic_id']) && $val['unic_id'] > 0 ? '<p class="line_pad db_text2">'.$val['unic_id'].'</p>' : '' ); ?>
							</td>
							<td class="cthree">
								<p class="line_pad db_text2">
<?php
											echo calcStatus($val['body_status']);
?>
								</p>
							</td>
						</tr>
<?php
					}
				}
			}
		}else{
			foreach( $results as $val ){
?>
					<tr>
						<td class="cone sep_v">
							<p class="line_pad db_text2">
								<a class="db_text2" href="<?php echo $val['bloginfo_url']; ?>/info?donor_id=<?php echo $val['id']; ?>&body_status=<?php echo strtolower(calcStatus($val['body_status'])); ?>">
									<?php echo $val['body_last_name'].', '.$val['body_first_name']; ?>
								</a>
							</p>
						</td>
						<td class="ctwo sep_v">
							<?php echo ( isset($val['unic_id']) && $val['unic_id'] > 0 ? '<p class="line_pad db_text2">'.$val['unic_id'].'</p>' : '' ); ?>
						</td>
						<td class="cthree">
							<p class="line_pad db_text2">
<?php
								echo calcStatus($val['body_status']);
?>
							</p>
						</td>
					</tr>
<?php
			}
		}
		exit;
	}
//echo '<pre>'; print_r($results);
	//* ���������� �� ������� �� ������ �������
    //* ���������� �� ID 
    

    if(isset($_GET['sort_by_id'])){

		if($sort == 'AtoZ'){
			array_sort($results, 'unic_id');
		} else {
			array_sort($results, '!unic_id');
		}

		foreach( $results as $val ){
			if(!is_array($val)) continue;
?>
				<tr>
					<td class="cone sep_v">
						<p class="line_pad db_text2">
							<a class="db_text2" href="<?php echo $val['bloginfo_url']; ?>/info?donor_id=<?php echo $val['id']; ?>&body_status=<?php echo strtolower(calcStatus($val['body_status'])); ?>">
								<?php echo $val['body_last_name'].', '.$val['body_first_name']; ?>
							</a>
						</p>
					</td>
					<td class="ctwo sep_v">
						<?php echo ( isset($val['unic_id']) && $val['unic_id'] > 0 ? '<p class="line_pad db_text2">'.$val['unic_id'].'</p>' : '' ); ?>
					</td>
					<td class="cthree">
						<p class="line_pad db_text2">
<?php
						echo calcStatus($val['body_status']);
?>
						</p>
					</td>
				</tr>
<?php
		}

		exit;
    }
    
    //* ���������� �� BODY_STATUS
    if( isset($_GET['db_table_status']) ){
    
	/* Registered - registered */
	/* Withdrew - withdrew */
	/* Accepted - accepted */
	/* Rejected - rejected */
	/* In Storage - reception */
	/* In Use - usage */
	/* Cremated - departed */
    
	foreach( $results as $key => $val ){
	    if( $val['body_status'] == 'accepted' ){ $tmp_res[0][] = $val; }
	    if( $val['body_status'] == 'departed' ){ $tmp_res[1][] = $val; }
	    if( $val['body_status'] == 'reception' ){ $tmp_res[2][] = $val; }
	    if( $val['body_status'] == 'usage' ){ $tmp_res[3][] = $val; }
	    if( $val['body_status'] == 'registered' ){ $tmp_res[4][] = $val; }
	    if( $val['body_status'] == 'rejected' ){ $tmp_res[5][] = $val; }
	    if( $val['body_status'] == 'withdrew' ){ $tmp_res[6][] = $val; }
	
	if( $val['body_status'] != 'accepted'  && 
		$val['body_status'] != 'departed' &&
		$val['body_status'] != 'reception'  &&
		$val['body_status'] != 'usage'  &&
		$val['body_status'] != 'registered'  &&
		$val['body_status'] != 'rejected'  &&
		$val['body_status'] != 'withdrew' 
		){ $tmp_res2[7][] = $val; }


	}
	
	if( $sort == 'AtoZ' ){
	    ksort($tmp_res);
	}else{
	    krsort($tmp_res);
	}

	
	$tmp_res = array_merge($tmp_res, $tmp_res2);

	foreach( $tmp_res as $v ){
	foreach( $v as $val ){
?>
            <tr>
				<td class="cone sep_v">
					<p class="line_pad db_text2">
						<a class="db_text2" href="<?php echo $val['bloginfo_url']; ?>/info?donor_id=<?php echo $val['id']; ?>&body_status=<?php echo strtolower(calcStatus($val['body_status'])); ?>">
							<?php echo $val['body_last_name'].', '.$val['body_first_name']; ?>
						</a>
					</p>
				</td>
				<td class="ctwo sep_v">
					<?php echo ( isset($val['unic_id']) && $val['unic_id'] > 0 ? '<p class="line_pad db_text2">'.$val['unic_id'].'</p>' : '' ); ?>
				</td>
				<td class="cthree">
					<p class="line_pad db_text2">
<?php
						echo calcStatus($val['body_status']);
?>
					</p>
				</td>
            </tr>
<?php
	}
      }

	exit;
    }

    //* ���������� �� LAST_NAME
    $chars = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

    if( $sort != 'AtoZ' ){ $chars = array_reverse($chars); }

    foreach( $chars as $char ){
		if( !isset($_GET['sort_by_id']) ){
?>
			<!--separate for letter-->
<!--				<tr>
					<td class="bd_let_line_left" colspan="3">
						<p class="db_letter"><a name="<?php echo $char.'-anchor'; ?>"><?php echo $char; ?></a></p>
					</td>
				</tr> //-->
			<!--/separate for letter-->
<?php
		}/* end IF */
	
	foreach( $results as $val ){

	    if( !is_array($val) ){ continue; }
	    if( ucfirst($val['body_last_name']{0}) == $char ){
?>	    
<!--line-->
            <!--tr>
		    <td class="sep_left"></td>
		    <td class="sep_center"></td>
		    <td class="sep_right"></td>
	    </tr-->
			
            <tr>
				<td class="cone sep_v">
					<p class="line_pad db_text2">
						<a class="db_text2" href="<?php echo $val['bloginfo_url']; ?>/info?donor_id=<?php echo $val['id']; ?>&body_status=<?php echo strtolower(calcStatus($val['body_status'])); ?>">
							<?php echo $val['body_last_name'].', '.$val['body_first_name']; ?>
						</a>
					</p>
				</td>
				<td class="ctwo sep_v">
					<?php echo ( isset($val['unic_id']) && $val['unic_id'] > 0 ? '<p class="line_pad db_text2">'.$val['unic_id'].'</p>' : '' ); ?>
				</td>
				<td class="cthree">
					<p class="line_pad db_text2">
<?php
						echo calcStatus($val['body_status']);
?>
					</p>
				</td>
            </tr>
<!--/line-->
<?php	    
	    } // end if
	} // end foreach
    } // end foreach

}

sortDonors($results, $sort);
//*/
?>