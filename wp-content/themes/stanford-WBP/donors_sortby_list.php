<?php
/*
 * Template Name: DONORS SORTBY LIST
 */



//echo 'zzz:'; 
//print_r($_POST['sort']);
//print_r($_POST['tags']);
//die;

function sortByDonors($results, $sort = 'alphabet', $tags = ''){
	global $wpdb;

	$ser_results = $_POST['results'];
	$results = unserialize(stripslashes($ser_results));
	$sort = isset($_POST['sort']) ? $_POST['sort'] : 'alphabet';
	//$sort = 'alphabet';
	$tags = isset($_POST['tags']) ? $_POST['tags'] : '';
	
	
	$table_name = 'wp_wbp_donors_data';
	$sql = 'SELECT id, body_first_name, body_last_name, body_status, unic_id, gender, date_of_birth FROM '.$table_name.' WHERE ';

	$param = $tags;
	$last_first = explode(' ', $param);
	
	/*
	if( count($last_first) == 1 && is_numeric($last_first[0]) ){
		$sql .= ' WHERE unic_id LIKE "'.$last_first[0].'%" ORDER BY unic_id ASC';
	}elseif( count($last_first) == 1 && is_string($last_first[0]) ){
		$sql .= ' WHERE body_last_name LIKE "'.$last_first[0].'%" OR body_first_name LIKE "'.$last_first[0].'%"';
	}elseif( count($last_first) > 1 ){
		$sql .= ' WHERE (body_last_name LIKE "'.$last_first[0].'%" AND body_first_name LIKE "'.$last_first[1].'%") OR (body_last_name LIKE "'.$last_first[1].'%" AND body_first_name LIKE "'.$last_first[0].'%") ORDER BY body_last_name ASC';
	}else{
		//$sql .= 'body_last_name LIKE "'.$last_first[0].'%" OR body_first_name LIKE "'.$last_first[0].'%" ORDER BY body_last_name ASC';
	}
*/
	
	$chars = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
//echo $sql; die;
	
	switch($sort){
	    case 'alphabet':
			if( count($last_first) == 1 && is_int((int)$last_first[0]) ){
				$sql .= 'unic_id = "'.$last_first[0].'" ORDER BY unic_id ASC';
			}elseif( count($last_first) > 1 ){
				$sql .= 'body_last_name LIKE "'.$last_first[0].'%" AND body_first_name LIKE "'.$last_first[1].'%" ORDER BY body_last_name ASC';
			}else{
				$sql .= 'body_last_name LIKE "'.$last_first[0].'%" OR body_first_name LIKE "'.$last_first[0].'%" ORDER BY body_last_name ASC';
			}
			$chars = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
			break;
	    case 'alphabet_revers':
			if( count($last_first) > 1 ){
				$sql .= 'body_last_name LIKE "'.$last_first[0].'%" AND body_first_name LIKE "'.$last_first[1].'%" ORDER BY body_last_name DESC';
			}else{
				$sql .= 'body_last_name LIKE "'.$last_first[0].'%" OR body_first_name LIKE "'.$last_first[0].'%" ORDER BY body_last_name DESC';
			}
			$chars = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
			$chars = array_reverse($chars);
			break;
	    case 'date_sign':
			if( !empty($param) ){
				$sql .= 'date_registration LIKE "%'.$param.'%"';
			}else{
				$sql .= '1 = 1 ORDER BY date_registration DESC';
			}
			break;
	    case 'age':
			if( !empty($param) ){
				$date_birth = (int)date('Y')-(int)$param;
				$sql .= 'date_of_birth="'.$date_birth.'"';
			}else{
				$sql .= '1=1 ORDER BY date_of_birth';
			}
			break;
	    case 'date_dirth':
			if( !empty($param) ){
				$sql .= 'date_of_birth="'.$param.'"';
			}else{
				$sql .= '1=1 ORDER BY date_of_birth';
			}
			break;
	    case 'gender':
			if( !empty($param) ){

$f = array('woman', 'female', 'lady', 'skirt', 'dame', 'gentlewoman', 'madam', 'madame', 'senora', 'babe', 'beauty', 'belle', 'chick', 'damsel', 'doll', 'gal', 'girl', 'ingenue', 'lass', 'lassie', 'mademoiselle', 'maid', 'maiden', 'miss', 'senorita');
$m = array('male', 'bastard', 'bloke', 'buck', 'cat', 'chap', 'chappie', 'dude', 'fella', 'fellow', 'galoot', 'gent', 'gentleman', 'guy', 'hombre', 'jack', 'joe', 'joker', 'lad', 'man', 'master', 'mister', 'sir', 'buddy', 'buster');

if (in_array($param, $f)) {
$param ="female";
}
if (in_array($param, $m)) {
    $param ="male";
}


				$sql .= 'gender="'.$param.'"';
			}else{
				$sql .= '1=1 ORDER BY gender';
			}
			break;
	    case 'body_status':
			switch( $param ){

			case 'Registered':{ 
				$param = 'registered';
			}break;
			case 'Withdrew':{
				$param = 'withdrew';
			}break;
			case 'Accepted':{
				$param = 'accepted';
			}break;
			case 'Rejected':{
				$param = 'rejected';
			}break;
			case 'In Storage':{
				$param = 'reception';
			}break;
			case 'In Use':{
				$param = 'usage';
			}break;
			case 'Departed':{
				$param = 'departed';
			}break;
			default: break;
			}

			if( !empty($param) ){
				$sql .= 'body_status="'.$param.'"';
			}else{
				$sql .= '1=1 ORDER BY body_status';
			}
			break;
	    case 'id_num':
			if( !empty($param) ){
				$sql .= 'unic_id="'.$param.'"';
			}else{
				$sql = 'SELECT id, body_first_name, body_last_name, body_status, unic_id, gender, date_of_birth 
					  FROM '.$table_name.' 
					    WHERE unic_id > 0 ORDER BY unic_id';
				$tmp_res_1 = $wpdb->get_results($sql, ARRAY_A);
				
				$sql = 'SELECT id, body_first_name, body_last_name, body_status, unic_id, gender, date_of_birth 
					  FROM '.$table_name.' 
					    WHERE unic_id < 1 ORDER BY body_last_name';
				$tmp_res_2 = $wpdb->get_results($sql, ARRAY_A);
				$results = array_merge($tmp_res_1, $tmp_res_2);
				

$tmp_flag = true;
				//$sql .= '1=1 ORDER BY unic_id DESC';
			}
			break;
	    default: break;
	}

	//if( !$tmp_flag ){


	//echo "sort:".$sort;
	//echo $sql;die;

	    $results = $wpdb->get_results($sql, ARRAY_A);



	/*}
	$tmp_flag = false;//*/

	if( isset($chars) ){
		foreach( $chars as $char ){
?>
			<!--separate for letter-->

			<tr>
				<td class="bd_let_line_left" colspan="3">
                                             <script type="text/javascript"> $(".sr_big_red").text("<? echo count($results); ?>"); </script>
					<p class="db_letter"><a name="<?php echo $char.'-anchor'; ?>"><?php echo $char; ?></a></p>
				</td>


			</tr>
			<!--/separate for letter-->
<?php


			foreach( $results as $val ){
				if( !is_array($val) ){ continue; }
				if( ucfirst($val['body_last_name']{0}) == $char ){
?>

					<tr>
						<td class="cone sep_v">
							<p class="line_pad db_text2">
								<a class="db_text2" href="<?php echo $val['bloginfo_url']; ?>/info?donor_id=<?php echo $val['id']; ?>&body_status=<?php echo $val['body_status']; ?>">
								<!--a class="db_text2" href="<?php echo $results['bloginfo_url']; ?>/info?donor_id=<?php echo $val['id']; ?>&body_status=<?php echo $next_status; ?>"-->
									<?php echo $val['body_last_name'].', '.$val['body_first_name']; ?>
								</a>
							</p>
						</td>
						<td class="ctwo sep_v">
							<?php echo ( isset($val['unic_id']) && $val['unic_id'] > 0 ? '<p class="line_pad db_text2">'.$val['unic_id'].'</p>' : '' ); ?>
						</td>
						<td class="cthree">
							<p class="line_pad db_text2">
	<?php
								if( isset($val['body_status']) && $val['body_status'] ){
									switch( $val['body_status'] ){
										case 'registered':{
											echo 'Registered';
										}break;
										case 'withdrew':{
											echo 'Withdrew';
										}break;
										case 'accepted':{
											echo 'Accepted';
										}break;
										case 'rejected':{
											echo 'Rejected';
										}break;
										case 'reception':{
											echo 'In Storage';
										}break;
										case 'usage':{
											echo 'In Use';
										}break;
										case 'departed':{
											echo 'Departed';
										}break;
										default: break;
									} // end switch
								} // end if
	?>
							</p>
						</td>
					</tr>
<?php
				}
			}
		}
	}else{
		foreach( $results as $val ){
?>
				<tr>
					<td class="cone sep_v">
						<p class="line_pad db_text2">
							<a class="db_text2" href="<?php echo $val['bloginfo_url']; ?>/info?donor_id=<?php echo $val['id']; ?>&body_status=<?php echo $val['body_status']; ?>">
							<!--a class="db_text2" href="<?php echo $results['bloginfo_url']; ?>/info?donor_id=<?php echo $val['id']; ?>&body_status=<?php echo $next_status; ?>"-->
								<?php echo $val['body_last_name'].', '.$val['body_first_name']; ?>
							</a>
						</p>
					</td>
					<td class="ctwo sep_v">
						<?php echo ( isset($val['unic_id']) && $val['unic_id'] > 0 ? '<p class="line_pad db_text2">'.$val['unic_id'].'</p>' : '' ); ?>
					</td>
					<td class="cthree">
						<p class="line_pad db_text2">
<?php
							if( isset($val['body_status']) && $val['body_status'] ){
								switch( $val['body_status'] ){
									case 'registered':{
										echo 'Registered';
									}break;
									case 'withdrew':{
										echo 'Withdrew';
									}break;
									case 'accepted':{
										echo 'Accepted';
									}break;
									case 'rejected':{
										echo 'Rejected';
									}break;
									case 'reception':{
										echo 'In Storage';
									}break;
									case 'usage':{
										echo 'In Use';
									}break;
									case 'departed':{
										echo 'Departed';
									}break;
									default: break;
								} // end switch
							} // end if
?>
						</p>
					</td>
				</tr>
<?php
		}
	}
?>
<script>
	$(function(){
		$('#hidd_res').val('<?php echo serialize($results); ?>');
	})
</script>
<?php
}

sortByDonors($results, /*$sort,*/ $tags);
?>