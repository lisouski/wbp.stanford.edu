<?php
/*
 * Template Name: Search
 */
 
 /**
 * �������� � ������ ������ (4� pdf - http://4wbp.vex/search/)
 */
 
get_header();
?>

<script type='text/javascript'>
$(document).ready(function(){
//autocomplete

	$.widget( "custom.catcomplete", $.ui.autocomplete, {
		_renderItem: function( ul, item) {
			return $( "<li><\/li>" )
			.data( "item.autocomplete", item )
			.append( $( "<a><\/a>" ).html( item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + this.term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1<\/strong>") ) )
			.appendTo( ul );
		}
	});
	//field id click function
	$("#tags_ln").on("click", function(){
			//call to DB script
			var aTo  = $(this).catcomplete({source: '/autocompletsearch.php?search=last_name&status='+$("#search_hidd_referer").val(), select: autocompleteRightUser, close:autocompleteRightUser, minLength: 1, delay: 500});

	});
	$("#tags_fn").on("click", function(){
			//call to DB script
			$(this).catcomplete({source: '/autocompletsearch.php?search=first_name&status='+$("#search_hidd_referer").val(), select: autocompleteRightUser, close:autocompleteRightUser, minLength: 1, delay: 500});
	});
	//end of field id click function
	
	
	//autocomplete result action (can be blank)
	function autocompleteRightUser(event, ui){

		if (ui.item) id = ui.item.id;
		else id = null;
		if (id)	{
		        $("#tags_fn").val(ui.item.val);
                $("#tags_ln").val(ui.item.val);

		//$("#userid").val(id);
		}
		else $('#query_toolbar').remove();

	}
	//end autocomplete

});
//end document.ready
</script>

	<div class="wrapper4">
<?php
wbpGetBigHeaderBlock();

//echo '<pre>'; print_r(get_defined_constants());

$title_name = '';
if( isset($_GET['link']) && $_GET['link'] ){
    switch( $_GET['link'] ){
		case 'registered':{
			$title_name = strtoupper('register donor');
		};break;
		case 'accepted':{
			$title_name = strtoupper('accept donation');
			echo '<style>.search_title_icon{ background-image:url('.get_bloginfo('template_url').'/img/icons/accept.png); }</style>';
		};break;
		case 'reception':{
			$title_name = strtoupper('body prep');
			echo '<style>.search_title_icon{ background-image:url('.get_bloginfo('template_url').'/img/icons/body_prep.png); width:46px; }</style>';
		};break;
		case 'usage':{
			$title_name = strtoupper('body use');
			echo '<style>.search_title_icon{ background-image:url('.get_bloginfo('template_url').'/img/icons/body_use.png); }</style>';
		};break;
		case 'departed':{
			$title_name = strtoupper('final disposition');
			echo '<style>.search_title_icon{ background-image:url('.get_bloginfo('template_url').'/img/icons/final_disp.png); }</style>';
		};break;
		default:break;
    }
}

?>
        <div class="begin_cont">
        	<div class="begin_tit">
				<div class="search_title_icon">
					
				</div>
				<p><?php echo $title_name; ?></p>
			</div>
			
			<form action="<?php bloginfo('url'); ?>/search-results" method="post">
				<input id="search_hidd_referer" type="hidden" name="search_hidd_referer" value="<?php echo ( isset($_GET['refer']) && $_GET['refer'] ? $_GET['refer'] : '' ); ?>" />
					
				<div class="begin_last_name">
				<input id="tags_ln" class="ui-autocomplete-input" type="text" name="search_last_name" onFocus="doClear(this)" onBlur="doDefault(this)" value="" placeholder="Last Name..." />
				</div>		
				<div class="begin_first_name">
				<input id="tags_fn" type="text" name="search_first_name" onFocus="doClear(this)" onBlur="doDefault(this)" value="" placeholder="First Name..." />
				</div>
		    
<style>
    .begin_cont{ height:434px; width:417px; background:url(<?php bloginfo('template_url'); ?>/img/bg_start.png) no-repeat; margin:0 auto; padding-top:18px;}
</style>
		    
<?php
if( $_GET['refer'] != 'registered' && $_GET['refer'] != 'accepted' ){
?>
<style>
    .begin_cont{ height:434px; width:417px; background:url(<?php bloginfo('template_url'); ?>/img/bg_enter.png); margin:0 auto; padding-top:18px;}
</style>
				<div class="begin_id">
					<input id="tags_id" type="text" name="search_id" onFocus="doClear(this)" onBlur="doDefault(this)" value="" placeholder="ID Number..." />
				</div>
<?php
}
?>
				<div class="begin_button">
					<input type="submit" value="search" />
				</div>
<?php
if( $_GET['link'] == 'registered' || $_GET['link'] == 'accepted' ){
?>
				<div>
					<a class="nf_a" style="margin-left:105px;" href="<?php bloginfo('url'); ?>/edit">
						<input class="nf" type="button" value="NEW FILE" />
					</a>
				</div>
<?php
}
?>
                <!--<div class="begin_button">
					<input type="button" value="NEW FILE" />
				</div>-->
            </form>

			<div class="begin_nav">
				<div class="begin_back">
					<a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>>BACK</a>
				</div>
				<div class="begin_home"><a href="<?php bloginfo('url'); ?>">HOME</a></div>
			</div><div class="clear"></div>
        </div>
    
    </div>
	
<?php get_footer(); ?>