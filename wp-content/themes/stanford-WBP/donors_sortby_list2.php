<?php
/*
 * Template Name: DONORS SORTBY LIST
 */




function calcStatus($body_status){
	if(isset($body_status) && $body_status){
		switch($body_status){
			case 'registered':{
				return 'Registered';
			}break;
			case 'withdrew':{
				return 'Withdrew';
			}break;
			case 'accepted':{
				return 'Accepted';
			}break;
			case 'rejected':{
				return 'Rejected';
			}break;
			case 'reception':{
				return 'In Storage';
			}break;
			case 'usage':{
				return 'In Use';
			}break;
			case 'departed':{
				return 'Departed';
			}break;
			default: return "";
		} // end switch
	} else {
		return "";
	}		
}

function array_sort_func($a, $b=NULL) { 
	static $keys; 
	if($b === NULL) return $keys = $a; 
	foreach($keys as $k){ 
		if(@$k[0] == '!'){ 
			$k = substr($k, 1); 
			if(@$a[$k] !==  @$b[$k]){
				if(@$a[$k] == "0" && @$b[$k] != "0") return 1;
				if(@$b[$k] == "0" && @$a[$k] != "0") return -1;
				else return strcmp(@$b[$k], @$a[$k]);
			}
		} else if(@$a[$k] !== @$b[$k]) { 
			if(@$b[$k] == "0" && @$a[$k] != "0") return -1;
			if(@$a[$k] == "0" && @$b[$k] != "0") return 1;
			else return strcmp(@$a[$k], @$b[$k]); 
		}
   }
   return 0; 
} 

function array_sort(&$array) { 
   if(!$array) return $keys; 
   $keys = func_get_args(); 
   array_shift($keys);
   array_sort_func($keys); 
   usort($array, "array_sort_func");        
} 


function sortByDonors($n, $sort = 'AtoZ', $tags = ''){

//	$wp_load_loc = $_SERVER['DOCUMENT_ROOT']."/WBP-DB/wp-load.php";
//	require_once($wp_load_loc);
	require_once( dirname(__FILE__) . '/../../../wp-load.php' );

	global $wpdb;

	$sort = isset($_POST['sort']) ? $_POST['sort'] : 'AtoZ';
	$tags = isset($_POST['tags']) ? $_POST['tags'] : '';
        $n = isset($_POST['n']) ? $_POST['n'] : 1 ;
        $s = PER_PAGE;
        $dd = ($n-1)*$s;


	// $h = fopen("3.csv","w");
	// fwrite($h,'1-'.$_POST['n'].' 2-'.$n.' 3-'.$dd);
	
	
	$table_name = 'wp_wbp_donors_data';
	$sql = 'SELECT SQL_CALC_FOUND_ROWS id, body_first_name, body_last_name, body_status, unic_id, gender, date_of_birth FROM '.$table_name.' WHERE ';

	$param = $tags;
	$last_first = explode(' ', $param);
		
	switch($sort){
		case 'dob':
        $stype = 'DOB '.$param;

        list($mon,$yr) = explode('/',$param);
        $sql .= 'date_of_birth LIKE "'.$mon.'/%" and date_of_birth LIKE "%/'.$yr.'" ORDER BY body_last_name ASC';

        break;


		case 'AtoZ':
            $stype = 'A to Z';
            $sql .= ' s_street like "%'.$param.'%" or s_city like "%'.$param.'%" or ';

            if( count($last_first) == 1 && is_int((int)$last_first[0]) && ((int)$last_first[0]) > 0 ){

                    $sql .= 'unic_id = "'.$last_first[0].'"  or  s_zip = "'.$param.'"  ORDER BY unic_id ASC';
            }
            elseif( count($last_first) > 1 ){
                    $sql .= '(body_last_name LIKE "'.$last_first[0].'%" AND body_first_name LIKE "'.$last_first[1].'%") OR (body_first_name LIKE "'.$last_first[0].'%" AND body_last_name LIKE "'.$last_first[1].'%") ORDER BY body_last_name ASC';
            }
            else{
                    $sql .= 'body_last_name LIKE "'.$last_first[0].'%" OR body_first_name LIKE "'.$last_first[0].'%" ORDER BY body_last_name ASC';
            }
            break;
    
    	case 'ZtoA':
            $stype = 'Z to A';
            if( count($last_first) > 1 ){
                    $sql .= '(body_last_name LIKE "'.$last_first[0].'%" AND body_first_name LIKE "'.$last_first[1].'%") OR (body_first_name LIKE "'.$last_first[0].'%" AND body_last_name LIKE "'.$last_first[1].'%") ORDER BY body_last_name DESC';
            }else{
                    $sql .= 'body_last_name LIKE "'.$last_first[0].'%" OR body_first_name LIKE "'.$last_first[0].'%" ORDER BY body_last_name DESC';
            }
            break;
	    
	    case 'date_sign':
			$stype = 'Date signing';
			if( !empty($param) ){
				$sql .= 'date_registration LIKE "%'.$param.'%"';
			}else{
				$sql .= '1 = 1 ORDER BY date_registration DESC';
			}
			break;
	    
	    case 'age':
			$stype = 'Age';
			if( !empty($param) ){
				$date_birth = (int)date('Y')-(int)$param;
				$sql .= 'date_of_birth="'.$date_birth.'"';
			}else{
				$sql .= '1=1 ORDER BY date_of_birth';
			}
			break;
	    
	    case 'date_dirth':
			$stype = 'Date of Birth';
			if( !empty($param) ){
				$sql .= 'date_of_birth="'.$param.'"';
			}else{
				$sql .= '1=1 ORDER BY date_of_birth';
			}
			break;
	    
	    case 'gender':
			$stype = 'Gender';
			if( !empty($param) ){

				$f = array('woman', 'female', 'lady', 'skirt', 'dame', 'gentlewoman', 'madam', 'madame', 'senora', 'babe', 'beauty', 'belle', 'chick', 'damsel', 'doll', 'gal', 'girl', 'ingenue', 'lass', 'lassie', 'mademoiselle', 'maid', 'maiden', 'miss', 'senorita');
				$m = array('male', 'bastard', 'bloke', 'buck', 'cat', 'chap', 'chappie', 'dude', 'fella', 'fellow', 'galoot', 'gent', 'gentleman', 'guy', 'hombre', 'jack', 'joe', 'joker', 'lad', 'man', 'master', 'mister', 'sir', 'buddy', 'buster');

				if (in_array($param, $f)) {
					$param ="female";
				}
				if (in_array($param, $m)) {
					$param ="male";
				}
				$sql .= 'gender="'.$param.'"';
			}
			else{
				$sql .= '1=1 ORDER BY gender';
			}
			break;
	    
	    case 'body_status':
			$stype = 'Body status';

			switch( $param ){

			case 'Registered':{ 
				$param = 'registered';
			}break;
			case 'Withdrew':{
				$param = 'withdrew';
			}break;
			case 'Accepted':{
				$param = 'accepted';
			}break;
			case 'Rejected':{
				$param = 'rejected';
			}break;
			case 'In Storage':{
				$param = 'reception';
			}break;
			case 'In Use':{
				$param = 'usage';
			}break;
			case 'Departed':{
				$param = 'departed';
			}break;
			default: break;
			}

			if( !empty($param) ){
				$sql .= 'body_status="'.$param.'"';
			}else{
				$sql .= '1=1 ORDER BY body_status';
			}
			break;
	    
	    case 'id_num':
			$stype = 'ID Number';

			if( !empty($param) ){
				$sql .= 'unic_id="'.$param.'"';
			}else{
				$sql = 'SELECT SQL_CALC_FOUND_ROWS id, body_first_name, body_last_name, body_status, unic_id, gender, date_of_birth 
					  FROM '.$table_name.' 
					    WHERE unic_id > 0 ORDER BY unic_id';
				$tmp_res_1 = $wpdb->get_results($sql, ARRAY_A);
				
				$sql = 'SELECT id, body_first_name, body_last_name, body_status, unic_id, gender, date_of_birth 
					  FROM '.$table_name.' 
					    WHERE unic_id < 1 ORDER BY body_last_name';
				$tmp_res_2 = $wpdb->get_results($sql, ARRAY_A);
				$results = array_merge($tmp_res_1, $tmp_res_2);
				
				$tmp_flag = true;
				//$sql .= '1=1 ORDER BY unic_id DESC';
			}
			break;


	    default: break;
	}

	//if( !$tmp_flag ){


	//echo "sort:".$sort;
	//echo $sql;die;


$sql = $sql.' LIMIT '.$dd.', '.$s;

//file_put_contents('file4.txt', $sql);


$results = $wpdb->get_results($sql, ARRAY_A);

$whole = $wpdb->get_results("SELECT FOUND_ROWS() as total", ARRAY_A);
$tot = $whole[0][total];
$rec = count($results);


$html = '';
foreach( $results as $val ){
	error_log($val['body_status']);

	if( !is_array($val) ){ continue; }

	$html .= "<tr>\n";
	$html .= "<td class='cone sep_v'>\n";
	$html .= "<p class='line_pad db_text2'>\n";
	$html .= "<a class='db_text2' href='".get_bloginfo('url')."/info?donor_id=".$val['id']."&body_status=".strtolower($val['body_status'])."'>\n";
	$html .= $val['body_last_name'].', '.$val['body_first_name']."\n";
	$html .= "</a>\n</p>\n</td>\n<td class='ctwo sep_v'>\n";
	$html .= ( isset($val['unic_id']) && $val['unic_id'] > 0 ? "<p class='line_pad db_text2'>".$val['unic_id'].'</p>' : '' ); 
	$html .= "</td>\n<td class='cthree'>\n<p class='line_pad db_text2'>\n";
	$html .=  calcStatus($val['body_status']);
	$html .= "</p>\n</td>\n</tr>\n";

}

$pg = '';

if ($rec > 0) {
$quantity = $s; 
$limit=3;
$page= $dd/$s+1;
$num = $whole[0][total];
$num_result = count($results)-1;
if(!is_numeric($page)) $page=1;
if ($page<1) $page=1;
$pages = $num/$quantity;
$pages = ceil($pages);
$pages;

if ($page>$pages) $page = 1;
$pg .= "<p align=right><span class='sr_big'>Page # " . $page . ' of '.$pages."</span></p><br /><br />\n"; 

$pg .= "<span class='sr_big'>Pages</span><br />\n";


if ($page>1) {
$pg .= "<a class='db_text2' href='javascript:void(0)' onClick=".'"pagclick_sort ('.'1'.','."'".$sort."'".','."'".$tags."'".')"'."><<</a> &nbsp; ";
$pg .= "<a class='db_text2' href='javascript:void(0)' onClick=".'"pagclick_sort ('.($page-1).','."'".$sort."'".','."'".$tags."'".')"'.">< </a> &nbsp; ";
}
$start = $page-$limit;
$end = $page+$limit;
for ($j = 1; $j<=$pages; $j++) {
if ($j>=$start && $j<=$end) {
if ($j==($page)) $pg .= "<span class='sr_big_red'>".$j."</span> &nbsp; ";
              else $pg .= "<a class='db_text2' href='javascript:void(0)' onClick=".'"pagclick_sort ('.$j.','."'".$sort."'".','."'".$tags."'".')"'.">".$j.'</a> &nbsp; ';
    }
}

if ($j>$page && ($page+1)<$j) {
$pg .= "<a class='db_text2' href='javascript:void(0)' onClick=".'"pagclick_sort ('.($page+1).','."'".$sort."'".','."'".$tags."'".')"'."> ></a> &nbsp; ";
$pg .= "<a class='db_text2' href='javascript:void(0)' onClick=".'"pagclick_sort ('.$pages.','."'".$sort."'".','."'".$tags."'".')"'."> >></a> &nbsp; ";
}


}

// $html = htmlspecialchars($html);
echo json_encode(array("html"=>$html,"pg"=>$pg,"rec"=>$rec,"tot"=>$tot,"stype"=>$stype));

}

sortByDonors($_POST['n'], $_POST['sort'], $_POST['tags']);
?>
