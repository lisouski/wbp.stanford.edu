<?php

/*
 * Template Name: Query Admin
 */

wp_enqueue_style('wbp_style_formstyler2');
get_header();
?>

<div class="wrapper3">
    <div class="query_h_bd3">
        <div style="height:73px">
            <div class="header_left"><a href="<?php bloginfo('url'); ?>"></a></div>
            <div class="header_right">
            <a name="header"><div class="right_bd"></div></a>
            <div class="header_nav">
                <div class="nav_back">
                <a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>></a>
                </div>
                <div class="nav_home"><a href="<?php bloginfo('url'); ?>"></a></div>
            </div>
            </div>
            <div class="header_center"><?php echo strtoupper(get_bloginfo('name')); ?></div>
        </div><div class="clear"></div>

    <div style="text-align:center; margin:50px">
        <h2 style="margin:20px; font-family:'NewsGothicSemi'">SELECT TYPE OF REPORT</h2>
        <form id='mainform' action="<?php bloginfo('url'); ?>/queryr" method="post" >

            <div id="custom_options">
         <table width=100%><tr valign=top><td width=50%>
            <!-- Custom fields -->
            <!--
            - Name
            - Registration Date
            - Body Status
            - Body ID
            - Gender
            - Phone number
            - Email address
            - Date of death
            - Cause of death
            - Preparation
            - Body location
            - Date of cremation
            -->
                <div class="tc_line">
                    <h2 style="text-align:center; margin-top:20px; font-family:'NewsGothicDemi';">Fields</p>
                </div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroupall">
                    <input id="fld_all" type="checkbox" name="custom_all">
                </div>
                    <p class="tcl_p5">All Fields</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_1" class="fld" type="checkbox" name="custom_name" >
                </div>
                    <p class="tcl_p5">Name</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_2" class="fld" type="checkbox" name="custom_date_registration">
                </div>
                    <p class="tcl_p5">Registration Date</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_3" class="fld" type="checkbox" name="custom_body_status">
                </div>
                    <p class="tcl_p5">Body Status</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_4" class="fld" type="checkbox" name="custom_unic_id">
                </div>
                    <p class="tcl_p5">Body ID</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_5" class="fld" type="checkbox" name="custom_gender">
                </div>
                    <p class="tcl_p5">Gender</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_6" class="fld" type="checkbox" name="custom_address">
                </div>
                    <p class="tcl_p5">Address</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_7" class="fld" type="checkbox" name="custom_lives_beyond">
                </div>
                    <p class="tcl_p5">Lives Beyond</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_8" class="fld" type="checkbox" name="custom_agree_to_pay">
                </div>
                    <p class="tcl_p5">Agree to Pay</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_9" class="fld" type="checkbox" name="custom_phone">
                </div>
                    <p class="tcl_p5">Phone Number</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_10" class="fld" type="checkbox" name="custom_email">
                </div>
                    <p class="tcl_p5">Email Address</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_11" class="fld" type="checkbox" name="custom_date_of_death">
                </div>
                    <p class="tcl_p5">Date of Death</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_12" class="fld" type="checkbox" name="custom_cause_of_death">
                </div>
                    <p class="tcl_p5">Cause of Death</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_13" class="fld" type="checkbox" name="custom_date_of_birth">
                </div>
                    <p class="tcl_p5">Date of Birth</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_14" class="fld" type="checkbox" name="custom_sig_card_received">
                </div>
                    <p class="tcl_p5">Registration Form Received</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_15" class="fld" type="checkbox" name="custom_signature_date">
                </div>
                    <p class="tcl_p5">Signature Date</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_16" class="fld" type="checkbox" name="custom_how_about_us">
                </div>
                    <p class="tcl_p5">How did donor hear about us</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_17" class="fld" type="checkbox" name="custom_appreciation_received">
                </div>
                    <p class="tcl_p5">Wallet Card Sent</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_18" class="fld" type="checkbox" name="custom_withdrew">
                </div>
                    <p class="tcl_p5">Withdrew</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_19" class="fld" type="checkbox" name="custom_withdrew_reason">
                </div>
                    <p class="tcl_p5">Withdrew Reason</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_20" class="fld" type="checkbox" name="custom_contact_information">
                </div>
                    <p class="tcl_p5">Contact Information</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_21" class="fld" type="checkbox" name="custom_preparation">
                </div>
                    <p class="tcl_p5">Preparation</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_22" class="fld" type="checkbox" name="custom_body_location">
                </div>
                    <p class="tcl_p5">Body Location</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_23" class="fld" type="checkbox" name="custom_date_of_cremation">
                </div>
                    <p class="tcl_p5">Date of Cremation</p>
                </div><div class="clear"></div>


                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_24" class="fld" type="checkbox" name="custom_usages">
                </div>
                    <p class="tcl_p5">Body Usage</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                <div class="check_left_padding flgroup">
                    <input id="fld_25" class="fld" type="checkbox" name="custom_final">
                </div>
                    <p class="tcl_p5">Final Disposition</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
        
                <!-- Filter options -->
            </td><td td width=50%>
                <div class="tc_line">
                    <h2 style="text-align:center; margin-top:20px; font-family:'NewsGothicDemi';">Filters</p>
                </div><div class="clear"></div>



                <div class="tc_line">
                    <p class="tcl_p5">Name:</p>
                    <div class="select200" style="margin-left:20px">
                        <input class="tcl_input" type=text name=filter_name placeholder="Enter part of name if need">
                    </div>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                    <p class="tcl_p5">Body Status:</p>
                    <div class="select200" style="margin-left:20px; text-align: left;">
                       <p class="tcl_p14 bsgroupall"><input type=checkbox id="body_status_all"  name="filter_body_status0" value="all" checked> <span >All</span></p>
                       <p class="tcl_p14 bsgroup"><input type=checkbox id="body_status_1" class="fbodystatus" name="filter_body_status[]" value="registered"  checked> <span >Registered</span></p>
                       <p class="tcl_p14 bsgroup"><input type=checkbox id="body_status_2" class="fbodystatus" name="filter_body_status[]" value="withdrew"    checked> <span >Withdrew  </span></p>
                       <p class="tcl_p14 bsgroup"><input type=checkbox id="body_status_3" class="fbodystatus" name="filter_body_status[]" value="accepted"    checked> <span >Accepted  </span></p>
                       <p class="tcl_p14 bsgroup"><input type=checkbox id="body_status_4" class="fbodystatus" name="filter_body_status[]" value="rejected"    checked> <span >Rejected  </span></p>
                       <p class="tcl_p14 bsgroup"><input type=checkbox id="body_status_5" class="fbodystatus" name="filter_body_status[]" value="reception"   checked> <span >In Storage</span></p>
                       <p class="tcl_p14 bsgroup"><input type=checkbox id="body_status_6" class="fbodystatus" name="filter_body_status[]" value="usage"       checked> <span >In Use    </span></p>
                       <p class="tcl_p14 bsgroup"><input type=checkbox id="body_status_7" class="fbodystatus" name="filter_body_status[]" value="departed"    checked> <span >Departed  </span></p>
                    </div>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                    <p class="tcl_p5">Gender:</p>
                    <div class="select200" style="margin-left:60px; text-align: left;">
                       <p class="tcl_p14 fggroupall"> <input type=checkbox id="gender_all" name="filter_gender0" value="all"  checked> <span >All</span></p>
                       <p class="tcl_p14 fggroup"> <input type=checkbox id="gender_1" class="fgender" name="filter_gender[]" value="male"  checked> <span >Male</span></p>
                       <p class="tcl_p14 fggroup"> <input type=checkbox id="gender_2" class="fgender" name="filter_gender[]" value="female"  checked> <span >Female</span></p>
                       <p class="tcl_p14 fggroup"> <input type=checkbox id="gender_3" class="fgender" name="filter_gender[]" value="unknown"  checked> <span >Unknown</span></p>

                    </div>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                             <div class="tc_line" style="text-align: left;">
                    <p class="tcl_p4">Registration date:</p>
                    <div class="select200" style="margin-left:70px; text-align: left;">

                       <div class="tc_date"> <span class="tcl_p15" style="margin-right:16px;">From:</span><input id="datefrom" class="tcl_input customDate" style="width: 150px;" type="text" name="filter_from" value="01/01/1930" placeholder="MM/DD/YYYY" /></div>
                       <div class="tc_date"> <span class="tcl_p15" style="margin-right:35px;">To:</span><input id="dateto" class="tcl_input customDate" style="width: 150px;" type="text" name="filter_to" value="<?php echo date('m/d/Y'); ?>" placeholder="MM/DD/YYYY" /></div>
		       <p class="tcl_p14">Choose data if need</p> </p>
                    </div>
                </div><div class="clear"></div>

                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>




            </div>
     </td></tr></table>
            <div class="tc_submit" style="float:center">
                <!-- <input type="hidden" name="page_number" value="1" /> -->
                <button  class="btn_query_export" name="generate" style="margin:20px; " onClick="return val1()" >Generate</button>

                <button  class="btn_query_export" name="export" style="margin:20px 20px 20px 300px; " onClick="return val2()" >Export to Excel</button>

            </div>
        </form>
    </div>
    </div>
</div>
<script type="text/javascript" src="/ui/jquery.ui.datepicker.js"></script>
<script type='text/javascript'>


    $(document).ready(function(){
        $('#datefrom').datepicker({
            showOn: 'button',
            showButtonPanel: true,
            closeText: "Ok",
            minDate: new Date(1914, 1 - 1, 1),
            /*maxDate: new Date(),*/
            shortYearCutoff: 30,
            yearRange: '1914:2027',
            changeMonth: true,
            changeYear: true,
            buttonImage: '<?php bloginfo('url'); ?>/wp-content/themes/stanford-WBP/img/but_calendar2.png',
            buttonImageOnly: true,
            firstDay: 1,
onSelect: function() {
    $(this).data('datepicker').inline = true;
},
onClose: function(selectedDate) {
       if (selectedDate != '') {
        $( "#dateto" ).datepicker( "option", "minDate", selectedDate );
       }
    $(this).data('datepicker').inline = false;
}


        });


    });



    $(document).ready(function(){
        $('#dateto').datepicker({
            showOn: 'button',
            showButtonPanel: true,
            closeText: "Ok",
            minDate: new Date(1914, 1 - 1, 1),
            maxDate: new Date(),
            shortYearCutoff: 30,
            yearRange: '1914:2017',
            changeMonth: true,
            changeYear: true,
            buttonImage: '<?php bloginfo('url'); ?>/wp-content/themes/stanford-WBP/img/but_calendar2.png',
            buttonImageOnly: true,
            firstDay: 1,
onSelect: function() {
    $(this).data('datepicker').inline = true;
},
onClose: function(selectedDate) {
    $( "#datefrom" ).datepicker( "option", "maxDate", selectedDate );
    $(this).data('datepicker').inline = false;

}


        });


    });



$("#body_status_all").change(function(){
if ($("#body_status_all").prop('checked') == true)  {

$('.fbodystatus').prop('checked',true);
$(".bsgroup span").addClass('checked');

} else  {
$('.fbodystatus').prop('checked',false);
$(".bsgroup span").removeClass('checked');

}
});

    
$('.fbodystatus').change(function(){

var son = 0;
for (i=1; i<=7; i++) {
if ($("#body_status_"+i).prop('checked') == false) {
son = 1;
} 
if (son == 1) {
$("#body_status_all").prop('checked',false);
$(".bsgroupall span").removeClass('checked');
}else {
$("#body_status_all").prop('checked',true);
$(".bsgroupall span").addClass('checked');
}


}
});



$("#gender_all").change(function(){
if ($("#gender_all").prop('checked') == true)  {

$('.fgender').prop('checked',true);
$(".fggroup span").addClass('checked');

} else  {
$('.fgender').prop('checked',false);
$(".fggroup span").removeClass('checked');

}
});

    
$('.fgender').change(function(){

var gon = 0;
for (i=1; i<=3; i++) {
if ($("#gender_"+i).prop('checked') == false) {
gon = 1;
} 
if (gon == 1) {
$("#gender_all").prop('checked',false);
$(".fggroupall span").removeClass('checked');
}else {
$("#gender_all").prop('checked',true);
$(".fggroupall span").addClass('checked');
}


}
});

var nfon = 0;

$("#fld_all").change(function(){
if ($("#fld_all").prop('checked') == true)  {
nfon = 1;
$('.fld').prop('checked',true);
$(".flgroup span").addClass('checked');

} else  {
nfon = 0;
$('.fld').prop('checked',false);
$(".flgroup span").removeClass('checked');

}
});

    
$('.fld').change(function(){

var fon = 0;
nfon = 0;
for (i=1; i<=25; i++) {
if ($("#fld_"+i).prop('checked') == false) {
fon = 1;
} else {
nfon = 1;
}
if (fon == 1) {
$("#fld_all").prop('checked',false);
$(".flgroupall span").removeClass('checked');
}else {
$("#fld_all").prop('checked',true);
$(".flgroupall span").addClass('checked');
}

}
});


       function val1() {
      if ( nfon == 1 ) {
      document.getElementById('mainform').action = '<?php bloginfo('url'); ?>/queryr';
      document.getElementById('mainform').submit();
      }else {
      alert('Please select at least one parameter in the left column');
      return false;
      }

       }

       function val2() {
      if ( nfon == 1 ) {
      document.getElementById('mainform').action = '<?php bloginfo('url') ?>/export-xls';
      document.getElementById('mainform').submit();
      }else {
      alert('Please select at least one parameter in the left column');
      return false;
      }

       }



</script>

<?php
get_footer();
?>
