<!--<H1>COMMENTS.PHP</H1>-->
<h2>Список самых положительных комментариев <img src="<?php bloginfo('template_url'); ?>/images/icons/smile.gif" alt="smile" title="smile" width="30" /></h2>
<?php
/*	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die();

        if (!empty($post->post_password)) {
            if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {
?>
				
		<p class="nocomments">Эта запись защищена паролем.<p>
				
<?php
		return;
            }
        }
*/
	$oddcomment = 'alt';
?>
<div id="commentlist">
	<?php if ($comments) : ?>
	<?php foreach ($comments as $comment) : ?>
	<span>
		<div class="<?php echo $oddcomment; ?> comment" id="comment-<?php comment_ID() ?>">
		<h3><?php comment_author_link() ?> <span>[ <?php comment_date('dMy') ?>]</span></h3>
			<?php if ($comment->comment_approved == '0') : ?>
			<em>Ваш комментарий ожидает модерации.</em>
			<?php endif; ?>
			<?php comment_text() ?>
		</div>
		</span>
	<?php
		if ('alt' == $oddcomment) $oddcomment = '';
		else $oddcomment = 'alt';
	?>
	<?php endforeach; /* end for each comment */ ?>
	</div>
	<?php else : ?>
	<?php if ('open' == $post->comment_status) : ?>
	<div id="hidelist" style="display:none"></div>
	</div>
	<?php else : ?>
	<div style="display:none"></div>
</div>
<p>Комментирование запрещено.</p>
<?php endif; ?>
<?php endif; ?>
<?php if ('open' == $post->comment_status) : ?>

<div class="post">
  <div id="loading" style="display: none;">Оставьте комментарий:</div>
    <h2 id="respond">Оставьте комментарий:</h2>
    <?php if ( get_option('comment_registration') && !$user_ID ) : ?>
	    <p>Вы должны быть <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">залогинены</a> для оставления комментариев.</p>
    <?php else : ?>
  <form id="commentform" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
	  <?php if ( $user_ID ) : ?>
	  <p>
		  Вы вошли как <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. 
		  <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Выйти">Выйти &raquo;</a>
	  </p>
	  <?php else : ?>
	  <input type="text" name="author" class="input1" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" />
	  <label for="author"><small>Имя <?php if ($req) echo "(required)"; ?></small></label><br/><br/>
	  <input type="text" name="email" class="input1" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" />
	  <label for="email"><small>Почта (не публикуется) <?php if ($req) echo "(обязательно)"; ?></small></label><br/><br/>
	  <?php endif; ?>
	  <small><strong>XHTML:</strong> <?php echo allowed_tags(); ?></small><br/><br/>
	  <textarea name="comment" id="comment" class="input1" cols="45" rows="10" tabindex="4"></textarea><br/><br/>
	  <input name="submit" type="submit" id="submit" class="button" tabindex="5" value="Отправить" />
	  <input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
	  <br/><br/>
	  <?php do_action('comment_form', $post->ID); ?>
  </form>
  <?php endif; ?>
  <?php endif; ?>
</div>
