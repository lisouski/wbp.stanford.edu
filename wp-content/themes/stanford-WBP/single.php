<?php get_header(); ?>

<?php get_sidebar(); ?>

	<div id="content">

		<!--<H1>SINGLE.PHP</H1>-->
<?php
		if(have_posts()) {
			
			the_category();

			while(have_posts()) {
				
				the_post();
?>
				<div class="post" id="post-<?php the_ID(); ?>" itemscope itemtype="http://data-vocabulary.org/Review">

					<div class="entry">
<?php
						echo '<h3>'; the_title(); echo '</h3>';

						echo '<p>';
						    the_content('Читать далее &raquo');
						echo '</p>';
?>
					</div>

				</div>
<?php
			}
		}else {
?>
			<h2 align="center">Не найдено</h2>
			<p align="center">Извините, ничего не найдено.</p>

<?php	
		}
?>

<!--<input id="inp" type="button" style="width: 300px" onmouseover="this.style.width = parseInt(this.style.width)-10+'px'" value="Укоротить на 10px" />-->

<script>

/*example function "cou_t"
  function cou_t(){
    for(var i=0; i<3; i++){
      alert('текст '+i);
    }
  }
*/  
//   cou_t();						//вызов ф-ии
//   alert(inp);					//вывод типа данных переменной "inp", т.к она  - инпут, еслиб у нее было значение - вывело бы его
// var pr = prompt('my age', '?');			//выводит титульник запроса и предлагает пользовательский ввод(ок/отмена)
// alert('mne'+pr+'let');				
// var isAdmin = confirm('i`m is admin?');		//в переменную ложит значение и предлагает да/нет(вернет true/false)
//  alert(isAdmin);
/*тернарный оператор "?"
var a = 2;
var b = 2;
var res = (a+b<4)?'true':'false';
alert(res);
*/
/*
var a = prompt('чему равно "a"', 1);
var res = (a==1)?'value_1':(a==2)?'value_2':(a==3)?'value_3':'какое-то большое значение';
alert(res);
*/

</script>

	</div>

<?php get_footer(); ?>