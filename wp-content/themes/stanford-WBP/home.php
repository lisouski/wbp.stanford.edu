<?php
/*
Template Name: Home Page
*/
?>

<?php
global $wpdb;
$wpdb->show_errors();

get_header();

//get_sidebar();
?>

	<div id="content">

		<H1>HOME.PHP</H1>
<?php
if( is_user_logged_in() ){
?>
	<!--<a href="#">DATABASE</a><br />
	<a href="#">REGISTRATION</a><br />
	<a href="#">ACCEPTANCE</a><br />
	<a href="#">RECEPTION</a><br />
	<a href="#">USAGE</a><br />
	<a href="#">DEPARTURE</a><br />-->
<?php
/*$args = array(
    'depth'        	=> 0,
    'show_date'    => '',
    'date_format'  => get_option("date_format"),
    'child_of'     => 0,
    'exclude'      => '',
    'include'      => '',
    'title_li'     => '',
    'echo'         => 1,
    'authors'      => '',
    'sort_column'  => 'menu_order, post_title',
    'sort_order'  => 'ASC',
    'link_before'  => '',
    'link_after'   => '',
    'exclude_tree'   => '',
    'meta_key'   => '',
    'meta_value'   => '',
    'number '   => '',
    'offset '   => '',
    'walker' =>  
); //*/

wp_list_pages();
?>
<?php
}
?>
		
<?php

// Call content from static page "Home"
if( have_posts() ){
  while( have_posts() ){
    the_post();
    the_content();
  }
}
//*/
?>

	</div>

<?php get_footer(); ?>