<?php
/*
 * Template Name: Edit
 */

 /**
 * �� ��http://4wbp.vex/edit/?donor_id=6)
 */
 wp_enqueue_style('wbp_style_formstyler2');

 get_header();

 global $wpdb;

 if (isset($_POST['ser_data']) && $_POST['ser_data']){
    $data = unserialize(stripslashes($_POST['ser_data']));
}
else {
    if (isset($_GET['donor_id']) && $_GET['donor_id']) {
        $table_data = 'wp_wbp_donors_data';
        $donor_id = (int)trim($_GET['donor_id']);
        $sql = 'SELECT * FROM '.$table_data.' WHERE id = "'.$donor_id.'"';
        $results = $wpdb->get_results($sql, ARRAY_A);
        $data = $results['0'];
        $data['address'] = unserialize(stripslashes($data['address']));
        $data['body_address'] = unserialize(stripslashes($data['body_address']));
        $data['time_of_death'] = unserialize(stripslashes($data['time_of_death']));
        $data['accept_optional_info'] = unserialize(stripslashes($data['accept_optional_info']));
        $data['accept_don_check'] = unserialize(stripslashes($data['accept_don_check']));
        $data['reception'] = unserialize(stripslashes($data['reception']));
        $data['usages'] = unserialize(stripslashes($data['usages']));
        $data['final'] = unserialize(stripslashes($data['final']));

        // Find out the edit page to load
        $next_status = 'registered';
        if (isset($_GET['body_status']) && $_GET['body_status']) {
            $next_status = $_GET['body_status'];
        }
    }
    else {
        $data['id'] = '';
        $data['user_name'] = '';
        $data['fired'] = '';
        $data['date_registration'] = '';
        $data['body_first_name'] = '';
        $data['body_last_name'] = '';
        $data['body_status'] = '';
        $data['body_location'] = '';
        $data['body_address'] = '';
        $data['birth_day'] = '';
        $data['birth_month'] = '';
        $data['birth_year'] = '';
        $data['date_of_death'] = '';
        $data['cause_of_death'] = '';
        $data['time_of_death'] = '';
        $data['gender'] = '';
        $data['lives_beyond'] = '';
        $data['agree_to_pay'] = '';
        $data['sig_card_received'] = '';
        $data['signature_date'] = '';
        $data['how_about_us'] = '';
        $data['appreciation_received'] = '';
        $data['withdrew'] = '';
        $data['reason'] = '';
        $data['accept_don_check'] = '';
        $data['reception'] = '';
        $data['street'] = '';
        $data['city'] = '';
        $data['state'] = '';
        $data['zip_code'] = '';
        $data['phone_number'] = '';
        $data['email'] = '';
        $data['kin_first_name'] = '';
        $data['kin_last_name'] = '';
        $data['kin_status'] = '';
        $data['kin_phone_number'] = '';
        $data['kin_email'] = '';
        $data['kin_street'] = '';
        $data['kin_city'] = '';
        $data['kin_state'] = '';
        $data['kin_zip_code'] = '';
        $data['phys_id'] = '';
        $data['phys_first_name'] = '';
        $data['phys_last_name'] = '';
        $data['phys_phone_number'] = '';
        $data['phys_email'] = '';
        $data['phys_street'] = '';
        $data['phys_city'] = '';
        $data['phys_state'] = '';
        $data['phys_zip_code'] = '';
    }
}
?>

<link rel="stylesheet" href="<?php bloginfo('url');?>/themes/base/jquery.ui.all.css">
<script type="text/javascript" src="<?php bloginfo('url');?>/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="<?php bloginfo('url');?>/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="<?php bloginfo('url');?>/ui/jquery.ui.widget.js"></script>
<script type='text/javascript'>
    // Global variables
    var globalDataArray = <?php echo json_encode($data); ?>;

    <?php $time = strtotime("-103 year", time());  $y_date = date("Y", $time);?>

    function initDatePickers(){
        $('.customDate').datepicker({
            showOn: 'button',
            showButtonPanel: true,
            closeText: "Ok",
            minDate: new Date(<?php echo $y_date; ?>, 1 - 1, 1),
            /*maxDate: new Date(),*/
            shortYearCutoff: 30,
            yearRange: '<?php echo $y_date.":".date("Y"); ?>',
            changeMonth: true,
            changeYear: true,
            buttonImage: '<?php bloginfo('template_url'); ?>/img/but_calendar2.png',
            buttonImageOnly: true,
            firstDay: 1,
            onSelect: function() {
                $(this).data('datepicker').inline = true;
            },
            onClose: function() {
                $(this).data('datepicker').inline = false;
            }
        });
    }

    $(document).ready(function(){
        initDatePickers();
    });


jQuery(function($){
$.mask.definitions['~']='[01]';
$.mask.definitions['!']='[0123]';
$.mask.definitions['=']='[12]';

$("#date_of_birth").mask("~9/!9/=999",{placeholder:"MM/DD/YYYY"});
$("#date_of_death").mask("~9/!9/=999",{placeholder:"MM/DD/YYYY"});
$("#date_of_reception").mask("~9/!9/=999",{placeholder:"MM/DD/YYYY"});
$("#date_of_preparation").mask("~9/!9/=999",{placeholder:"MM/DD/YYYY"});
$("#date_of_preparation2").mask("~9/!9/=999",{placeholder:"MM/DD/YYYY"});
$("#date_of_final").mask("~9/!9/=999",{placeholder:"MM/DD/YYYY"});
$(".phone-mask").mask("(999) 999-9999");

});

function isValidDate(s,a) {
if (!isValidDate2(s))  {
alert (a);
} 
}

function isValidDate2(s) {
  var bits = s.split('/');
  var y = bits[2], m  = bits[0], d = bits[1];
  if (y < 1913  || y > <?php echo date("Y"); ?>) {
  return false;
  }
  // Assume not leap year by default (note zero index for Jan)
  var daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];

  // If evenly divisible by 4 and not evenly divisible by 100,
  // or is evenly divisible by 400, then a leap year
  if ( (!(y % 4) && y % 100) || !(y % 400)) {
    daysInMonth[1] = 29;
  }
  return d <= daysInMonth[--m]
}

</script>

<div class="wrapper6">

    <?php $tabToOpen = $_GET['tab']; ?>

    <?php wbpGetSmallHeaderBlock(); ?>

    <div class="name_line">
        <?php
//echo '<pre>'; print_r($data); die;
        if(!isset($data['body_status']) || $data['body_status'] == ''){
            ?>
            <p id="new_file" class="tcl_name" style="float:left; padding-left:10px;">New File</p>
            <?php } ?>
            <div class="tc_line2">
                <?php
                echo '<div style="float:left; font-size:20px; padding-right:20px;">';
                echo '<a id="donor_name" class="name_link" href="';
                echo bloginfo('url');
                echo '/info/?donor_id='.$data['id'].'&body_status='.$data['body_status'].'">';
                    echo ($data['body_last_name'] ? $data['body_last_name'] : '');
                    echo ($data['body_last_name'] && $data['body_first_name'] ? ', ' : '');
                    echo ($data['body_first_name'] ? $data['body_first_name'] : '');
                echo '</a></div>';

                echo '<div id="donor_gender" ';
                echo ($data['gender'] == 'male' ? 'class="gender_m"' : ($data['gender'] == 'female' ? 'class="gender_f"' : ($data['gender'] == 'unknown' ? 'class="gender_u"' :'') ) );
                echo '></div>';

                $display_stat = isset($data['body_status']) && $data['body_status'] ? $data['body_status'] : '';
                if($display_stat == 'reception' ) $display_stat = 'In Storage';
                elseif( $display_stat == 'usage' ) $display_stat = 'In Use';
                echo '<div id="donor_body_status" style="padding-top:3px;">';
                echo (isset($display_stat) && $display_stat ? ' - '.$display_stat : '');
                echo '</div>';
                ?>
            </div>
            <div class="clear"></div>
            <div class="tc_sep">
                <div class="tc_sep_left"></div>
                <div class="tc_sep_right"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="t_cont">
            <dl class="tabs">
                <div id="blocker" style="position:absolute; top:0; left:0; height:45px; width:100%; z-index:3; opacity:0.5; background:white;"></div>
                <?php
/*$arr_status = array('registered', 'accepted', 'reception', 'usage', 'departed');
if( ($key = array_search($data['body_status'], $arr_status)) != false ){
        if( $key < (count($arr_status) - 1) ){
                $next_status = $arr_status[$key + 1];
        }else{
                $next_status = $arr_status[$key];
        }
    }*/
    ?>

    <!--REGISTER DONOR TAB -->
    <?php $isRegisterDonorTabOpen = ( isset($_POST['form_reg']) || (isset($_POST['form_reg']) && isset($data['body_status']) && ($data['body_status'] == 'withdrew' || $data['body_status'] == 'registration') || ($next_status == 'registered')) || (empty($_POST) && empty($_GET))) && ($tabToOpen != 'donor_information')?>
    <dt id="register_donor_tab" class="tabs_dt<?php echo $isRegisterDonorTabOpen ? ' selected' : '' ; ?>">Register donor</dt>
    <dd class="tabs_dd<?php echo $isRegisterDonorTabOpen ? ' selected' : '' ; ?>">
        <div class="tab-content">
            <div class="tc6">
  <span class="attool">Attention! after filling in all mandatory fields (First Name; Last Name; Address; City; ZIP) navigate to next tab or click SAVE button at the bottom of the form to save information.</span>
                <form id="register_form" onsubmit="return false" enctype="multipart/form-data">
                    <!--line-->
                    <div class="tc_line">
                        <p class="tcl_p">First Name:<span class="astool">*</span></p>
                        <input class="tcl_input" type="text" name="body_first_name"
                        value="<?php echo $data['body_first_name']; ?>" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50" required="required" />
                    </div><div class="clear"></div>
                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                    <!--line-->
                    <div class="tc_line">
                        <p class="tcl_p">Last Name:<span class="astool">*</span></p>
                        <input class="tcl_input" type="text" name="body_last_name"
                        value="<?php echo $data['body_last_name']; ?>" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50" required="required" />
                    </div><div class="clear"></div>
                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                    <!--line-->
                    <div class="tc_line">
                        <p class="tcl_p">Date of Birth:</p>
                        <div class="tc_date">

                            <span>
                                <input style="margin-right:20px;" class="tcl_input customDate" type="text" name="date_of_birth" id="date_of_birth"   onChange="isValidDate(this.value,'Wrong Date of Birth');"
                                value="<?php echo isset($data['date_of_birth']) ? $data['date_of_birth'] : ''; ?>" />
                            </span>
                                 <input type='hidden' name="date_of_birth_sec" id="date_of_birth_sec"  value="<?php echo isset($data['date_of_birth']) ? $data['date_of_birth'] : ''; ?>" >
                                    <!--<p>Month:</p>
                                    <input class="w130" type="text" name="birth_day" value="<?php echo $data['birth_day']; ?>" pattern="[0-9]{1,2}" maxlength="2" required="required" />
                                    <p>/ Day:</p>
                                    <input class="w25" type="text" name="birth_month" value="<?php echo $data['birth_month']; ?>" pattern="[0-9]{1,2}" maxlength="2" required="required" />
                                    <p>/  Year:</p>
                                    <input class="w45" type="text" name="birth_year" value="<?php echo $data['birth_year']; ?>" pattern="[0-9]{4}" maxlength="4" required="required" />
                                -->
                            </div>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <p class="tcl_p">Gender:</p>
                            <div class="select120">
                                <select  name="gender">
                                    <option value="">Select One</option>
                                    <option value="male"<?php echo isset($data['gender']) && $data['gender'] == 'male' ? ' selected="selected"' : ''; ?>>Male</option>
                                    <option value="female"<?php echo isset($data['gender']) && $data['gender'] == 'female' ? ' selected="selected"' : ''; ?>>Female</option>
                                    <option value="unknown"<?php echo isset($data['gender']) && $data['gender'] == 'unknown' ? ' selected="selected"' : ''; ?>>Unknown</option>
                                </select>
                            </div>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <p class="tcl_p">Address:<span class="astool">*</span></p>
                            <input class="tcl_input" type="text" name="address[street]"
                            value="<?php echo htmlspecialchars($data['address']['street']); ?>" pattern="[^!@$%<>]{1,150}" maxlength="150" required="required" />
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <p class="tcl_p">City:<span class="astool">*</span></p>
                            <input class="tcl_input" type="text" name="address[city]"
                            value="<?php echo htmlspecialchars($data['address']['city']); ?>" pattern="[^!@$%<>]{1,50}" maxlength="50" required="required" />
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <p class="tcl_p">State:</p>
                            <div class="select80">
                                <?php
                                $states = array(
                                    'AK', 'AL', 'AR', 'AZ', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'IA', 'ID', 'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME', 'MI', 'MN', 'MO', 'MS',
                                    'MT', 'NC', 'ND', 'NE', 'NH', 'HJ', 'NM', 'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VA', 'VT', 'WA', 'WI', 'WY', 'DC'
                                    );
                                    ?>
                                    <select name="address[state]">
                                        <option value="">Select One</option>
                                        <?php 
 if (isset($data['address']['state']) && $data['address']['state']) {
  echo '<option value="'.$data['address']['state'].'" selected="selected">'.strtoupper($data['address']['state']).'</option>';
 } else {
  echo '<option value="CA" selected="selected">CA</option>';   
 }
                                           ?>

                                        <?php
                                        foreach( $states as $state ){
                                            if( $state == $data['address']['state'] || $state == $def_state ){
                                                continue;
                                            }
                                            echo '<option value="'.$state.'"';

                                            echo ' >'.$state.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            <!--line-->
                            <div class="tc_line">
                                <p class="tcl_p">Zip Code:<span class="astool">*</span></p>
                                <input class="tcl_input w60" type="text" name="address[zip_code]"
                                value="<?php echo $data['address']['zip_code']; ?>" pattern="[0-9]{1,5}" maxlength="5" required="required" />
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            <!--line-->
                            <div class="tc_line">
                                <?php
                                    $jsonDistancematrix = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Stanford,CA&destinations='.urlencode($data['address']['city']).','.strtoupper($data['address']['state']));
                                    $objDistancematrix = json_decode($jsonDistancematrix);
                                    $miles = $objDistancematrix->rows[0]->elements[0]->distance->text;
                                ?>
                                <p class="tcl_p w_auto">Miles Away:</p>
                                <p class="tcl_p2"><?php echo $miles ?></p>
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            <!--line-->
                            <div id="div_agree_to_pay">
                                <div class="tc_line">
                                    <?php
//echo '<pre>'; print_r($data); die;
                                    ?>
                                    <p id="agree_to_pay_text" class="tcl_p w_auto">Agrees to pay for transit of donor's body to Stanford</p>
                                    <div class="check_padding">
                                        <input id="agree_to_pay" type="checkbox" name="agree_to_pay" value="yes" <?php echo isset($data['agree_to_pay']) && $data['agree_to_pay'] == 'yes' ? 'checked="checked"' : '' ; ?> />
                                    </div>
                                </div><div class="clear"></div>
                                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            </div>
                            <!--line-->
                            <!--div id="blocker" style="position:absolute; top:0; left:0; height:45px; width:100%; z-index:2; opacity:0.5; background:white;"></div-->
<script>
    jQuery(document).ready(function(){
        //* �"agrees to pay" ���"lives beyond" �       if( $('#lives_beyond_sel option:selected').val() == 'yes' ){
                $('#div_agree_to_pay').css({'display':'block'});
        }else if( $('#lives_beyond_sel option:selected').val() == 'no' ){
                $('#div_agree_to_pay').css({'display':'none'});
        }
        //* �"agrees to pay" ���"lives beyond" �
        $('#lives_beyond_sel').change(function() {
            if( $('#lives_beyond_sel option:selected').val() == 'yes' ){
                $('#agree_to_pay').attr({checked:'checked', value:'yes'});
                $('.check_padding > #agree_to_pay + span.checkbox').addClass('checked');
                $('#div_agree_to_pay').css({'display':'block'});
            }
            if( $('#lives_beyond_sel option:selected').val() == 'no' ){
                $('#agree_to_pay').attr({checked:'', value:''});
                $('.check_padding > #agree_to_pay + span.checkbox').removeClass('checked');
                $('#div_agree_to_pay').css({'display':'none'});
            }
        });

        /*if( $('#agree_to_pay').prop('checked') == false ){
                $('#agree_to_pay_text').text('Located beyond 100 mile radius and unwilling to pay for transit.');
        }//*/

        /*$('#blocker').on('click', function(){
                if( confirm('Are you sure you want to leave this page?') ){
                        $('#blocker').css({'display':'none'});
                }
        });//*/

        /*$('.tabs_dt').on('click', function(){
                if( confirm('Are you sure you want to leave this page?') == false ){
                        return false;
                }
        });//*/

        // Make withdraw reason field visible only when withdraw checkbox is ticked
        // Else do not display the reason field
        if( $('#withdraw_checkbox input').prop('checked') ) {
            $('#withdraw_reason').css({'display':'block'});
            $('#withdraw_status').val('withdrew');
            $('#withdrew').val('withdrew');
        }
        else {
            $('#withdraw_reason').css({'display':'none'});
            $('#withdraw_status').val('registered');
            $('#withdrew').val('registered');
        }

        $('#withdraw_checkbox').change(function() {
            if( $('#withdraw_checkbox input').prop('checked') ){
                $('#withdraw_reason').css({'display':'block'});
                $('#withdraw_status').val('withdrew');
                $('#withdrew').val('withdrew');
            }
            else {
                $('#withdraw_reason').css({'display':'none'});
                $('#withdraw_status').val('registered');
                $('#withdrew').val('registered');
            }
        });
    });

        /*$(function(){
            var cur_tab_name = $('.tabs_dt.selected').text();
            $('.tabs_dt').on('mousedown', function(){
                alert('Please save current changes: '+cur_tab_name);
                cur_tab_name = $('.tabs_dt.selected').text();
            });
    });//*/

</script>

<div class="tc_line">

    <?php
        $address_phone_value = '';
        if($data['address']['phone']){
            $address_phone_value = $data['address']['phone'];
        }else{
            $address_phone_value = ($data['address']['first_phone'].$data['address']['sec_phone'].$data['address']['third_phone']);
        }
    ?>
    <p class="tcl_p w_auto">Phone Number:</p>
    <input id="qwqw11" class="tcl_input group1 phone-mask" type="text" name="address[phone]" value="<?php echo $address_phone_value ?>"/>
    </div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


<div class="tc_line" style="display: none">
    <p class="tcl_p w_auto">Phone Number:</p>
<!--    <input id="qwqw14" class="tcl_input w35 group1" type="number" name="address[opt_phone]" value="<?php echo ( isset($data['address']['opt_phone']) ?
    $data['address']['opt_phone'] : '' ); ?>" pattern="[0-9]{1,3}" maxlength="3" />   //-->
    <input id="qwqw11" class="tcl_input w35 group1" type="number" name="address[first_phone]" value="<?php echo ( isset($data['address']['first_phone']) ?
    $data['address']['first_phone'] : '' ); ?>" pattern="[0-9]{3}" maxlength="3" />
    <!--p class="tcl_p dash">-</p-->
    <input id="qwqw12" class="tcl_input w35 group1" type="number" name="address[sec_phone]" value="<?php echo ( isset($data['address']['sec_phone']) ?
    $data['address']['sec_phone'] : '' ); ?>" pattern="[0-9]{3}" maxlength="3" />
    <!--p class="tcl_p dash">-</p-->
    <input id="qwqw13" class="tcl_input w45 group1" type="number" name="address[third_phone]" value="<?php echo ( isset($data['address']['third_phone']) ?
    $data['address']['third_phone'] : '' ); ?>" pattern="[0-9]{4}" maxlength="4" />
    <!--p class="tcl_p dash"> optional:</p-->

</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

<?php
// �reg_new_optional_phone
if( isset($data['address']['reg_new_optional_phone']) && is_array($data['address']['reg_new_optional_phone']) && !empty($data['address']['reg_new_optional_phone']) ){

//echo '<pre>'; print_r($data['address']); die;

    $optional_reg_new_phone = count($data['address']['reg_new_optional_phone']);
    foreach( $data['address']['reg_new_optional_phone'] as $k => $v ){
        if( empty($v['name']) ){
            unset($data['address']['reg_new_optional_phone'][$k]);
            $data['address']['v'] = array_values($data['address']['reg_new_optional_phone']);
            continue;
        }
        ?>

        <?php
            $address_additional_phone_value = '';
            if($v['phone']){
                $address_additional_phone_value = $v['phone'];
            }else{
                $address_additional_phone_value = ($v['first_phone'].$v['sec_phone'].$v['third_phone']);
            }
        ?>

        <div class="tc_line inserted">
            <input class="tcl_input group_1_<?php echo $k; ?>" type="text" name="address[reg_new_optional_phone][<?php echo $k; ?>][name]" pattern="[a-zA-Z0-9\s\-]{0,15}" maxlength="15"
                   value="<?php echo ( isset($v['name']) ? $v['name'] : '' ); ?>" />
            <input class="phone-mask tcl_input group_1_<?php echo $k; ?>" type="text" name="address[reg_new_optional_phone][<?php echo $k; ?>][phone]"
                   value="<?php echo $address_additional_phone_value; ?>" />
        </div><div class="clear"></div>
        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
        <!--line-->
        <div class="tc_line inserted" style="display: none">
            <input class="tcl_input w35 group_1_<?php echo $k; ?>" type="number" name="address[reg_new_optional_phone][<?php echo $k; ?>][first_phone]" pattern="[0-9]{3}" maxlength="3"
            value="<?php echo ( isset($v['first_phone']) ? $v['first_phone'] : '' ); ?>" />
            <p class="tcl_p dash">-</p>
            <input class="tcl_input w35 group_1_<?php echo $k; ?>" type="number" name="address[reg_new_optional_phone][<?php echo $k; ?>][sec_phone]" pattern="[0-9]{3}" maxlength="3"
            value="<?php echo ( isset($v['sec_phone']) ? $v['sec_phone'] : '' ); ?>" />
            <p class="tcl_p dash">-</p>
            <input class="tcl_input w45 group_1_<?php echo $k; ?>" type="number" name="address[reg_new_optional_phone][<?php echo $k; ?>][third_phone]" pattern="[0-9]{4}" maxlength="4"
            value="<?php echo ( isset($v['third_phone']) ? $v['third_phone'] : '' ); ?>" />
            <p class="tcl_p dash"> optional:</p>
            <input class="tcl_input w45 group_1_<?php echo $k; ?>" type="number" name="address[reg_new_optional_phone][<?php echo $k; ?>][opt_phone]" pattern="[0-9]{1,6}" maxlength="6"
            value="<?php echo ( isset($v['opt_phone']) ? $v['opt_phone'] : '' ); ?>" />
        </div>
        <!--/line-->
        <script>
            $(document).ready(function(){
                $('.group_1_<?php echo $k; ?>').groupinputs();
            });
        </script>
        <?php
    }
}
?>
<div id="add_reg_new_phone_body">

</div>

<div class="tc_line" style="border-bottom:1px solid grey;">
    <p class="tcl_p5">
        <input id="add_reg_new_opt_numb" type="button" value="ADD PHONE NUMBER" name="add_reg_new_opt_numb" />
    </p>
</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<!--line-->
<div class="tc_line">
    <p class="tcl_p">E-mail:</p>
    <input class="tcl_input" type="email" name="address[email]" value="<?php echo isset($data['address']['email']) ? $data['address']['email'] : ''; ?>" maxlength="100" />
</div>
<div class="clear"></div>
<div class="tc_sep">
    <div class="tc_sep_left"></div>
    <div class="tc_sep_right"></div>
</div>
<div class="clear"></div>
<!--line-->
<div class="tc_line">
    <p class="tcl_p w240">Registration Form Received?</p>
    <div class="select80">
        <select name="sig_card_received" >
            <option value="">Select One</option>
            <option value="no"<?php echo isset($data['sig_card_received']) && $data['sig_card_received'] == 'no' ? ' selected="selected"' : ''; ?>>No</option>
            <option value="yes"<?php echo isset($data['sig_card_received']) && $data['sig_card_received'] == 'yes' ? ' selected="selected"' : ''; ?>>Yes</option>
        </select>
    </div>
</div><div class="clear"></div>

<!--line-->
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                    <div class="tc_line">
                        <p class="tcl_p">Signature Date:</p>
                        <div class="tc_date">

                            <span>
                                <input style="margin-right:20px;" class="tcl_input customDate" type="text" name="signature_date" id="signature_date"   onChange="isValidDate(this.value,'Wrong Date of Registration');"
                                value="<?php echo isset($data['signature_date']) ? $data['signature_date'] : ''; ?>" />
                            </span>
                                 <input type='hidden' name="signature_date_sec" id="signature_date_sec"  value="<?php echo isset($data['signature_date']) ? $data['signature_date'] : ''; ?>" >

                            </div>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

<!--line-->

                        <div class="tc_line">
    <p class="tcl_p w240">How did donor hear about us?:</p>
    <input class="tcl_input" style="width: 450px;" type="email" name="how_about_us" value="<?php echo isset($data['how_about_us']) ? $data['how_about_us'] : ''; ?>" maxlength="100" />
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>



<!--line-->
<div class="tc_line">
    <p class="tcl_p w240">Wallet Card Sent?</p>
    <div class="select80">
        <select name="appreciation_received" >
            <option value="">Select One</option>
            <option value="no"<?php echo isset($data['appreciation_received']) && $data['appreciation_received'] == 'no' ? ' selected="selected"' : ''; ?>>No</option>
            <option value="yes"<?php echo isset($data['appreciation_received']) && $data['appreciation_received'] == 'yes' ? ' selected="selected"' : ''; ?>>Yes</option>
        </select>
    </div>
</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<!--line-->
<div class="tc_line" id="withdraw_checkbox">
    <div class="check_left_padding">
        <input type="checkbox" id="withdrew" name="withdrew" <?php echo isset($data['body_status']) && $data['body_status'] == 'withdrew' ? 'checked="checked"' : ''; ?> value="withdrew" />
        <input type="hidden" id="withdraw_status" name="withdraw_status" value="" />
    </div>
    <p class="tcl_p">Withdrew</p>
</div>
<div class="clear"></div>
<!--line-->
<div class="tc_line" id="withdraw_reason">
    <p class="tcl_p w_auto">Reason:</p>
    <div class="select120">
        <select name="reason" >
            <option value="">Select One</option>
            <option value="Distance"<?php echo isset($data['reason']) && $data['reason'] == 'Distance' ? ' selected="selected"' : ''; ?>>Distance</option>
            <option value="Personal"<?php echo isset($data['reason']) && $data['reason'] == 'Personal' ? ' selected="selected"' : ''; ?>>Personal</option>
            <option value="Family"<?php echo isset($data['reason']) && $data['reason'] == 'Family' ? ' selected="selected"' : ''; ?>>Family</option>
            <option value="Other"<?php echo isset($data['reason']) && $data['reason'] == 'Other' ? ' selected="selected"' : ''; ?>>Other</option>
        </select>
    </div>
</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<?php
if(!isset($data['body_status']) || $data['body_status'] == ''){
    ?>
    <input type="hidden" name="body_status" value="registered" />
    <?php
} else {
    ?>
    <!--line-->
    <div class="tc_line2">
        <p class="tcl_p w_auto">Record Created:</p>
        <?php $data['date'] = substr($data['date_registration'], 0, strpos($data['date_registration'], ' ')); ?>
        <p class="tcl_p2"><?php echo $data['fired'] == '' ? $data['user_name'].' '.$data['date'] : '"User Fired" '.$data['date']; ?></p>
    </div>
    <div class="clear"></div>
    <div class="tc_sep">
        <div class="tc_sep_left"></div>
        <div class="tc_sep_right"></div>
    </div>
    <div class="clear"></div>
    <?php
}
?>
<div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<div class="clear"></div>
<?php
if ($data['register_file']) {
?>
<div class="tc_line">
                        <p class="tcl_p">Existing File:</p>
    <input type="hidden" name="register_file_exist" value="<?php echo $data['register_file']; ?>" />
<p class="tcl_p3"><a target=_blank href='<?php echo WP_HOME; ?>/upload/<?php echo $data['register_file']; ?>' ><?php echo $data['register_file']; ?></a></p>
                    </div>

<?php
}
?>



                    <div class="tc_line">
                        <p class="tcl_p">Upload File:</p>
<input class="tcl_input" type="file" name="register_file" id="register_file" />
                    </div>

  <span class="attool">All fields marked with * are required!</span>

<div class="tc_submit">
    <input type="hidden" name="donor_id" value="<?php echo ( $data['id'] ? $data['id'] : '' ); ?>" />
    <input type="hidden" name="form_reg" value="reg" />
    <a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>>
        <input type="button" value="cancel" name="" />
    </a>
    <input type="submit" id="btn_register_save" value="save" name="" />
    <input type="submit" id="btn_register_finish" value="finish" name="" />
</div>

</form>
</div>
</div>
</dd>

<!-- script to save the data on the register donor form -->
<script type="text/javascript">
    // function to save the information to database

    function saveRegisterDonorFormFinish() {
        // get the form values
        // get all the inputs into an array.
        var $inputs = $('#register_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#register_form');
        var formValues = new FormData(form[0]);

        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }
        });

        if (requiredNotFilled) {
            $('#register_form').validate();
            return false;
        }


        // make the ajax call
        else {

            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo isset($data['id']) && (int)$data['id'] ? $data['id'] : 'new'; ?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
                    // get the donor data from the save page
                    var newDonorExists = jQuery.parseJSON(dat).existing;
                    var donorArray = jQuery.parseJSON(dat).donor_data;

                    // update the global variables
                    globalDataArray = donorArray;
                    changeInForm = false;

                    // in case the donor already exists, direct to the old record and alert about the same
                    if (newDonorExists) {
                        alert("A record with the same name and date of birth already exists. \nDirecting you to the existing record.");
                        window.location.replace("<?php bloginfo('url'); ?>/info?donor_id="+donorArray['id']+"&body_status=registered");
                    }

                    // else update the fields on top of the form and alert about the same
                    else {
                       window.location.replace("<?php bloginfo('url'); ?>/database");
                    }

                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
    }



    function saveRegisterDonorForm() {
        // get the form values
        // get all the inputs into an array.
        var $inputs = $('#register_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#register_form');
        var formValues = new FormData(form[0]);

        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }
            if (this.name == 'register_file') {
//            formValues.append("register_file", $('#register_file').files[0]);
            } else {
//            formValues.append (this.name, $(this).val());
            }
        });

        if (requiredNotFilled) {
            $('#register_form').validate();
            return false;
        }


        // make the ajax call
        else {


            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo isset($data['id']) && (int)$data['id'] ? $data['id'] : 'new'; ?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
                    // get the donor data from the save page
                    var newDonorExists = jQuery.parseJSON(dat).existing;
                    var donorArray = jQuery.parseJSON(dat).donor_data;

                    // update the global variables
                    globalDataArray = donorArray;
                    changeInForm = false;

                    // in case the donor already exists, direct to the old record and alert about the same
                    if (newDonorExists) {
                        alert("A record with the same name and date of birth already exists. \nDirecting you to the existing record.");
                        window.location.replace("<?php bloginfo('url'); ?>/info?donor_id="+donorArray['id']+"&body_status=registered");
                    }

                    // else update the fields on top of the form and alert about the same
                    else {
                        $('#new_file').text('');
                        $('#donor_name').text(donorArray['body_last_name'] + ", " + donorArray['body_first_name']);
                        $('#donor_name').attr("href", "<?php bloginfo('url'); ?>/info/?donor_id="+donorArray['id']+"&body_status="+donorArray['body_status']);
                        if (donorArray['gender'] == "male") {
                            $('#donor_gender').addClass("gender_m");
                            $('#donor_gender').removeClass("gender_f");
                            $('#donor_gender').removeClass("gender_u");
                        }
                        else if (donorArray['gender'] == "female") {
                            $('#donor_gender').addClass("gender_f");
                            $('#donor_gender').removeClass("gender_m");
                            $('#donor_gender').removeClass("gender_u");
                        }
                        else if (donorArray['gender'] == "unknown") {
                            $('#donor_gender').addClass("gender_u");
                            $('#donor_gender').removeClass("gender_m");
                            $('#donor_gender').removeClass("gender_f");
                        }
                        else {
                            $('#donor_gender').removeClass("gender_u");
                            $('#donor_gender').removeClass("gender_m");
                            $('#donor_gender').removeClass("gender_f");
                        }
                        var display_status = donorArray['body_status'];
                        if (display_status == "reception") display_status="In Storage";
                        else if (display_status == "usage") display_status="In Use";
                        $('#donor_body_status').text("- " + display_status);
                        alert("Information successfully saved!");
                        window.location.replace("<?php bloginfo('url'); ?>");

                    }
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
    }

    // Set onclick method on the save button
    $('#btn_register_save').click(saveRegisterDonorForm);
    $('#btn_register_finish').click(saveRegisterDonorFormFinish);

</script>


<!-- ACCEPT DONATION TAB -->
                <?php $isAcceptDonationTabOpen = ( (isset($_POST['form_accept_don']) || isset($_POST['form_accept_cont']) || isset($_POST['form_accept_body']) || ($next_status == 'accepted'))) || $tabToOpen == 'donor_information' ?>
<dt id="accept_donation_tab" class="tabs_dt<?php echo $isAcceptDonationTabOpen ? ' selected' : '' ; ?>">Accept donation</dt>
<dd class="tabs_dd<?php echo $isAcceptDonationTabOpen ? ' selected' : '' ; ?>">
    <div class="tab-content">

        <!-- DONOR INFORMATION TAB 2-->
        <?php $isDonorInformationTab2Open = isset($_POST['form_accept_don']) || ($tabToOpen == 'donor_information') ?>
        <dl class="tabs2">
            <div id="blocker2" style="position:absolute; top:8px; left:0; height:45px; width:100%; z-index:3; opacity:0.5; background:white;"></div>
            <dt id="donor_information_tab2" class="tabs2_dt<?php echo $isDonorInformationTab2Open ? ' selected' : ''; ?>">Donor information</dt>
            <dd class="tabs2_dd<?php echo $isDonorInformationTab2Open ? ' selected' : '' ; ?>">
                <div class="tab2_cont">
                    <div class="tc6">

                        <form id="donor_information_form" onsubmit="return false">
                            <!--line-->
                            <div class="tc_line">
                                <p class="tcl_p">First Name:<span class="astool">*</span></p>
                                <input class="tcl_input" type="text" name="body_first_name" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50" required="required"
                                value="<?php echo isset($data['body_first_name']) ? $data['body_first_name'] : ''; ?>" />
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            <!--line-->
                            <div class="tc_line">
                                <p class="tcl_p">Last Name:<span class="astool">*</span></p>
                                <input class="tcl_input" type="text" name="body_last_name" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50" required="required"
                                value="<?php echo isset($data['body_last_name']) ? $data['body_last_name'] : ''; ?>"  />
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            <!--line-->
                            <!--
                            <div class="tc_line">
                                <p class="tcl_p">Middle Initial:</p>
                                <input class="tcl_input" type="text" name="body_middle_name" pattern="[A-Z.\s]{1,50}" maxlength="50"
                                value="<?php echo isset($data['body_middle_name']) ? $data['body_middle_name'] : ''; ?>" />
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            -->
                            <!--line-->
                            <div class="tc_line">
                                <p class="tcl_p">Location:</p>
                                <input class="tcl_input" type="text" name="body_location" pattern="[^!@$%<>]{1,150}" maxlength="150"
                                value="<?php echo isset($data['body_location']) ? $data['body_location'] : ''; ?>"  />
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            <!--line-->
                            <div class="tc_line">
                                <p class="tcl_p">Address:<span class="astool">*</span></p>
                                <input class="tcl_input" type="text" name="body_address[street]" pattern="[^!@$%<>]{1,150}" maxlength="150" required="required"
                                value="<?php echo isset($data['body_address']['street']) ? htmlspecialchars($data['body_address']['street']) : ''; ?>" />
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            <!--line-->
                            <div class="tc_line">
                                <p class="tcl_p">City:<span class="astool">*</span></p>
                                <input class="tcl_input" type="text" name="body_address[city]" pattern="[^!@$%<>]{1,150}" maxlength="150" required="required"
                                value="<?php echo isset($data['body_address']['city']) ? htmlspecialchars($data['body_address']['city']) : ''; ?>"  />
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            <!--line-->
                            <div class="tc_line">
                                <p class="tcl_p">State:</p>
                                <div class="select80">
                                    <select name="body_address[state]">
                                        <option value="">Select One</option>
                                        <?php echo isset($data['body_address']['state']) && $data['body_address']['state'] ?
                                            '<option value="'.$data['body_address']['state'].'" selected="selected">'.strtoupper($data['body_address']['state']).'</option>' : ''; //*/ ?>
                                        <?php
                                        //if( !isset($data['address']['state']) || !$data['address']['state'] ){
                                        //    $def_state = 'CA';
                                        //    echo '<option value="'.$def_state.'" selected="selected">'.$def_state.'</option>';
                                        //}
                                        foreach( $states as $state ){
                                            if( ( isset($data['body_address']['state']) && $state == $data['body_address']['state']) || $state == $def_state ){
                                                continue;
                                            }
                                            echo '<option value="'.$state.'">'.$state.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            <!--line-->
                            <div class="tc_line">
                                <p class="tcl_p">Zip Code:<span class="astool">*</span></p>
                                <input class="tcl_input w60" type="text"  name="body_address[zip_code]" pattern="[0-9]{1,5}" maxlength="5" required="required"
                                value="<?php echo isset($data['body_address']['zip_code']) ? $data['body_address']['zip_code'] : ''; ?>" />
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            <!--line-->
                            <div class="tc_line">
                                <?php
                                    $body_address_phone_value = '';
                                    if($data['body_address']['phone']){
                                        $body_address_phone_value = $data['body_address']['phone'];
                                    }else{
                                        $body_address_phone_value = ($data['body_address']['first_phone'].$data['body_address']['sec_phone'].$data['body_address']['third_phone']);
                                    }
                                ?>
                                <p class="tcl_p w_auto">Phone Number:</p>
                                <input id="qwqw11" class="tcl_input group1 phone-mask" type="text" name="body_address[phone]" value="<?php echo $body_address_phone_value ?>"/>
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                            <div class="tc_line" style="display: none;">
                                <p class="tcl_p w_auto">Phone Number:</p>
                                <input id="qwqw24" class="tcl_input w35 group2" type="number" name="body_address[opt_phone]"
                                    value="<?php echo isset($data['body_address']['opt_phone']) ? $data['body_address']['opt_phone'] : ''; ?>" pattern="[0-9]{1,3}" maxlength="3" />
                                <input id="qwqw21" class="tcl_input w35 group2" type="number" name="body_address[first_phone]"
                                    value="<?php echo isset($data['body_address']['first_phone']) ? $data['body_address']['first_phone'] : ''; ?>" pattern="[0-9]{3}" maxlength="3" />
                                <!--p class="tcl_p dash">-</p-->
                                <input id="qwqw22" class="tcl_input w35 group2" type="number" name="body_address[sec_phone]"
                                    value="<?php echo isset($data['body_address']['sec_phone']) ? $data['body_address']['sec_phone'] : ''; ?>" pattern="[0-9]{3}" maxlength="3"/>
                                <!--p class="tcl_p dash">-</p-->
                                <input id="qwqw23" class="tcl_input w45 group2" type="number" name="body_address[third_phone]"
                                    value="<?php echo isset($data['body_address']['third_phone']) ? $data['body_address']['third_phone'] : ''; ?>" pattern="[0-9]{4}" maxlength="4" />
                                <!--p class="tcl_p dash"> optional:</p-->
                                <input type="hidden" name="body_address[email]" value="" />
                            </div>
                            <div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                            <!--line-->

                            <?php
                            // next_kin_phone
                            if( isset($data['body_address']['donor_info_optional_phone']) && is_array($data['body_address']['donor_info_optional_phone']) &&
                                !empty($data['body_address']['donor_info_optional_phone']) ){
                                $optional_donor_info_phone = count($data['body_address']['donor_info_optional_phone']);
                                foreach( $data['body_address']['donor_info_optional_phone'] as $k => $v ){
                                    if( empty($v['name']) ){
                                        unset($data['body_address']['donor_info_optional_phone'][$k]);
                                        $data['body_address']['donor_info_optional_phone'] = array_values($data['body_address']['donor_info_optional_phone']);
                                        continue;
                                    }
                                ?>
                                    <?php
                                    $body_address_additional_phone_value = '';
                                    if($v['phone']){
                                        $body_address_additional_phone_value = $v['phone'];
                                    }else{
                                        $body_address_additional_phone_value = ($v['first_phone'].$v['sec_phone'].$v['third_phone']);
                                    }
                                    ?>
                                <div class="tc_line inserted">
                                    <input class="tcl_input group_2_<?php echo $k; ?>" type="text" name="body_address[donor_info_optional_phone][<?php echo $k; ?>][name]" pattern="[a-zA-Z0-9\s\-]{0,15}" maxlength="15"
                                           value="<?php echo ( isset($v['name']) ? $v['name'] : '' ); ?>" />
                                    <input class="phone-mask tcl_input group_2_<?php echo $k; ?>" type="text" name="body_address[donor_info_optional_phone][<?php echo $k; ?>][phone]"
                                           value="<?php echo $body_address_additional_phone_value; ?>" />
                                </div><div class="clear"></div>
                                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                                    <!--line-->
                                    <div class="tc_line inserted" style="display: none">
                                        <input class="tcl_input w35 group_2_<?php echo $k; ?>" type="number" name="body_address[donor_info_optional_phone][<?php echo $k; ?>][first_phone]" pattern="[0-9]{3}" maxlength="3"
                                        value="<?php echo isset($data['body_address']['donor_info_optional_phone'][$k]['first_phone']) ? $data['body_address']['donor_info_optional_phone'][$k]['first_phone'] : ''; ?>" />
                                        <p class="tcl_p dash">-</p>
                                        <input class="tcl_input w35 group_2_<?php echo $k; ?>" type="number" name="body_address[donor_info_optional_phone][<?php echo $k; ?>][sec_phone]" pattern="[0-9]{3}" maxlength="3"
                                        value="<?php echo isset($data['body_address']['donor_info_optional_phone'][$k]['sec_phone']) ? $data['body_address']['donor_info_optional_phone'][$k]['sec_phone'] : ''; ?>" />
                                        <p class="tcl_p dash">-</p>
                                        <input class="tcl_input w45 group_2_<?php echo $k; ?>" type="number" name="body_address[donor_info_optional_phone][<?php echo $k; ?>][third_phone]" pattern="[0-9]{4}" maxlength="4"
                                        value="<?php echo isset($data['body_address']['donor_info_optional_phone'][$k]['third_phone']) ? $data['body_address']['donor_info_optional_phone'][$k]['third_phone'] : ''; ?>" />
                                        <p class="tcl_p dash"> optional:</p>
                                        <input class="tcl_input w45 group_2_<?php echo $k; ?>" type="number" name="body_address[donor_info_optional_phone][<?php echo $k; ?>][opt_phone]" pattern="[0-9]{1,6}" maxlength="6"
                                        value="<?php echo isset($data['body_address']['donor_info_optional_phone'][$k]['opt_phone']) ? $data['body_address']['donor_info_optional_phone'][$k]['opt_phone'] : ''; ?>" />
                                    </div><div class="clear"></div>
                                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                                    <!--/line-->
                                    <script>
                                        $(document).ready(function(){
                                            $('.group_2_<?php echo $k; ?>').groupinputs();
                                        });
                                    </script>
                                <?php
                                }
                            }
                            ?>
                        <div id="add_donor_info_phone_body">

                        </div>

                        <div class="tc_line" style="border-bottom:1px solid grey;">
                            <p class="tcl_p5">
                                <input id="add_donor_info_opt_numb" type="button" value="ADD PHONE NUMBER" name="add_donor_info_opt_numb" />
                            </p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                        <div class="tc_line">
                            <p class="tcl_p">Date of Death:</p>
                            <span>
                                <input style="margin-right:20px;" class="tcl_input customDate" type="text" name="date_of_death"  id='date_of_death' onChange="isValidDate(this.value,'Wrong Date of Death');"
                                value="<?php echo isset($data['date_of_death']) ? $data['date_of_death'] : ''; ?>" />
                            </span>
                                <input type="hidden" name="date_of_death_sec"  id='date_of_death_sec' 
                                value="<?php echo isset($data['date_of_death']) ? $data['date_of_death'] : ''; ?>" />


                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <div class="tc_line">
                            <p class="tcl_p">Cause of Death:<span class="astool">*</span></p>
                            <span>
                                <input class="tcl_input" type="text" name="cause_of_death" pattern="[a-zA-Z\s\-']{1,50}"
                                value="<?php echo isset($data['cause_of_death']) ? $data['cause_of_death'] : ''; ?>" maxlength="50"required="required" />
                            </span>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <p class="tcl_p">Time of Death:</p>
                            <!-- <input class="tcl_input w50 mar_right" type="text"  name="time_of_death[hour]" pattern="[0-9/]{1,2}" maxlength="2"
                            value="<?php echo isset($data['time_of_death']['hour']) ? $data['time_of_death']['hour'] : ''; ?>" required="required" />
                            <input class="tcl_input w50 mar_right" type="text"  name="time_of_death[minute]" pattern="[0-9/]{1,2}" maxlength="2"
                            value="<?php echo isset($data['time_of_death']['minute']) ? $data['time_of_death']['minute'] : ''; ?>" required="required" />
                             -->

                            <?php
                                $hoursAll = array();
                                for( $i = 1; $i < 13; $i++ ){
                                    if( $i < 10 ){ $hoursAll[] = '0'.$i; }
                                    else{ $hoursAll[] = $i; }
                                }
                                $minutes5Mins = array();
                                for( $i = 0; $i < 60; $i=$i+5 ){
                                    if( $i < 10 ){ $minutes5Mins[] = '0'.$i;     }
                                    else{ $minutes5Mins[] = $i; }
                                }
                            ?>

                            <div class="select80">
                                <select name="time_of_death[hour]">
                                    <option value="">Select One</option>
                                    <?php echo isset($data['time_of_death']['hour']) && $data['time_of_death']['hour'] ?
                                        '<option value="'.$data['time_of_death']['hour'].'" selected="selected">'.$data['time_of_death']['hour'].'</option>' : ''; ?>
                                    <?php
                                        foreach( $hoursAll as $hour ){
                                            if (isset($data['time_of_death']['hour']) && $data['time_of_death']['hour'] == $hour) { continue; }
                                            echo '<option value="'.$hour.'">'.$hour.'</option>';
                                        }
                                    ?>
                                </select>

                                <select name="time_of_death[minute]">
                                    <option value="">Select One</option>
                                    <?php echo isset($data['time_of_death']['minute']) && $data['time_of_death']['minute'] ?
                                        '<option value="'.$data['time_of_death']['minute'].'" selected="selected">'.$data['time_of_death']['minute'].'</option>' : ''; ?>
                                    <?php
                                    foreach( $minutes5Mins as $minute ){
                                        if (isset($data['time_of_death']['minute']) && $data['time_of_death']['minute'] == $minute) { continue; }
                                            echo '<option value="'.$minute.'">'.$minute.'</option>';
                                    }
                                    ?>
                                </select>

                                <select name="time_of_death[range]" >
                                    <option value="">Select One</option>
                                    <option value="AM" <?php echo (isset($data['time_of_death']['range']) && $data['time_of_death']['range']=="AM") ? "selected='selected'" : ""?> >AM</option>
                                    <option value="PM" <?php echo (isset($data['time_of_death']['range']) && $data['time_of_death']['range']=="PM") ? "selected='selected'" : ""?> >PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="tc_sep">
                            <div class="tc_sep_left"></div>
                            <div class="tc_sep_right"></div>
                        </div>



<div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<div class="clear"></div>
<?php
if ($data['donor_file']) {
?>
<div class="tc_line">
                        <p class="tcl_p">Existing File:</p>
    <input type="hidden" name="donor_file_exist" value="<?php echo $data['donor_file']; ?>" />
<p class="tcl_p3"><a target=_blank href='<?php echo WP_HOME; ?>/upload/<?php echo $data['donor_file']; ?>' ><?php echo $data['donor_file']; ?></a></p>
                    </div>

<?php
}
?>



                    <div class="tc_line">
                        <p class="tcl_p">Upload File:</p>
<input class="tcl_input" type="file" name="donor_file" id="donor_file" />
                    </div>
  <span class="attool">All fields marked with * are required!</span>




                        <div class="tc_submit">
                            <input type="hidden" name="donor_id" value="<?php echo ( $data['id'] ? $data['id'] : '' ); ?>" />
                            <input type="hidden" name="form_accept_don" value="accept" />
                            <a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>>
                                <input type="button" value="cancel" name="" />
                            </a>
                            <input id="btn_donor_info_save" type="submit" value="save" name="" />
                            <input type="submit" id="btn_donor_info_finish" value="finish" name="" />

                        </div>
                    </form>
                </div>
            </div>
        </dd>


<!-- script to save the data on the accept donation -> donor information form -->
<script type="text/javascript">
    // function to save the information to the database

    function saveDonorInformationFormFinish() {
        // get the form values
        // get all the inputs into an array.

        var $inputr = $('#register_form :input');
        var regNotFilled = false;

        $inputr.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    regNotFilled = true;
                }
            }
        });

        if (regNotFilled) {
            $("#register_donor_tab").trigger('click');
            $("#register_form").validate();
            return false;
        }

        else {


        var $inputs = $('#donor_information_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#donor_information_form');
        var formValues = new FormData(form[0]);


        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }
//            formValues[this.name] = $(this).val();
        });

        if (requiredNotFilled) {
            $('#donor_information_form').validate();
            return false;
        }
        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,
                success: function(dat)
                {
             window.location.replace("<?php bloginfo('url'); ?>/database");
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
      }
    }



    function saveDonorInformationForm() {
        // get the form values
        // get all the inputs into an array.
        var $inputs = $('#donor_information_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#donor_information_form');
        var formValues = new FormData(form[0]);


        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }
//            formValues[this.name] = $(this).val();
        });

        if (requiredNotFilled) {
            $('#donor_information_form').validate();
            return false;
        }
        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,
                success: function(dat)
                {
                    // get the donor data from the save page
                    var donorArray = jQuery.parseJSON(dat).donor_data;

                    // update the global variables
                    globalDataArray = donorArray;
                    changeInForm = false;

                    $('#donor_name').attr("href", "<?php bloginfo('url'); ?>/info/?donor_id="+donorArray['id']+"&body_status="+donorArray['body_status']);
                    var display_status = donorArray['body_status'];
                    if (display_status == "reception") display_status="In Storage";
                    else if (display_status == "usage") display_status="In Use";
                    $('#donor_body_status').text("- " + display_status);
                    alert("Information successfully saved!");
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
    }

    // Set onclick method on the save button
    $('#btn_donor_info_save').click(saveDonorInformationForm);
    $('#btn_donor_info_finish').click(saveDonorInformationFormFinish);
</script>


        <!-- CONTACTS INFORMATION TAB 2 -->
        <dt id="contacts_information_tab2" class="tabs2_dt<?php echo ( isset($_POST['form_accept_cont']) ? ' selected' : '' ); ?>">Contacts information</dt>
        <dd class="tabs2_dd<?php echo ( isset($_POST['form_accept_cont']) ? ' selected' : '' ); ?>">
            <div class="tab2_cont">
                <div class="tc6">
                    <form id="contacts_information_form" onsubmit="return false;">
                        <!--separator-->
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <div class="tc_sep_hight">
                            <div class="tc_sep_hight_left"></div>
                            <div class="tc_sep_hight_center">Next of kin information</div>
                            <div class="tc_tc_sep_hight_right"></div>
                        </div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <p class="tcl_p">Last Name:<span class="astool">*</span></p>
                            <input class="tcl_input" type="text" name="accept_optional_info[kin_last_name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50" required="required"
                                value="<?php echo isset($data['accept_optional_info']['kin_last_name']) ? $data['accept_optional_info']['kin_last_name'] : ''; ?>" />
                            <p class="tcl_p" style="margin-left:20px;">First Name:<span class="astool">*</span></p>
                            <input class="tcl_input" type="text" name="accept_optional_info[kin_first_name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50" required="required"
                                value="<?php echo isset($data['accept_optional_info']['kin_first_name']) ? $data['accept_optional_info']['kin_first_name'] : ''; ?>" />
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <p class="tcl_p">Relationship:<span class="astool"></span></p>
                            <input class="tcl_input" type="text" name="accept_optional_info[kin_status]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50" 
                                value="<?php echo isset($data['accept_optional_info']['kin_status']) ? $data['accept_optional_info']['kin_status'] : ''; ?>" />
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <p class="tcl_p">Address:<span class="astool"></span></p>
                            <input class="tcl_input" type="text" name="accept_optional_info[kin_street]" pattern="[^!@$%<>]{1,150}" maxlength="150" 
                                value="<?php echo isset($data['accept_optional_info']['kin_street']) ? $data['accept_optional_info']['kin_street'] : ''; ?>" />
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <p class="tcl_p">City:<span class="astool"></span></p>
                            <input class="tcl_input" type="text" name="accept_optional_info[kin_city]" pattern="[^!@$%<>]{1,50}" maxlength="50" 
                                value="<?php echo isset($data['accept_optional_info']['kin_city']) ? $data['accept_optional_info']['kin_city'] : ''; ?>" />
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <p class="tcl_p">State:</p>
                            <div class="select80">
                                <select name="accept_optional_info[kin_state]" >
                                    <option value="">Select One</option>
                                    <?php echo isset($data['accept_optional_info']['kin_state']) && $data['accept_optional_info']['kin_state'] ?
                                    '<option value="'.$data['accept_optional_info']['kin_state'].'" selected="selected">'.strtoupper($data['accept_optional_info']['kin_state']).'</option>' : ''; ?>
                                    <?php
                                    // if( !isset($data['accept_optional_info']['kin_state']) || !$data['accept_optional_info']['kin_state'] ){
                                    //     $def_state = 'CA';
                                    //     echo '<option value="'.$def_state.'" selected="selected">'.$def_state.'</option>';
                                    // }
                                    foreach( $states as $state ){
                                        if( $state == $data['accept_optional_info']['kin_state'] ) continue;
                                        echo '<option value="'.$state.'">'.$state.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <p class="tcl_p">Zip Code:<span class="astool"></span></p>
                            <input class="tcl_input w60" type="text"  name="accept_optional_info[kin_zip_code]" pattern="[0-9]{1,5}" maxlength="5" 
                                value="<?php echo isset($data['accept_optional_info']['kin_zip_code']) ? $data['accept_optional_info']['kin_zip_code'] : ''; ?>" />
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                        <div class="tc_line">
                            <?php
                            $accept_optional_info_kin_phone_value = '';
                            if($data['accept_optional_info']['kin_phone']){
                                $accept_optional_info_kin_phone_value = $data['accept_optional_info']['kin_phone'];
                            }else{
                                $accept_optional_info_kin_phone_value = ($data['accept_optional_info']['kin_opt_phone'].$data['accept_optional_info']['kin_first_phone'].$data['accept_optional_info']['kin_sec_phone']);
                            }
                            ?>
                            <p class="tcl_p w_auto">Phone Number:</p>
                            <input id="qwqw34" class="tcl_input group3 phone-mask" type="text" name="accept_optional_info[kin_phone]" value="<?php echo $accept_optional_info_kin_phone_value ?>" required="required"/>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                        <!--line-->
                        <div class="tc_line" style="display: none">
                            <p class="tcl_p w_auto">Phone Number:<span class="astool">*</span></p>
                            <input id="qwqw34" class="tcl_input w35 group3" type="number" name="accept_optional_info[kin_opt_phone]" pattern="[0-9]{1,3}" maxlength="3"
                            value="<?php echo isset($data['accept_optional_info']['kin_opt_phone']) ? $data['accept_optional_info']['kin_opt_phone'] : ''; ?>" />
                            <input id="qwqw31" class="tcl_input w35 group3" type="number" name="accept_optional_info[kin_first_phone]" pattern="[0-9]{3}" maxlength="3"
                            value="<?php echo isset($data['accept_optional_info']['kin_first_phone']) ? $data['accept_optional_info']['kin_first_phone'] : ''; ?>" />
                            <!--p class="tcl_p dash">-</p-->
                            <input id="qwqw32" class="tcl_input w35 group3" type="number" name="accept_optional_info[kin_sec_phone]" pattern="[0-9]{3}" maxlength="3"
                            value="<?php echo isset($data['accept_optional_info']['kin_sec_phone']) ? $data['accept_optional_info']['kin_sec_phone'] : ''; ?>" />
                            <!--p class="tcl_p dash">-</p-->
                            <input id="qwqw33" class="tcl_input w45 group3" type="number" name="accept_optional_info[kin_third_phone]" pattern="[0-9]{4}" maxlength="4"
                            value="<?php echo isset($data['accept_optional_info']['kin_third_phone']) ? $data['accept_optional_info']['kin_third_phone'] : ''; ?>" />
                            <!--p class="tcl_p dash"> optional:</p-->
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--separator-->

                        <?php
                        // next_kin_phone
                        if( isset($data['accept_optional_info']['next_kin_optional_phone']) && is_array($data['accept_optional_info']['next_kin_optional_phone']) &&
                            !empty($data['accept_optional_info']['next_kin_optional_phone']) ){
                            $optional_next_kin_phone = count($data['accept_optional_info']['next_kin_optional_phone']);
                            foreach( $data['accept_optional_info']['next_kin_optional_phone'] as $k => $v ){
                                if( empty($v['name']) ){
                                    unset($data['accept_optional_info']['next_kin_optional_phone'][$k]);
                                    $data['accept_optional_info']['next_kin_optional_phone'] = array_values($data['accept_optional_info']['next_kin_optional_phone']);
                                    continue;
                                }
                        ?>


                                <?php
                                $accept_optional_info_additional_kin_phone_value = '';
                                if($v['phone']){
                                    $accept_optional_info_additional_kin_phone_value = $v['phone'];
                                }else{
                                    $accept_optional_info_additional_kin_phone_value = ($v['first_phone'].$v['sec_phone'].$v['third_phone']);
                                }
                                ?>
                                <div class="tc_line inserted">
                                    <input class="tcl_input group_3_<?php echo $k; ?>" type="text" name="accept_optional_info[next_kin_optional_phone][<?php echo $k; ?>][name]" pattern="[a-zA-Z0-9\s\-]{0,15}" maxlength="15"
                                           value="<?php echo ( isset($v['name']) ? $v['name'] : '' ); ?>" />
                                    <input class="phone-mask tcl_input group_2_<?php echo $k; ?>" type="text" name="accept_optional_info[next_kin_optional_phone][<?php echo $k; ?>][phone]"
                                           value="<?php echo $accept_optional_info_additional_kin_phone_value; ?>" />
                                </div><div class="clear"></div>

                                <!--line-->
                                <div class="tc_line inserted" style="display: none">
                                    <input class="tcl_input w35 group_3_<?php echo $k; ?>" type="number" name="accept_optional_info[next_kin_optional_phone][<?php echo $k; ?>][first_phone]" pattern="[0-9]{3}" maxlength="3"
                                        value="<?php echo ( isset($v['first_phone']) ? $v['first_phone'] : '' ); ?>" />
                                    <p class="tcl_p dash">-</p>
                                    <input class="tcl_input w35 group_3_<?php echo $k; ?>" type="number" name="accept_optional_info[next_kin_optional_phone][<?php echo $k; ?>][sec_phone]" pattern="[0-9]{3}" maxlength="3"
                                        value="<?php echo ( isset($v['sec_phone']) ? $v['sec_phone'] : '' ); ?>" />
                                    <p class="tcl_p dash">-</p>
                                    <input class="tcl_input w45 group_3_<?php echo $k; ?>" type="number" name="accept_optional_info[next_kin_optional_phone][<?php echo $k; ?>][third_phone]" pattern="[0-9]{4}" maxlength="4"
                                        value="<?php echo ( isset($v['third_phone']) ? $v['third_phone'] : '' ); ?>" />
                                    <p class="tcl_p dash"> optional:</p>
                                    <input class="tcl_input w45 group_3_<?php echo $k; ?>" type="number" name="accept_optional_info[next_kin_optional_phone][<?php echo $k; ?>][opt_phone]" pattern="[0-9]{1,6}" maxlength="6"
                                        value="<?php echo ( isset($v['opt_phone']) ? $v['opt_phone'] : '' ); ?>" />
                                </div><div class="clear"></div>
                                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                                <!--/line-->
                                <script>
                                    $(document).ready(function(){
                                        $('.group_3_<?php echo $k; ?>').groupinputs();
                                    });
                                </script>
                        <?php
                            }
                        }
                        ?>
                    <div id="add_next_kin_phone_body">

                    </div>

                    <div class="tc_line" style="border-bottom:1px solid grey;">
                        <p class="tcl_p5">
                            <input id="add_next_kin_opt_numb" type="button" value="ADD PHONE NUMBER" name="add_next_kin_opt_numb" />
                        </p>
                    </div><div class="clear"></div>
                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                    <div class="tc_sep_hight">
                        <div class="tc_sep_hight_left"></div>
                        <div class="tc_sep_hight_center">Physician information</div>
                        <div class="tc_tc_sep_hight_right"></div>
                    </div><div class="clear"></div>
                    <!--line-->
                    <div class="tc_line">
                        <p class="tcl_p">Last Name:</p>
                        <input class="tcl_input" type="text" name="accept_optional_info[phys_last_name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50"
                            value="<?php echo isset($data['accept_optional_info']['phys_last_name']) ? $data['accept_optional_info']['phys_last_name'] : ''; ?>" />
                        <p class="tcl_p" style="margin-left:20px;">First Name:</p>
                        <input class="tcl_input" type="text" name="accept_optional_info[phys_first_name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50"
                            value="<?php echo isset($data['accept_optional_info']['phys_first_name']) ? $data['accept_optional_info']['phys_first_name'] : ''; ?>" />
                    </div><div class="clear"></div>
                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                        <div class="tc_line">
                            <?php
                            $accept_optional_info_phys_phone_value = '';
                            if($data['accept_optional_info']['phys_phone']){
                                $accept_optional_info_phys_phone_value = $data['accept_optional_info']['phys_phone'];
                            }else{
                                $accept_optional_info_phys_phone_value = ($data['accept_optional_info']['phys_first_phone'].$data['accept_optional_info']['phys_sec_phone'].$data['accept_optional_info']['phys_third_phone']);
                            }
                            ?>
                            <p class="tcl_p w_auto">Phone Number:</p>
                            <input id="qwqw41" class="tcl_input group4 phone-mask" type="text" name="accept_optional_info[phys_phone]" value="<?php echo $accept_optional_info_phys_phone_value ?>"/>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                    <!--line-->
                    <div class="tc_line" style="display: none;">
                        <p class="tcl_p w_auto">Phone Number:</p>
                        <input id="qwqw44" class="tcl_input w35 group4" type="number" name="accept_optional_info[phys_opt_phone]" pattern="[0-9]{1,3}" maxlength="3"
                            value="<?php echo ( isset($data['accept_optional_info']['phys_opt_phone']) ? $data['accept_optional_info']['phys_opt_phone'] : '' ); ?>" />
                        <input id="qwqw41" class="tcl_input w35 group4" type="number" name="accept_optional_info[phys_first_phone]" pattern="[0-9]{3}" maxlength="3"
                            value="<?php echo ( isset($data['accept_optional_info']['phys_first_phone']) ? $data['accept_optional_info']['phys_first_phone'] : '' ); ?>" />
                        <!--p class="tcl_p dash">-</p-->
                        <input id="qwqw42" class="tcl_input w35 group4" type="number" name="accept_optional_info[phys_sec_phone]" pattern="[0-9]{3}" maxlength="3"
                            value="<?php echo ( isset($data['accept_optional_info']['phys_sec_phone']) ? $data['accept_optional_info']['phys_sec_phone'] : '' ); ?>" />
                        <!--p class="tcl_p dash">-</p-->
                        <input id="qwqw43" class="tcl_input w45 group4" type="number" name="accept_optional_info[phys_third_phone]" pattern="[0-9]{4}" maxlength="4"
                            value="<?php echo ( isset($data['accept_optional_info']['phys_third_phone']) ? $data['accept_optional_info']['phys_third_phone'] : '' ); ?>" />
                        <!--p class="tcl_p dash"> optional:</p-->
                    </div><div class="clear"></div>
                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                    <!--separator-->

                    <?php
                    // phisic_phon
                    if( isset($data['accept_optional_info']['phisic_optional_phone']) && is_array($data['accept_optional_info']['phisic_optional_phone']) &&
                        !empty($data['accept_optional_info']['phisic_optional_phone']) ){
                        $optional_phisic_phone = count($data['accept_optional_info']['phisic_optional_phone']);
                    foreach( $data['accept_optional_info']['phisic_optional_phone'] as $k => $v ){
                        if( empty($v['name']) ){
                            unset($data['accept_optional_info']['phisic_optional_phone'][$k]);
                            $data['accept_optional_info']['phisic_optional_phone'] = array_values($data['accept_optional_info']['phisic_optional_phone']);
                            continue;
                        }
                        ?>
                        <?php
                        $accept_optional_info_additional_phys_phone_value = '';
                        if($v['phone']){
                            $accept_optional_info_additional_phys_phone_value = $v['phone'];
                        }else{
                            $accept_optional_info_additional_phys_phone_value = ($v['first_phone'].$v['sec_phone'].$v['third_phone']);
                        }
                        ?>
                        <div class="tc_line inserted">
                            <input class="tcl_input group_4_<?php echo $k; ?>" type="text" name="accept_optional_info[phisic_optional_phone][<?php echo $k; ?>][name]" pattern="[a-zA-Z0-9\s\-]{0,15}" maxlength="15"
                                   value="<?php echo ( isset($v['name']) ? $v['name'] : '' ); ?>" />
                            <input class="phone-mask tcl_input group_4_<?php echo $k; ?>" type="text" name="accept_optional_info[phisic_optional_phone][<?php echo $k; ?>][phone]"
                                   value="<?php echo $accept_optional_info_additional_phys_phone_value; ?>" />
                        </div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line inserted" style="display: none;">
                            <input class="tcl_input w35 group_4_<?php echo $k; ?>" type="number" name="accept_optional_info[phisic_optional_phone][<?php echo $k; ?>][first_phone]" pattern="[0-9]{3}" maxlength="3"
                            value="<?php echo ( isset($v['first_phone']) ? $v['first_phone'] : '' ); ?>" />
                            <p class="tcl_p dash">-</p>
                            <input class="tcl_input w35 group_4_<?php echo $k; ?>" type="number" name="accept_optional_info[phisic_optional_phone][<?php echo $k; ?>][sec_phone]" pattern="[0-9]{3}" maxlength="3"
                            value="<?php echo ( isset($v['sec_phone']) ? $v['sec_phone'] : '' ); ?>" />
                            <p class="tcl_p dash">-</p>
                            <input class="tcl_input w45 group_4_<?php echo $k; ?>" type="number" name="accept_optional_info[phisic_optional_phone][<?php echo $k; ?>][third_phone]" pattern="[0-9]{4}" maxlength="4"
                            value="<?php echo ( isset($v['third_phone']) ? $v['third_phone'] : '' ); ?>" />
                            <p class="tcl_p dash"> optional:</p>
                            <input class="tcl_input w45 group_4_<?php echo $k; ?>" type="number" name="accept_optional_info[phisic_optional_phone][<?php echo $k; ?>][opt_phone]" pattern="[0-9]{1,6}" maxlength="6"
                            value="<?php echo ( isset($v['opt_phone']) ? $v['opt_phone'] : '' ); ?>" />
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--/line-->
                        <script>
                            $(document).ready(function(){
                                $('.group_4_<?php echo $k; ?>').groupinputs();
                            });
                        </script>
                        <?php
                    }
                }
                ?>
                <div id="add_phisic_phone_body">

                </div>

                <div class="tc_line" style="border-bottom:1px solid grey;">
                    <p class="tcl_p5">
                        <input id="add_phisic_opt_numb" type="button" value="ADD PHONE NUMBER" name="add_phisic_opt_numb" />
                    </p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_sep_hight">
                    <div class="tc_sep_hight_left"></div>
                    <div class="tc_sep_hight_center">Disposal of cremains</div>
                    <div class="tc_tc_sep_hight_right"></div>
                </div><div class="clear"></div>
                <!--line-->
                <div class="tc_line">
                    <p class="tcl_p w_auto">Does family want cremains?</p>
                    <div class="select80">

                        <select name="accept_optional_info[kin_want_cremains]" id="cremains">
                            <option value="">Select One</option>
                            <option value="Yes"<?php echo isset($data['accept_optional_info']['kin_want_cremains']) &&
                                $data['accept_optional_info']['kin_want_cremains'] == 'Yes' ? ' selected="selected"' : ''; ?>>Yes</option>
                            <option value="No"<?php echo isset($data['accept_optional_info']['kin_want_cremains']) &&
                                $data['accept_optional_info']['kin_want_cremains'] == 'No' ? ' selected="selected"' : ''; ?>>No</option>
                        </select>
                    </div>
</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<!--line-->
<div class="tc_line">
    <p class="tcl_p w285">Same as next of kin: </p>
    <div class="check_padding">
        <input id="same_next_kin" type="checkbox" name="accept_optional_info[next_person]"
            value="<?php echo isset($data['accept_optional_info']['next_person']) && $data['accept_optional_info']['next_person'] ? 'checked="checked"' : ''; ?>" />
    </div>
</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>



<!--line-->
<div class="tc_line">
    <p class="tcl_p w285">Last Name of person requesting cremains:</p>
    <input id="empty_inp" class="tcl_input empty_inp" type="text" name="accept_optional_info[kin_last_name_alt]" pattern="[a-zA-Z\,\s\-\']{1,100}" maxlength="100"
        value="<?php echo ( isset($data['accept_optional_info']['kin_last_name_alt']) && $data['accept_optional_info']['kin_last_name_alt'] ?
          $data['accept_optional_info']['kin_last_name_alt'] : (isset($data['accept_optional_info']['kin_last_name']) && $data['accept_optional_info']['kin_last_name'] ?
          $data['accept_optional_info']['kin_last_name'] : '') ); ?>" />
  </div><div class="clear"></div>
  <!--line-->
  <div class="tc_line">
    <p class="tcl_p w285">First Name of person requesting cremains:</p>
    <input id="empty_inp2" class="tcl_input empty_inp" type="text" name="accept_optional_info[kin_first_name_alt]" pattern="[a-zA-Z\,\s\-\']{1,100}" maxlength="100"
        value="<?php echo ( isset($data['accept_optional_info']['kin_first_name_alt']) && $data['accept_optional_info']['kin_first_name_alt'] ?
          $data['accept_optional_info']['kin_first_name_alt'] : (isset($data['accept_optional_info']['kin_first_name']) && $data['accept_optional_info']['kin_first_name'] ?
          $data['accept_optional_info']['kin_first_name'] : '') ); ?>" />
</div><div class="clear"></div>



  <!--line-->
<div class="tc_line">
    <p class="tcl_p">Relationship:</p>
    <input class="tcl_input empty_inp" type="text" name="accept_optional_info[kin_status_alt]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50"
    value="<?php echo ( isset($data['accept_optional_info']['kin_status_alt']) && $data['accept_optional_info']['kin_status_alt'] ?
      $data['accept_optional_info']['kin_status_alt'] : (isset($data['accept_optional_info']['kin_status']) ?
      $data['accept_optional_info']['kin_status'] : '') ); ?>" />
    </div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<!--line-->
<div class="tc_line">
    <p class="tcl_p">Address:</p>
    <input class="tcl_input empty_inp" type="text" name="accept_optional_info[kin_street_alt]" pattern="[^!@$%<>]{1,150}" maxlength="150"
        value="<?php echo ( isset($data['accept_optional_info']['kin_street_alt']) && $data['accept_optional_info']['kin_street_alt'] ?
          $data['accept_optional_info']['kin_street_alt'] : (isset($data['accept_optional_info']['kin_street']) ?
          $data['accept_optional_info']['kin_street'] : '') ); ?>" />
    </div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<!--line-->
<div class="tc_line">
    <p class="tcl_p">City:</p>
    <input class="tcl_input empty_inp" type="text" name="accept_optional_info[kin_city_alt]" pattern="[^!@$%<>]{1,50}" maxlength="50"
        value="<?php echo ( isset($data['accept_optional_info']['kin_city_alt']) && $data['accept_optional_info']['kin_city_alt'] ?
          $data['accept_optional_info']['kin_city_alt'] : (isset($data['accept_optional_info']['kin_city']) ?
          $data['accept_optional_info']['kin_city'] : '') ); ?>" />
    </div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<!--line-->
<div class="tc_line">
    <p class="tcl_p">State:</p>
    <div class="select80">
        <select name="accept_optional_info[kin_state_alt]">
            <option value="">Select One</option>
            <?php echo ( isset($data['accept_optional_info']['kin_state_alt']) && $data['accept_optional_info']['kin_state_alt'] ?
                '<option value="'.$data['accept_optional_info']['kin_state_alt'].'" selected="selected">'.
                strtoupper($data['accept_optional_info']['kin_state_alt']).'</option>' :
                (isset($data['accept_optional_info']['kin_state']) && $data['accept_optional_info']['kin_state'] ?
                  '<option value="'.$data['accept_optional_info']['kin_state'].'" selected="selected">'.
                  strtoupper($data['accept_optional_info']['kin_state']).'</option>' : '') ); ?>
            <?php
            if( !isset($data['accept_optional_info']['kin_state_alt']) || !$data['accept_optional_info']['kin_state_alt'] ){
            //    $def_state = 'CA';
            //    echo '<option value="'.$def_state.'" selected="selected">'.$def_state.'</option>';
            }
            foreach( $states as $state ){
                if( $state == $data['accept_optional_info']['kin_state_alt'] ) continue;
                echo '<option value="'.$state.'">'.$state.'</option>';
            }
            ?>
        </select>
    </div>
</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<!--line-->
<div class="tc_line">
    <p class="tcl_p">Zip Code:</p>
    <input class="tcl_input w60 empty_inp" type="text"  name="accept_optional_info[kin_zip_code_alt]" pattern="[0-9]{1,5}" maxlength="5"
        value="<?php echo ( isset($data['accept_optional_info']['kin_zip_code_alt']) && $data['accept_optional_info']['kin_zip_code_alt'] ?
          $data['accept_optional_info']['kin_zip_code_alt'] : (isset($data['accept_optional_info']['kin_zip_code']) ?
          $data['accept_optional_info']['kin_zip_code'] : '') ); ?>" />
  </div><div class="clear"></div>
  <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                        <div class="tc_line">
                            <?php
                            $accept_optional_info_kin_phone_alt_value = '';
                            if($data['accept_optional_info']['kin_phone_alt']){
                                $accept_optional_info_kin_phone_alt_value = $data['accept_optional_info']['kin_phone_alt'];
                            }elseif (isset($data['accept_optional_info']['kin_first_phone_alt'])&&$data['accept_optional_info']['kin_first_phone_alt']){
                                $accept_optional_info_kin_phone_alt_value = ($data['accept_optional_info']['kin_first_phone_alt'].$data['accept_optional_info']['kin_sec_phone_alt'].$data['accept_optional_info']['kin_third_phone_alt']);
                            }else{
                                $accept_optional_info_kin_phone_alt_value = ($data['accept_optional_info']['kin_phone']);
                            }
                            ?>
                            <p class="tcl_p w_auto">Phone Number:</p>
                            <input id="qwqw51" class="tcl_input group5 phone-mask" type="text" name="accept_optional_info[kin_phone_alt]" value="<?php echo $accept_optional_info_kin_phone_alt_value ?>"/>
                        </div>
  <!--line-->
  <div class="tc_line" style="display: none">
    <p class="tcl_p w_auto">Phone Number:</p>
    <input id="qwqw54" class="tcl_input w35 empty_inp group5" type="number" name="accept_optional_info[kin_opt_phone_alt]" pattern="[0-9]{1,3}" maxlength="3"
    value="<?php echo ( isset($data['accept_optional_info']['kin_opt_phone_alt']) && $data['accept_optional_info']['kin_opt_phone_alt'] ?
      $data['accept_optional_info']['kin_opt_phone_alt'] :
      (isset($data['accept_optional_info']['kin_opt_phone']) ? $data['accept_optional_info']['kin_opt_phone'] : '') ); ?>" />
      <input id="qwqw51" class="tcl_input w35 empty_inp group5" type="number" name="accept_optional_info[kin_first_phone_alt]" pattern="[0-9]{3}" maxlength="3"
      value="<?php echo ( isset($data['accept_optional_info']['kin_first_phone_alt']) && $data['accept_optional_info']['kin_first_phone_alt'] ?
          $data['accept_optional_info']['kin_first_phone_alt'] :
          (isset($data['accept_optional_info']['kin_first_phone']) ? $data['accept_optional_info']['kin_first_phone'] : '') ); ?>" />
          <!--p class="tcl_p dash">-</p-->
          <input id="qwqw52" class="tcl_input w35 empty_inp group5" type="number"  name="accept_optional_info[kin_sec_phone_alt]" pattern="[0-9]{3}" maxlength="3"
          value="<?php echo ( isset($data['accept_optional_info']['kin_sec_phone_alt']) && $data['accept_optional_info']['kin_sec_phone_alt'] ?
              $data['accept_optional_info']['kin_sec_phone_alt'] :
              (isset($data['accept_optional_info']['kin_sec_phone']) ? $data['accept_optional_info']['kin_sec_phone'] : '') ); ?>" />
              <!--p class="tcl_p dash">-</p-->
              <input id="qwqw53" class="tcl_input w45 empty_inp group5" type="number" name="accept_optional_info[kin_third_phone_alt]" pattern="[0-9]{4}" maxlength="4"
              value="<?php echo ( isset($data['accept_optional_info']['kin_third_phone_alt']) && $data['accept_optional_info']['kin_third_phone_alt'] ?
                  $data['accept_optional_info']['kin_third_phone_alt'] :
                  (isset($data['accept_optional_info']['kin_third_phone']) ? $data['accept_optional_info']['kin_third_phone'] : '') ); ?>" />
                  <!--p class="tcl_p dash"> optional:</p-->
              </div><div class="clear"></div>
              <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
              <!--/line-->
              <script>

                $(function(){

                    jQuery('#same_next_kin').change(function(){
                        if( jQuery('#same_next_kin').attr('checked') == 'checked' ){
                            jQuery('.empty_inp').attr({value:''});
                        }
                    })

                });

            </script>

            <?php
            // kin_optional
            if( isset($data['accept_optional_info']['kin_optional_phone']) && is_array($data['accept_optional_info']['kin_optional_phone']) && !empty($data['accept_optional_info']['kin_optional_phone']) ){
                $optional_phone = count($data['accept_optional_info']['kin_optional_phone']);
                foreach( $data['accept_optional_info']['kin_optional_phone'] as $k => $v ){
                    if( empty($v['name']) ){
                        unset($data['accept_optional_info']['kin_optional_phone'][$k]);
                        $data['accept_optional_info']['kin_optional_phone'] = array_values($data['accept_optional_info']['kin_optional_phone']);
                        continue;
                    }
                    ?>

                    <?php
                    $accept_optional_info_kin_optional_phone_value = '';
                    if($v['phone']){
                        $accept_optional_info_kin_optional_phone_value = $v['phone'];
                    }else{
                        $accept_optional_info_kin_optional_phone_value = ($v['first_phone'].$v['sec_phone'].$v['third_phone']);
                    }
                    ?>

                    <div class="tc_line inserted">
                        <input class="tcl_input group_5_<?php echo $k; ?>" type="text" name="accept_optional_info[kin_optional_phone][<?php echo $k; ?>][name]" pattern="[a-zA-Z0-9\s\-]{0,15}" maxlength="15"
                               value="<?php echo ( isset($v['name']) ? $v['name'] : '' ); ?>" />
                        <input class="tcl_input phone-mask group_5_<?php echo $k; ?>" type="text" name="accept_optional_info[kin_optional_phone][<?php echo $k; ?>][phone]"
                               value="<?php echo $accept_optional_info_kin_optional_phone_value; ?>" />
                    </div>

                    <!--line-->
                    <div class="tc_line inserted" style="display: none">
                        <input class="tcl_input w35 group_5_<?php echo $k; ?>" type="number" name="accept_optional_info[kin_optional_phone][<?php echo $k; ?>][first_phone]" pattern="[0-9]{3}" maxlength="3"
                        value="<?php echo ( isset($v['first_phone']) ? $v['first_phone'] : '' ); ?>" />
                        <p class="tcl_p dash">-</p>
                        <input class="tcl_input w35 group_5_<?php echo $k; ?>" type="number" name="accept_optional_info[kin_optional_phone][<?php echo $k; ?>][sec_phone]" pattern="[0-9]{3}" maxlength="3"
                        value="<?php echo ( isset($v['sec_phone']) ? $v['sec_phone'] : '' ); ?>" />
                        <p class="tcl_p dash">-</p>
                        <input class="tcl_input w45 group_5_<?php echo $k; ?>" type="number" name="accept_optional_info[kin_optional_phone][<?php echo $k; ?>][third_phone]" pattern="[0-9]{4}" maxlength="4"
                        value="<?php echo ( isset($v['third_phone']) ? $v['third_phone'] : '' ); ?>" />
                        <p class="tcl_p dash"> optional:</p>
                        <input class="tcl_input w45 group_5_<?php echo $k; ?>" type="number" name="accept_optional_info[kin_optional_phone][<?php echo $k; ?>][opt_phone]" pattern="[0-9]{1,6}" maxlength="6"
                        value="<?php echo ( isset($v['opt_phone']) ? $v['opt_phone'] : '' ); ?>" />
                    </div><div class="clear"></div>
                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                    <!--/line-->
                    <script>
                        $(document).ready(function(){
                            $('.group_5_<?php echo $k; ?>').groupinputs();
                        });
                    </script>
                    <?php
                }
            }
            ?>
            <div id="add_phone_body">

            </div>

            <div class="tc_line" style="border-bottom:1px solid grey;">
                <p class="tcl_p5">
                    <input id="add_opt_numb" type="button" value="ADD PHONE NUMBER" name="add_opt_numb" />
                </p>
            </div><div class="clear"></div>
            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

<script>
    $(function(){

        // reg_new_optional_phone
        // Dynamically adding optional phone
        var next_reg_new_opt_phone = <?php echo (isset($optional_reg_new_phone) ? $optional_reg_new_phone : 0); ?>;
        $('#add_reg_new_opt_numb').on('click', function(){
            $('#add_reg_new_phone_body').append(
                '<div class="tc_line">'+
                '<p class="tcl_p w_auto">Optional Phone Name:</p>'+
                '<input class="tcl_input group_1_add_'+next_reg_new_opt_phone+'" type="text" name="address[reg_new_optional_phone]['+next_reg_new_opt_phone+'][name]" pattern="[a-zA-Z0-9\'\s\-_]{0,70}" maxlength="70" value="" />'+
                '<input class="phone-mask tcl_input group_1_add_'+next_reg_new_opt_phone+'" type="text" name="address[reg_new_optional_phone]['+next_reg_new_opt_phone+'][phone]" value="" />'+
                '</div><div class="clear"></div>'+
                '<br><div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>'
            );
            $(".phone-mask").mask("(999) 999-9999");
            $('.group_1_add_'+next_reg_new_opt_phone).groupinputs();
            next_reg_new_opt_phone = next_reg_new_opt_phone + 1;//*/
        });

        // donor_info
        // Dynamically adding optional phone
        var next_donor_info_opt_phone = <?php echo (isset($optional_donor_info_phone) ? $optional_donor_info_phone : 0); ?>;
        $('#add_donor_info_opt_numb').on('click', function(){
            $('#add_donor_info_phone_body').append(
                '<div class="tc_line">'+
                '<p class="tcl_p w_auto">Optional Phone Name:</p>'+
                '<input class="tcl_input group_2_add_'+next_donor_info_opt_phone+'" type="text" name="body_address[donor_info_optional_phone]['+next_donor_info_opt_phone+'][name]" pattern="[a-zA-Z0-9\'\s\-_]{0,70}" maxlength="70" value="" />'+
                '<input class="phone-mask tcl_input group_2_add_'+next_donor_info_opt_phone+'" type="text" name="body_address[donor_info_optional_phone]['+next_donor_info_opt_phone+'][phone]" value="" />'+
                '</div><div class="clear"></div>'+
                '<br><div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>'
            );
            $(".phone-mask").mask("(999) 999-9999");
            $('.group_2_add_'+next_donor_info_opt_phone).groupinputs();
            next_donor_info_opt_phone = next_donor_info_opt_phone + 1;//*/
        });

        // next_kin_phone
        // Dynamically adding optional phone
        var next_kin_numb_opt_phone = <?php echo (isset($optional_next_kin_phone) ? $optional_next_kin_phone : 0); ?>;
        $('#add_next_kin_opt_numb').on('click', function(){
            $('#add_next_kin_phone_body').append(
                '<div class="tc_line">'+
                '<p class="tcl_p w_auto">Optional Phone Name:</p>'+
                '<input class="tcl_input group_3_add_'+next_kin_numb_opt_phone+'" type="text" name="accept_optional_info[next_kin_optional_phone]['+next_kin_numb_opt_phone+'][name]" pattern="[a-zA-Z0-9\'\s\-_]{0,70}" maxlength="70" value="" />'+
                '<input class="phone-mask tcl_input group_3_add_'+next_kin_numb_opt_phone+'" type="text" name="accept_optional_info[next_kin_optional_phone]['+next_kin_numb_opt_phone+'][phone]" value="" />'+
                '</div><div class="clear"></div>'+
                '<br><div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>'
            );
            $(".phone-mask").mask("(999) 999-9999");
            $('.group_3_add_'+next_kin_numb_opt_phone).groupinputs();
            next_kin_numb_opt_phone = next_kin_numb_opt_phone + 1;//*/
        });

        // phisic_phon
        // Dynamically adding optional phone
        var phisic_numb_opt_phone = <?php echo (isset($optional_phisic_phone) ? $optional_phisic_phone : 0); ?>;
        $('#add_phisic_opt_numb').on('click', function(){
            $('#add_phisic_phone_body').append(
                '<div class="tc_line">'+
                '<p class="tcl_p w_auto">Optional Phone Name:</p>'+
                '<input class="tcl_input group_4_add_'+phisic_numb_opt_phone+'" type="text" name="accept_optional_info[phisic_optional_phone]['+phisic_numb_opt_phone+'][name]" pattern="[a-zA-Z0-9\'\s\-_]{0,70}" maxlength="70" value="" />'+
                '<input class="phone-mask tcl_input group_4_add_'+phisic_numb_opt_phone+'" type="text" name="accept_optional_info[phisic_optional_phone]['+phisic_numb_opt_phone+'][phone]" value="" />'+
                '</div><div class="clear"></div>'+
                '<br><div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>'
            );
            $(".phone-mask").mask("(999) 999-9999");
            $('.group_4_add_'+phisic_numb_opt_phone).groupinputs();
            phisic_numb_opt_phone = phisic_numb_opt_phone + 1;//*/
        });

        // kin_optional
        // Dynamically adding optional phone
        var kin_numb_opt_phone = <?php echo (isset($optional_phone) ? $optional_phone : 0); ?>;
        $('#add_opt_numb').on('click', function(){
            $('#add_phone_body').append(
                '<div class="tc_line">'+
                '<p class="tcl_p w_auto">Optional Phone Name:</p>'+
                '<input class="tcl_input group_5_add_'+kin_numb_opt_phone+'" type="text" name="accept_optional_info[kin_optional_phone]['+kin_numb_opt_phone+'][name]" pattern="[a-zA-Z0-9\'\s\-_]{0,70}" maxlength="70" value="" />'+
                '<input class="tcl_input phone-mask group_5_add_'+kin_numb_opt_phone+'" type="text" name="accept_optional_info[kin_optional_phone]['+kin_numb_opt_phone+'][phone]" value="" />'+
                '</div><div class="clear"></div>'+
                '<br><div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>'
            );
            $(".phone-mask").mask("(999) 999-9999");
            $('.group_5_add_'+kin_numb_opt_phone).groupinputs();
            kin_numb_opt_phone = kin_numb_opt_phone + 1;//*/
        });

        // Body Condition
        $('#add_pos').live('click', function(){
            $('#add_dody').append(
                '<div class="tc_line">'+
                '<div class="check_left_padding">'+
                '<input type="checkbox" name="accept_don_check[added][]" value="" style="float:left;" />'+
                '<input class="tcl_input" type="text" name="accept_don_check[added_text][]" value="" style="margin-left:60px;" placeholder="enter the text..." />'+
                '</div>'+
                '</div>'+
                '<div class="clear"></div><div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>'
            );
        })

    });

</script>


<div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<div class="clear"></div>
<?php
if ($data['contact_file']) {
?>
<div class="tc_line">
                        <p class="tcl_p">Existing File:</p>
    <input type="hidden" name="contact_file_exist" value="<?php echo $data['contact_file']; ?>" />
<p class="tcl_p3"><a target=_blank href='<?php echo WP_HOME; ?>/upload/<?php echo $data['contact_file']; ?>' ><?php echo $data['contact_file']; ?></a></p>
                    </div>

<?php
}
?>



                    <div class="tc_line">
                        <p class="tcl_p">Upload File:</p>
<input class="tcl_input" type="file" name="contact_file" id="contact_file" />
                    </div>

  <span class="attool">All fields marked with * are required!</span>




<div class="tc_submit">
    <input type="hidden" name="donor_id" value="<?php echo ( $data['id'] ? $data['id'] : '' ); ?>" />
    <input type="hidden" name="form_accept_cont" value="accept" />
    <a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>>
        <input type="button" value="cancel" name="" />
    </a>
    <input id="btn_contacts_information_save" type="submit" value="save" name="" />
    <input id="btn_contacts_information_finish" type="submit" value="finish" name="" />
</div>

</form>
</div>
</div>
</dd>

<!-- script to save the data on the accept donation -> contacts information form -->
<script type="text/javascript">
    // function to save the information to database

    function saveContactsInformationFormFinish() {
        // get the form values
        // get all the inputs into an array.


        var $inputr = $('#register_form :input');
        var regNotFilled = false;

        $inputr.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    regNotFilled = true;
                }
            }
        });

        if (regNotFilled) {
            $("#register_donor_tab").trigger('click');
            $("#register_form").validate();
            return false;
        }

        else {

        var $inputs = $('#contacts_information_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#contacts_information_form');
        var formValues = new FormData(form[0]);


        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }
//            formValues[this.name] = $(this).val();
        });

        if (requiredNotFilled) {
            $('#contacts_information_form').validate();
            return false;
        }
        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
             window.location.replace("<?php bloginfo('url'); ?>/database");
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
      }
    }






    function saveContactsInformationForm() {
        // get the form values
        // get all the inputs into an array.
        var $inputs = $('#contacts_information_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#contacts_information_form');
        var formValues = new FormData(form[0]);


        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }
//            formValues[this.name] = $(this).val();
        });

        if (requiredNotFilled) {
            $('#contacts_information_form').validate();
            return false;
        }
        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
                    // get the donor data from the save page
                    var donorArray = jQuery.parseJSON(dat).donor_data;

                    // update the global variables
                    globalDataArray = donorArray;
                    changeInForm = false;

                    $('#donor_name').attr("href", "<?php bloginfo('url'); ?>/info/?donor_id="+donorArray['id']+"&body_status="+donorArray['body_status']);
                    var display_status = donorArray['body_status'];
                    if (display_status == "reception") display_status="In Storage";
                    else if (display_status == "usage") display_status="In Use";
                    $('#donor_body_status').text("- " + display_status);
                    alert("Information successfully saved!");
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
    }

    // Set onclick method on the save button
    $('#btn_contacts_information_save').click(saveContactsInformationForm);
    $('#btn_contacts_information_finish').click(saveContactsInformationFormFinish);
</script>


<!-- BODY CONDITION TAB 2 -->
                <?php $isBodyConditionTab2Open = ((!isset($_POST['form_accept_cont']) && !isset($_POST['form_accept_don'])) || isset($_POST['form_accept_body']) || ($next_status == 'accepted')) && ($tabToOpen != 'donor_information') ?>
<dt id="body_condition_tab2" class="tabs2_dt<?php echo ( $isBodyConditionTab2Open ? ' selected' : '' ); ?>">Body Condition</dt>
<dd class="tabs2_dd<?php echo ( $isBodyConditionTab2Open ? ' selected' : '' ); ?>">
    <div class="tab2_cont">
        <div class="tc6">
            <form id="body_condition_form" onsubmit="return false;">
                <!--line-->
                <div class="tc_line">
                    <p class="tcl_p w160">Body Accepted:</p>

                    <div class="select80">
                        <?php $isBodyAccepted = isset($data['accept_don_check']['body_status']) && $data['accept_don_check']['body_status'] == 'accepted'; ?>
                      <select id="status_select" name="body_status_choice">
                        <option value="">Select One</option>
                        <option value="rejected"<?php echo !$isBodyAccepted ? ' selected' : ''; ?>>No</option>
                        <option value="accepted"<?php echo ($isBodyAccepted || !isset($data['accept_don_check']['body_status'])) ? ' selected' : ''; ?>>Yes</option>
                    </select>
                    <input id="accept_reject_status" type="hidden" name="accept_don_check[body_status]" value="">
                    </div>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <div class="tc_line">
                    <p class="tcl_p w_auto">Number of Limbs:</p>
                    <div class="select80">
                        <select name="accept_don_check[limbs]">
                            <option value="">Select One</option>
                            <option value="4"<?php echo isset($data['accept_don_check']['limbs']) && $data['accept_don_check']['limbs'] == '4' ?
                            ' selected="selected"' : ''; ?>>4</option>
                            <option value="3"<?php echo isset($data['accept_don_check']['limbs']) && $data['accept_don_check']['limbs'] == '3' ?
                            ' selected="selected"' : ''; ?>>3</option>
                            <option value="2"<?php echo isset($data['accept_don_check']['limbs']) && $data['accept_don_check']['limbs'] == '2' ?
                            ' selected="selected"' : ''; ?>>2</option>
                            <option value="1"<?php echo isset($data['accept_don_check']['limbs']) && $data['accept_don_check']['limbs'] == '1' ?
                            ' selected="selected"' : ''; ?>>1</option>
                        </select>
                    </div>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                <!--line-->
                <div class="tc_line2" style="border-bottom:1px solid grey;">
                    <p id="accept_note" class="tcl_p w160">Additional Comments:</p>
                    <textarea class="textarea1" name="accept_don_check[comments]" pattern="[a-zA-Z0-9\s\'\w\-]{0,500}" maxlength="500"><?php echo isset($data['accept_don_check']['comments']) ? $data['accept_don_check']['comments'] : ''; ?></textarea>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <?php $displayCheckboxes = !$isBodyAccepted; ?>
                <div id="wrap_checkbox" style="display:<?php echo $displayCheckboxes?'block':'none' ?>">
                    <div id="add_dody">
                        <!--line-->
                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="accept_don_check[ascites]" <?php echo isset($data['accept_don_check']['ascites']) ? 'checked="checked"' : ''; ?> value="Ascites" />
                            </div>
                            <p class="tcl_p5">Ascites</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="accept_don_check[aids]" <?php echo isset($data['accept_don_check']['aids']) ? 'checked="checked"' : ''; ?> value="AIDS" />
                            </div>
                            <p class="tcl_p5">AIDS</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="accept_don_check[bed_sores]" <?php echo isset($data['accept_don_check']['bed_sores']) ? 'checked="checked"' : ''; ?> value="Bed Sores" />
                            </div>
                            <p class="tcl_p5">Bed Sores</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="accept_don_check[contractures]" <?php echo isset($data['accept_don_check']['contractures']) ? 'checked="checked"' : ''; ?> value="Contractures" />
                            </div>
                            <p class="tcl_p5">Contractures</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="accept_don_check[creutzfelt-jacobs_disease]" <?php echo isset($data['accept_don_check']['creutzfelt-jacobs_disease']) ? 'checked="checked"' : ''; ?> value="Creutzfelt-Jacobs Disease" />
                            </div>
                            <p class="tcl_p5">Creutzfelt-Jacobs Disease</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="accept_don_check[edema]" <?php echo isset($data['accept_don_check']['edema']) ? 'checked="checked"' : ''; ?> value="Edema" />
                            </div>
                            <p class="tcl_p5">Edema</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="accept_don_check[hepatitis]" <?php echo isset($data['accept_don_check']['hepatitis']) ? 'checked="checked"' : ''; ?> value="Hepatitis" />
                            </div>
                            <p class="tcl_p5">Hepatitis</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="accept_don_check[infectious_disease]" <?php echo isset($data['accept_don_check']['infectious_disease']) ? 'checked="checked"' : ''; ?> value="Infectious disease" />
                            </div>
                            <p class="tcl_p5">Infectious disease</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="accept_don_check[jaundice]" <?php echo isset($data['accept_don_check']['jaundice']) ? 'checked="checked"' : ''; ?> value="Jaundice" />
                            </div>
                            <p class="tcl_p5">Jaundice</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="accept_don_check[obesity]" <?php echo isset($data['accept_don_check']['obesity']) ? 'checked="checked"' : ''; ?> value="Obesity" />
                            </div>
                            <p class="tcl_p5">Obesity</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <!--line-->
                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="accept_don_check[tuberculosis]" <?php echo isset($data['accept_don_check']['tuberculosis']) ? 'checked="checked"' : ''; ?> value="Tuberculosis" />
                            </div>
                            <p class="tcl_p5">Tuberculosis</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                        <?php
                        //echo '<pre>'; print_r($data);
                        if( isset($data['accept_don_check']['added_pos']) && is_array($data['accept_don_check']['added_pos']) && !empty($data['accept_don_check']['added_pos']) ){
                            foreach( $data['accept_don_check']['added_pos'] as $k => $v ){
                                ?>
                                <!--line-->
                                <div class="tc_line">
                                    <div class="check_left_padding">
                                        <input type="checkbox" name="accept_don_check[added_pos][]" checked="checked" value="<?php echo $data['accept_don_check']['added_pos'][$k]; ?>" />
                                    </div>
                                    <p class="tcl_p5"><?php echo $data['accept_don_check']['added_pos'][$k]; ?></p>
                                </div><div class="clear"></div>
                                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                                <?php
                            }
                        }

                        if( isset($data['accept_don_check']['added']) && is_array($data['accept_don_check']['added']) && !empty($data['accept_don_check']['added']) &&
                            isset($data['accept_don_check']['added_text']) && is_array($data['accept_don_check']['added_text']) && !empty($data['accept_don_check']['added_text']) ){
                            foreach( $data['accept_don_check']['added'] as $k => $v ){
                                //foreach( $data['accept_don_check']['added_text'] as $k => $v ){
                                ?>
                                <!--line-->
                                <div class="tc_line">
                                    <div class="check_left_padding">
                                        <input type="checkbox" name="accept_don_check[added_pos][]" checked="checked" value="<?php echo $data['accept_don_check']['added_text'][$k]; ?>" />
                                    </div>
                                    <p class="tcl_p5"><?php echo $data['accept_don_check']['added_text'][$k]; ?></p>
                                </div><div class="clear"></div>
                                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
                                <?php
                //}
                            }
                        }
                        ?>
                    </div>
                    <div class="tc_line" style="border-bottom:1px solid grey;">
                        <div class="check_left_padding">
                        </div>
                        <p class="tcl_p5">
                            <input id="add_pos" type="button" value="ADD CONDITION" name="add_pos" />
                        </p>
                    </div><div class="clear"></div>
                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                <script>
                    $(document).ready(function(){
                        var selected_stat = '';
                        $('#pre_body_stat_accept').on('change', function(){
                            $('#accept_body_status').val("accepted");
                        });

                        $('#pre_body_stat_regect').on('change', function(){
                            $('#accept_body_status').val("rejected");
                        });

                        // Change the value of the accept_reject_status input field according to the option selected
                        if ($('#status_select').val() == "accepted") {
                            $('#accept_reject_status').val("accepted");
                        }
                        else if ($('#status_select').val() == "rejected") {
                            $('#accept_reject_status').val("rejected");
                        }

                        $('#status_select').on('change', function () {
                            if ($('#status_select').val() == "accepted") {
                                $('#accept_reject_status').val("accepted");
                            }
                            else if ($('#status_select').val() == "rejected") {
                                $('#accept_reject_status').val("rejected");
                            }
                        });
                    });

                </script>



<div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<div class="clear"></div>
<?php
if ($data['condition_file']) {
?>
<div class="tc_line">
                        <p class="tcl_p">Existing File:</p>
    <input type="hidden" name="condition_file_exist" value="<?php echo $data['condition_file']; ?>" />
<p class="tcl_p3"><a target=_blank href='<?php echo WP_HOME; ?>/upload/<?php echo $data['condition_file']; ?>' ><?php echo $data['condition_file']; ?></a></p>
                    </div>

<?php
}
?>



                    <div class="tc_line">
                        <p class="tcl_p">Upload File:</p>
<input class="tcl_input" type="file" name="condition_file" id="condition_file" />
                    </div>




            </div>
                <div class="tc_submit">
                    <input type="hidden" name="donor_id" value="<?php echo ( $data['id'] ? $data['id'] : '' ); ?>" />
                    <input type="hidden" name="form_accept_body" value="accept" />
                    <a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>>
                        <input type="button" value="cancel" name="" />
                    </a>
                    <input id="btn_body_condition_save" type="submit" value="save" name="" />
                    <input id="btn_body_condition_finish" type="submit" value="finish" name="" />
                </div>
        </form>
    </div>
</div>
</dd>
</dl>
</div>
</dd>

<!-- script to save the data on the accept donation -> body condition form -->
<script type="text/javascript">
    // function to save the information to the database
    function saveBodyConditionFormFinish() {
        // get the form values
        // get all the inputs into an array.

        var $inputr = $('#register_form :input');
        var regNotFilled = false;
        $inputr.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    regNotFilled = true;
                }
            }
        });

        if (regNotFilled) {
            $("#register_donor_tab").trigger('click');
            $("#register_form").validate();
            return false;
        }

        else {


        var $inputs = $('#body_condition_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#body_condition_form');
        var formValues = new FormData(form[0]);
        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }

            if ($(this).attr('type') == "checkbox") {
                if ($(this).is(':checked')) {
//                   formValues[this.name] = $(this).val();
                }
            }
            else {
//                formValues[this.name] = $(this).val();
            }
        });

        if (requiredNotFilled) {
            $('#body_condition_form').validate();
            return false;
        }
        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
             window.location.replace("<?php bloginfo('url'); ?>/database");
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
      }
    }




    function saveBodyConditionForm() {
        // get the form values
        // get all the inputs into an array.
        var $inputs = $('#body_condition_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#body_condition_form');
        var formValues = new FormData(form[0]);
        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }

            if ($(this).attr('type') == "checkbox") {
                if ($(this).is(':checked')) {
//                   formValues[this.name] = $(this).val();
                }
            }
            else {
//                formValues[this.name] = $(this).val();
            }
        });

        if (requiredNotFilled) {
            $('#body_condition_form').validate();
            return false;
        }
        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
                    // get the donor data from the save page
                    var donorArray = jQuery.parseJSON(dat).donor_data;

                    // update the global variables
                    globalDataArray = donorArray;
                    changeInForm = false;

                    $('#donor_name').attr("href", "<?php bloginfo('url'); ?>/info/?donor_id="+donorArray['id']+"&body_status="+donorArray['body_status']);
                    var display_status = donorArray['body_status'];
                    if (display_status == "reception") display_status="In Storage";
                    else if (display_status == "usage") display_status="In Use";
                    $('#donor_body_status').text("- " + display_status);
                    alert("Information successfully saved!");
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
    }

    // Set onclick method on the save button
    $('#btn_body_condition_save').click(saveBodyConditionForm);
    $('#btn_body_condition_finish').click(saveBodyConditionFormFinish);
</script>


<!-- BODY PREP TAB-->
<dt id="body_prep_tab" class="tabs_dt<?php echo ( (isset($_POST['form_recept']) || ($next_status == 'reception')) ? ' selected' : '' ); ?>">Body Prep</dt>
<dd class="tabs_dd<?php echo ( (isset($_POST['form_recept']) || ($next_status == 'reception')) ? ' selected' : '' ); ?>">

<div class="tab-content">
<div class="tc7">
    <form id="body_prep_form" onsubmit="return false;">

      <!--line--><div class="tc_line">
      <p class="tcl_p w160">Date of Arrival:</p>
      <div class="tc_date">
        <span>
            <input style="margin-right:20px;" class="tcl_input customDate" type="text" name="recept[date]"  id='date_of_reception' onChange="isValidDate(this.value,'Wrong Date of Reseption');"
            value="<?php echo isset($data['reception']['date']) ? $data['reception']['date'] : ''; ?>" />
        </span>
            <input type="hidden"  id='date_of_reception_sec' 
            value="<?php echo isset($data['reception']['date']) ? $data['reception']['date'] : ''; ?>" />
    </div>
</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

<!--line--><div class="tc_line">
<p class="tcl_p w160">ID Number:</p>
<div class="select140">
  <select  name="recept[unic_id]" >
    <option value="">Select One</option>
    <?php
    $table_data = 'wp_wbp_donors_data';
    $sql = 'SELECT unic_id FROM '.$table_data.' WHERE unic_id > 0';
    $all_exist_id = $wpdb->get_results($sql, ARRAY_A);
    $max_id = 1000;
    foreach( $all_exist_id as $tmp_val ){
        $tmp_arr[$tmp_val['unic_id']] = $tmp_val['unic_id'];
        if ($tmp_val['unic_id'] > $max_id) $max_id = $tmp_val['unic_id'];
    }
    $all_exist_id = $tmp_arr;

//* ��_id
    for( $i = 0; $i < 10; $i++ ){
        $all_id_arr[$i] = $max_id+$i;
    }
//echo '<pre>'; print_r($all_id_arr); die;
    $tmp_count = 0;
    if( isset($data['unic_id']) && $data['unic_id'] > 0 ){
        echo '<option value="'.$data['unic_id'].'" selected="selected">'.$data['unic_id'].'</option>';
        $end_cicle = 3;
    }
    else{
        $end_cicle = 4;
    }
    foreach( $all_id_arr as $v ){
        if( $tmp_count > $end_cicle ){
            break;
        }

        if( !isset($all_exist_id[$v]) ){
            echo '<option value="'.$v.'">'.$v.'</option>';
            $tmp_count++;
        }else{
            continue;
        }
    }
    $tmp_count = 0;


/*if( isset($data['unic_id']) && $data['unic_id'] > 0 ){
        echo '<option value="'.$data['unic_id'].'" selected="selected">'.$data['unic_id'].'</option>';
        for( $i = 1; $i < 5; $i++ ){
                $next_id = $data['max_id'] + $i;
                echo '<option value="'.$next_id.'">'.$next_id.'</option>';
        }
}else{
        for( $i = 1; $i < 6; $i++ ){
                $next_id = ($data['max_id'] > 0 ? $data['max_id'] : 1110) + $i;
                echo '<option value="'.$next_id.'">'.$next_id.'</option>';
        }
    }//*/
    ?>
</select></div>
<p class="tcl_p w160">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; or Other:</p>
<input class="tcl_input right10" style="width: 120px;" type="text" name="recept[unic_id_other]" id="unic_id_other" pattern="[0-9]{1,10}" maxlength="10"  onkeypress="return isNumberKey(event)"  />
<input class="btn_checkid" id="btn_body_prep_checkid" type="button" value="Check ID" name="checkid" onClick='checkdast($("#unic_id_other").val())' />

</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

<!--line--><div class="tc_line">
<p class="tcl_p w160">Estimated Weight:</p>
<input class="tcl_input w35 right10" type="text" name="recept[estim_weight]" pattern="[0-9]{1,3}" maxlength="3"
value="<?php echo isset($data['reception']['estim_weight']) ? $data['reception']['estim_weight'] : ''; ?>" />
<p class="tcl_p3">lbs.</p>
</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

<!--line-->
<div class="tc_line">
    <p class="tcl_p w160">Preparation:</p>
    <div class="select140">

        <select id="prep_select" name="recept[preparation][select]">
            <option value="Fresh"<?php echo isset($data['reception']['preparation']['select']) && $data['reception']['preparation']['select'] == 'Fresh' ? ' selected="selected"' : ''; ?>>Fresh</option>
            <option value="Embalmed"<?php echo isset($data['reception']['preparation']['select']) && $data['reception']['preparation']['select'] == 'Embalmed' ? ' selected="selected"' : ''; ?>>Embalmed</option>
            <option value="Frozen"<?php echo isset($data['reception']['preparation']['select']) && $data['reception']['preparation']['select'] == 'Frozen' ? ' selected="selected"' : ''; ?>>Frozen</option>
        </select>

    </div>
    <p class="tcl_p w60">&nbsp;</p>
    <div class="tc_date">
        <span>
            <input class="tab_input w105 customDate" type="text" name="recept[preparation][prep_date]"   id='prep_date' onChange="isValidDate(this.value,'Wrong Date of Second Preparation');"
                       value="<?php echo isset($data['reception']['preparation']['prep_date']) ?
                           $data['reception']['preparation']['prep_date'] : ''; ?>" placeholder="Date">
        </span>
    </div>
    <p class="tcl_p w60">&nbsp;</p>
    <p class="tcl_p w105">Pacemaker:</p>
    <div class="select80">
        <select  name="recept[body_cond][pacemaker]">
            <option value="">Select One</option>
            <option value="No"<?php echo isset($data['reception']['body_cond']['pacemaker']) && $data['reception']['body_cond']['pacemaker'] == 'No' ?
                ' selected="selected"' : ''; ?>>No</option>
            <option value="Yes"<?php echo isset($data['reception']['body_cond']['pacemaker']) && $data['reception']['body_cond']['pacemaker'] == 'Yes' ?
                ' selected="selected"' : ''; ?>>Yes</option>
        </select></div>
</div><div class="clear"></div>

<script>
function checkdast(a) {


        var formValues = {};
        formValues['unicid']=a;
      if (a) {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/wp-content/themes/stanford-WBP/checkunicid.php",
                type: "POST",
                data : formValues,
                success: function(dat)
                {
                    alert(dat);
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });

          } else {

          alert('Please enter ID Number');

          }




}
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

</script>



<!--separator-->
<div class="tc_sep_hight">
  <div class="tc_sep_hight_left"></div>
  <div class="tc_sep_hight_center">Body conditions</div>
  <div class="tc_sep_hight_right"></div>
</div><div class="clear"></div>


<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

<!--line--><div class="tc_line2">
<p class="tcl_p w160">Remarks:</p>
<textarea class="textarea1" name="recept[body_cond][remarks]"><?php echo isset($data['reception']['body_cond']['remarks']) ? $data['reception']['body_cond']['remarks'] : ''; ?></textarea>
</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<!--line--><div class="tc_line">
<p class="tcl_p w160">Body Location:</p>
<div class="select270">
  <select  name="recept[body_cond][body_loc]" >
    <option value="">Select One</option>
    <option value="Bio Skills Refrigerator"<?php echo isset($data['reception']['body_cond']['body_loc']) && $data['reception']['body_cond']['body_loc'] == 'Bio Skills Refrigerator' ? ' selected="selected"' : ''; ?>>Bio Skills Refrigerator</option>
    <option value="Body Holding"<?php echo isset($data['reception']['body_cond']['body_loc']) && $data['reception']['body_cond']['body_loc'] == 'Body Holding' ? ' selected="selected"' : ''; ?>>Body Holding</option>
    <option value="Body Holding Freezer"<?php echo isset($data['reception']['body_cond']['body_loc']) && $data['reception']['body_cond']['body_loc'] == 'Body Holding Freezer' ? ' selected="selected"' : ''; ?>>Body Holding Freezer</option>
    <option value="Loading Dock Refrigerator"<?php echo isset($data['reception']['body_cond']['body_loc']) && $data['reception']['body_cond']['body_loc'] == 'Loading Dock Refrigerator' ? ' selected="selected"' : ''; ?>>Loading Dock Refrigerator</option>
    <option value="Embalming"<?php echo isset($data['reception']['body_cond']['body_loc']) && $data['reception']['body_cond']['body_loc'] == 'Embalming' ?  ' selected="selected"' : ''; ?>>Embalming</option>


</select></div>
</div><div class="clear"></div>

<!--line--><div class="tc_line">
<p class="tcl_p w160">Slot Number:</p>
<input class="tcl_input w50" type="text" name="recept[body_cond][slot_numb]"  pattern="[0-9]{1,4}" maxlength="4"
value="<?php echo is_array($data['reception']) && $data['reception']['body_cond']['slot_numb'] ? $data['reception']['body_cond']['slot_numb'] : ''; ?>" />
</div><div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>



<div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<div class="clear"></div>
<?php
if ($data['preparation_file']) {
?>
<div class="tc_line">
                        <p class="tcl_p">Existing File:</p>
    <input type="hidden" name="preparation_file_exist" value="<?php echo $data['preparation_file']; ?>" />
<p class="tcl_p3"><a target=_blank href='<?php echo WP_HOME; ?>/upload/<?php echo $data['preparation_file']; ?>' ><?php echo $data['preparation_file']; ?></a></p>
                    </div>

<?php
}
?>



                    <div class="tc_line">
                        <p class="tcl_p">Upload File:</p>
<input class="tcl_input" type="file" name="preparation_file" id="preparation_file" />
                    </div>



<div class="tc_submit">
    <input type="hidden" name="donor_id" value="<?php echo ( $data['id'] ? $data['id'] : '' ); ?>" />
    <!--input type="hidden" name="body_status" value="reception" /-->
    <input type="hidden" name="form_recept" value="recept" />

    <a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>>
        <input type="button" value="cancel" name="" />
    </a>
    <!-- <input type="submit" value="save and store" name="save_store"  /> -->
    <!-- <input type="submit" value="save and hold" name="save_hold" /> -->
    <input id="btn_body_prep_save" type="submit" value="save" name="save_hold" />
    <input id="btn_body_prep_finish" type="submit" value="finish" name="save_hld" />
</div>

</form></div>
</div>
</dd>

<!-- script to save the data on the body prep form -->
<script type="text/javascript">
    // function to save the information to the database

    function saveBodyPrepFormFinish() {
        // get the form values
        // get all the inputs into an array.

        var $inputr = $('#register_form :input');
        var regNotFilled = false;
        $inputr.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    regNotFilled = true;
                }
            }
        });

        if (regNotFilled) {
            $("#register_donor_tab").trigger('click');
            $("#register_form").validate();
            return false;
        }

        else {

        var $inputs = $('#body_prep_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#body_prep_form');
        var formValues = new FormData(form[0]);
        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }

            if ($(this).attr('type') == "checkbox") {
                if ($(this).is(':checked')) {
//                   formValues[this.name] = $(this).val();
                }
            }
            else {
//                formValues[this.name] = $(this).val();
            }
        });

        if (requiredNotFilled) {
            $('#body_prep_form').validate();
            return false;
        }

        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
             window.location.replace("<?php bloginfo('url'); ?>/database");
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
      }
    }









    function saveBodyPrepForm() {
        // get the form values
        // get all the inputs into an array.
        var $inputs = $('#body_prep_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#body_prep_form');
        var formValues = new FormData(form[0]);
        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }

            if ($(this).attr('type') == "checkbox") {
                if ($(this).is(':checked')) {
//                   formValues[this.name] = $(this).val();
                }
            }
            else {
//                formValues[this.name] = $(this).val();
            }
        });

        if (requiredNotFilled) {
            $('#body_prep_form').validate();
            return false;
        }

        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
                    // get the donor data from the save page
                    if(jQuery.parseJSON(dat).existing == true) {
                    alert("Entered ID Number is already used. Please choose another ID.");
                    $('#body_prep_tab').trigger('click');
                    return false;
                    }
                    var donorArray = jQuery.parseJSON(dat).donor_data;

                    // update the global variables
                    globalDataArray = donorArray;
                    changeInForm = false;

                    $('#donor_name').attr("href", "<?php bloginfo('url'); ?>/info/?donor_id="+donorArray['id']+"&body_status="+donorArray['body_status']);
                    var display_status = donorArray['body_status'];
                    if (display_status == "reception") display_status="In Storage";
                    else if (display_status == "usage") display_status="In Use";
                    $('#donor_body_status').text("- " + display_status);
                    alert("Information successfully saved!");
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
    }

    // Set onclick method on the save button
    $('#btn_body_prep_save').click(saveBodyPrepForm);
    $('#btn_body_prep_finish').click(saveBodyPrepFormFinish);
</script>


<!-- BODY USAGE TAB -->
<dt id="body_usage_tab" class="tabs_dt<?php echo ( (isset($_POST['form_usage']) || ($next_status == 'usage')) ? ' selected' : '' ); ?>">Body usage</dt>
<dd class="tabs_dd<?php echo ( (isset($_POST['form_usage']) || ($next_status == 'usage')) ? ' selected' : '' ); ?>">

<div class="tab-content">
<div class="tc6">
  <span id="usage_form">
     <form id="body_usage_form" onsubmit="return false;">
        <!--separator-->
        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
        <div class="tc_sep_hight">
            <div class="tc_sep_hight_left"></div>
            <div class="tc_sep_hight_center">Preparation</div>
            <div class="tc_tc_sep_hight_right"></div>
        </div><div class="clear"></div>

        <div class="tc_line">
            <input type="hidden" name="usages[preparation]"
                value="<?php echo ( isset($data['reception']['preparation']['select']) && $data['reception']['preparation']['select'] ?
                ucwords($data['reception']['preparation']['select']) : '' ); ?>" />
        <p class="tcl_p5"><?php echo ( isset($data['reception']['preparation']['select']) && $data['reception']['preparation']['select'] ?
            ucwords($data['reception']['preparation']['select']) : '' ); ?></p>
        </div><div class="clear"></div>
    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

    <!--div class="tc_line">
                <div class="check_left_padding">
                        <input type="radio" name="usages[preparation]"
                                value="Fresh Body"<?php //echo (!isset($data['usages']['preparation']) || $data['usages']['preparation'] != 'Emblambed' ? ' checked="checked"' : ''); ?> />
                </div>
                <p class="tcl_p5">Fresh Body</p>
    </div><div class="clear"></div>
    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

    <div class="tc_line">
                <div class="check_left_padding">
                        <input type="radio" name="usages[preparation]"
                                value="Emblambed"<?php //echo isset($data['usages']['preparation']) && $data['usages']['preparation'] == 'Emblambed' ? ' checked="checked"' : ''; ?> />
                </div>
                <p class="tcl_p5">Emblambed</p>
    </div><div class="clear"></div>
    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div-->

    <!--separator-->
    <div class="tc_sep_hight">
        <div class="tc_sep_hight_left"></div>
        <div class="tc_sep_hight_center">Primary uses</div>
        <div class="tc_tc_sep_hight_right"></div>
    </div><div class="clear"></div>

    <!--line-->
    <div class="tc_line2">
        <p class="tcl_p w160">Internal:</p>
        <p class="tcl_p3">&nbsp;</p>
    </div><div class="clear"></div>
    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

        <!--line-->
 <!--       <div class="tc_line">
            <div class="check_left_padding">
                <input type="checkbox" name="usages[primary][internal][surg_71q]" value="Surg 71q: Procedural Anatomy"
                <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                    isset($data['usages']['primary']['internal']['surg_71q']) && $data['usages']['primary']['internal']['surg_71q'] ) ?
                    ' checked="checked"' : ''); ?> />
                </div>
                <p class="tcl_p5">Surg 71q: Procedural Anatomy</p>
            </div><div class="clear"></div>
            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
     //-->
        <!--line-->

            <!--line-->
            <div class="tc_line">
                <div class="check_left_padding">
                    <input type="checkbox" name="usages[primary][internal][surg_203a]" value="Surg 203A: Fall QTR"
                    <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                        isset($data['usages']['primary']['internal']['surg_203a']) && $data['usages']['primary']['internal']['surg_203a'] ) ?
                        ' checked="checked"' : ''); ?> />
                    </div>
                    <p class="tcl_p5">Surg 203A: Fall QTR</p>
                </div><div class="clear"></div>
                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                <!--line-->
                <div class="tc_line">
                    <div class="check_left_padding">
                        <input type="checkbox" name="usages[primary][internal][surg_203b]" value="Surg 203B: Spring QTR"
                        <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                            isset($data['usages']['primary']['internal']['surg_203b']) && $data['usages']['primary']['internal']['surg_203b'] ) ?
                            ' checked="checked"' : ''); ?> />
                        </div>
                        <p class="tcl_p5">Surg 203B: Spring QTR</p>
                    </div><div class="clear"></div>
                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


        <div class="tc_line">
            <div class="check_left_padding">
                <input type="checkbox" name="usages[primary][internal][surg_101]" value="Surg 101: Winter QTR"
                <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                    isset($data['usages']['primary']['internal']['surg_101']) && $data['usages']['primary']['internal']['surg_101'] ) ?
                    ' checked="checked"' : ''); ?> />
                </div>
                <p class="tcl_p5">Surg 101: Winter QTR</p>
            </div><div class="clear"></div>
            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>





         <!--line-->
         <div class="tc_line">
             <div class="check_left_padding">
                 <input type="checkbox" name="usages[primary][internal][resident][checkbox]" value="Resident Course" class="with_note"
                 <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                     isset($data['usages']['primary'][internal][resident]['checkbox']) &&
                     $data['usages']['primary'][internal][resident]['checkbox'] ) ? ' checked="checked"' : ''); ?> />
                 </div>
                 <p class="tcl_p5" style="width: 250px;">Resident Course:</p>
                 <input class="tcl_input" type="text" name="usages[primary][internal][resident][name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50"
                 value="<?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                     isset($data['usages']['primary'][internal][resident]['name']) &&
                     $data['usages']['primary'][internal][resident]['name'] ) ?
                     $data['usages']['primary'][internal][resident]['name']: ''); ?>" />
                 <p class="tcl_p5">&nbsp;Date:&nbsp;</p>
      <div class="tc_date">
        <span>
            <input style="margin-right:20px; width:100px;" class="tcl_input customDate" type="text" name="usages[primary][internal][resident][date]"  id='date_of_resident' onChange="isValidDate(this.value,'Wrong Date of Course');"
            value="<?php echo isset($data['usages']['primary'][internal][resident]['date']) ? $data['usages']['primary'][internal][resident]['date'] : ''; ?>" />
        </span>
            <input type="hidden"  id='date_of_resident_sec' 
            value="<?php echo isset($data['usages']['primary'][internal][resident]['date']) ? $data['usages']['primary'][internal][resident]['date'] : ''; ?>" />
      </div>
             <!--line-->
             <div class="tc_line2">
                 <p class="tcl_p w160">Notes:</p>
                 <textarea class="textarea1" name="usages[primary][internal][resident][note]"><?php echo isset($data['usages']['primary']['internal']['resident']['note']) ? $data['usages']['primary']['internal']['resident']['note'] : ''; ?></textarea>
             </div>
         </div>
         <div class="clear"></div>
         <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


         <!--line-->
         <div class="tc_line">
             <div class="check_left_padding">
                 <input type="checkbox" name="usages[primary][internal][other_med_student][checkbox]" value="Other Med Student Course" class="with_note"
                     <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                         isset($data['usages']['primary'][internal][other_med_student]['checkbox']) &&
                         $data['usages']['primary'][internal][other_med_student]['checkbox'] ) ? ' checked="checked"' : ''); ?> />
             </div>
             <p class="tcl_p5"  style="width: 250px;">Other Med Student Course:&nbsp;</p>
             <input class="tcl_input" type="text" name="usages[primary][internal][other_med_student][name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50"
                    value="<?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                        isset($data['usages']['primary'][internal][other_med_student]['name']) &&
                        $data['usages']['primary'][internal][other_med_student]['name'] ) ?
                        $data['usages']['primary'][internal][other_med_student]['name']: ''); ?>" />
             <p class="tcl_p5">&nbsp;Date:&nbsp;</p>
             <div class="tc_date">
        <span>
            <input style="margin-right:20px; width:100px;" class="tcl_input customDate" type="text" name="usages[primary][internal][other_med_student][date]"  id='date_of_other_med_student' onChange="isValidDate(this.value,'Wrong Date of Course');"
                   value="<?php echo isset($data['usages']['primary'][internal][other_med_student]['date']) ? $data['usages']['primary'][internal][other_med_student]['date'] : ''; ?>" />
        </span>
                 <input type="hidden"  id='date_of_other_med_student_sec'
                        value="<?php echo isset($data['usages']['primary'][internal][other_med_student]['date']) ? $data['usages']['primary'][internal][other_med_student]['date'] : ''; ?>" />
             </div>
             <!--line-->
             <div class="tc_line2">
                 <p class="tcl_p w160">Notes:</p>
                 <textarea class="textarea1" name="usages[primary][internal][other_med_student][note]"><?php echo isset($data['usages']['primary']['internal']['other_med_student']['note']) ? $data['usages']['primary']['internal']['other_med_student']['note'] : ''; ?></textarea>
             </div>
         </div><div class="clear"></div>
         <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

         <!--line-->
         <div class="tc_line">
             <div class="check_left_padding">
                 <input type="checkbox" name="usages[primary][internal][prosection][checkbox]" value="Prosection Usage" class="with_note"
                 <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                     isset($data['usages']['primary'][internal][prosection]['checkbox']) &&
                     $data['usages']['primary'][internal][prosection]['checkbox'] ) ? ' checked="checked"' : ''); ?> />
                 </div>
                 <p class="tcl_p5"  style="width: 250px;">Prosection Usage:&nbsp;</p>
                 <input class="tcl_input" type="text" name="usages[primary][internal][prosection][name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50"
                 value="<?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                     isset($data['usages']['primary'][internal][prosection]['name']) &&
                     $data['usages']['primary'][internal][prosection]['name'] ) ?
                     $data['usages']['primary'][internal][prosection]['name']: ''); ?>" />
             <div class="clear"></div>
             <!--line-->
             <div class="tc_line2">
                 <p class="tcl_p w160">Notes:</p>
                 <textarea class="textarea1" name="usages[primary][internal][prosection][note]"><?php echo isset($data['usages']['primary']['internal']['prosection']['note']) ? $data['usages']['primary']['internal']['prosection']['note'] : ''; ?></textarea>
             </div>
         </div><div class="clear"></div>
         <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


         <!--line-->
         <div class="tc_line">
             <div class="check_left_padding">
                 <input type="checkbox" name="usages[primary][internal][other][checkbox]" value="Other" class="with_note"
                     <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                         isset($data['usages']['primary'][internal][other]['checkbox']) &&
                         $data['usages']['primary'][internal][other]['checkbox'] ) ? ' checked="checked"' : ''); ?> />
             </div>
             <p class="tcl_p5"  style="width: 250px;">Other:&nbsp;</p>
             <input class="tcl_input" type="text" name="usages[primary][internal][other][name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50"
                    value="<?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                        isset($data['usages']['primary'][internal][other]['name']) &&
                        $data['usages']['primary'][internal][other]['name'] ) ?
                        $data['usages']['primary'][internal][other]['name']: ''); ?>" />
             <p class="tcl_p5">&nbsp;Date:&nbsp;</p>
             <div class="tc_date">
        <span>
            <input style="margin-right:20px; width:100px;" class="tcl_input customDate" type="text" name="usages[primary][internal][other][date]"  id='date_of_other' onChange="isValidDate(this.value,'Wrong Date of Course');"
                   value="<?php echo isset($data['usages']['primary'][internal][other]['date']) ? $data['usages']['primary'][internal][other]['date'] : ''; ?>" />
        </span>
                 <input type="hidden"  id='date_of_other_sec'
                        value="<?php echo isset($data['usages']['primary'][internal][other]['date']) ? $data['usages']['primary'][internal][other]['date'] : ''; ?>" />
             </div>
             <!--line-->
             <div class="tc_line2">
                 <p class="tcl_p w160">Notes:</p>
                 <textarea class="textarea1" name="usages[primary][internal][other][note]"><?php echo isset($data['usages']['primary']['internal']['other']['note']) ? $data['usages']['primary']['internal']['other']['note'] : ''; ?></textarea>
             </div>
         </div><div class="clear"></div>
         <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

                    <!--line-->
<!--                    <div class="tc_line">
                        <div class="check_left_padding">
                            <input type="checkbox" name="usages[primary][internal][surg_254]" value="Surg 254: Operative Anatomy and Tehniques"
                            <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                                isset($data['usages']['primary']['internal']['surg_254']) && $data['usages']['primary']['internal']['surg_254'] ) ?
                                ' checked="checked"' : ''); ?> />
                            </div>
                            <p class="tcl_p5">Surg 254: Operative Anatomy and Tehniques</p>
                        </div><div class="clear"></div>
                        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
//-->
                        <!--line-->
<!--                        <div class="tc_line">
                            <div class="check_left_padding">
                                <input type="checkbox" name="usages[primary][internal][surg_296]" value="Surg 296: Individual Work"
                                <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                                    isset($data['usages']['primary']['internal']['surg_296']) && $data['usages']['primary']['internal']['surg_296'] ) ?
                                    ' checked="checked"' : ''); ?> />
                                </div>
                                <p class="tcl_p5">Surg 296: Individual Work</p>
                            </div><div class="clear"></div>
                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
//-->
                            <!--line-->
<!--                            <div class="tc_line">
                                <div class="check_left_padding">
                                    <input type="checkbox" name="usages[primary][internal][intern_prog]" value="Summer Intern Program"
                                    <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                                        isset($data['usages']['primary']['internal']['intern_prog']) && $data['usages']['primary']['internal']['intern_prog'] ) ?
                                        ' checked="checked"' : ''); ?> />
                                    </div>
                                    <p class="tcl_p5">Summer Intern Program</p>
                                </div><div class="clear"></div>
                                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
//-->
                                <!--line-->
                                <div class="tc_line2">
                                    <p class="tcl_p w160">External:</p>
                                    <p class="tcl_p3">&nbsp;</p>
                                </div><div class="clear"></div>
                                <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>




         <!--line-->
         <div class="tc_line">
             <div class="check_left_padding">
                 <input type="checkbox" name="usages[primary][external][outside_course][checkbox]" value="Outside Course" class="with_note"
                 <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                     isset($data['usages']['primary'][external][outside_course]['checkbox']) &&
                     $data['usages']['primary'][external][outside_course]['checkbox'] ) ? ' checked="checked"' : ''); ?> />
                 </div>
                 <p class="tcl_p5"  style="width: 250px;">Outside Course/BioSkills:&nbsp;</p>
                 <input class="tcl_input" type="text" name="usages[primary][external][outside_course][name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50"
                 value="<?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                     isset($data['usages']['primary'][external][outside_course]['name']) &&
                     $data['usages']['primary'][external][outside_course]['name'] ) ?
                     $data['usages']['primary'][external][outside_course]['name']: ''); ?>" />
                 <p class="tcl_p5">&nbsp;Date:&nbsp;</p>
      <div class="tc_date">
        <span>
            <input style="margin-right:20px; width:100px;" class="tcl_input customDate" type="text" name="usages[primary][external][outside_course][date]"  id='date_of_outside_course' onChange="isValidDate(this.value,'Wrong Date of Course');"
            value="<?php echo isset($data['usages']['primary'][external][outside_course]['date']) ? $data['usages']['primary'][external][outside_course]['date'] : ''; ?>" />
        </span>
            <input type="hidden"  id='date_of_outside_course_sec' 
            value="<?php echo isset($data['usages']['primary'][external][outside_course]['date']) ? $data['usages']['primary'][external][outside_course]['date'] : ''; ?>" />
      </div>
             <div class="tc_line2">
                 <p class="tcl_p w160">Notes:</p>
                 <textarea class="textarea1" name="usages[primary][external][outside_course][note]"><?php echo isset($data['usages']['primary']['external']['outside_course']['note']) ? $data['usages']['primary']['external']['outside_course']['note'] : ''; ?></textarea>
             </div>
         </div><div class="clear"></div>
         <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>



         <!--line-->
         <div class="tc_line">
             <div class="check_left_padding">
                 <input type="checkbox" name="usages[primary][external][other][checkbox]" value="Other" class="with_note"
                 <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                     isset($data['usages']['primary'][external][other]['checkbox']) &&
                     $data['usages']['primary'][external][other]['checkbox'] ) ? ' checked="checked"' : ''); ?> />
                 </div>
                 <p class="tcl_p5"  style="width: 250px;">Other:&nbsp;</p>
                 <input class="tcl_input" type="text" name="usages[primary][external][other][name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50"
                 value="<?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                     isset($data['usages']['primary'][external][other]['name']) &&
                     $data['usages']['primary'][external][other]['name'] ) ?
                     $data['usages']['primary'][external][other]['name']: ''); ?>" />
                 <p class="tcl_p5">&nbsp;Date:&nbsp;</p>
      <div class="tc_date">
        <span>
            <input style="margin-right:20px; width:100px;" class="tcl_input customDate" type="text" name="usages[primary][external][other][date]"  id='date_of_external_other' onChange="isValidDate(this.value,'Wrong Date of Course');"
            value="<?php echo isset($data['usages']['primary'][external][other]['date']) ? $data['usages']['primary'][external][other]['date'] : ''; ?>" />
        </span>
            <input type="hidden"  id='date_of_external_other_sec' 
            value="<?php echo isset($data['usages']['primary'][external][other]['date']) ? $data['usages']['primary'][external][other]['date'] : ''; ?>" />
      </div>
             <div class="tc_line2">
                 <p class="tcl_p w160">Notes:</p>
                 <textarea class="textarea1" name="usages[primary][external][other][note]"><?php echo isset($data['usages']['primary']['external']['other']['note']) ? $data['usages']['primary']['external']['other']['note'] : ''; ?></textarea>
             </div>
         </div><div class="clear"></div>
         <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>


                                <!--line-->
<!--                                <div class="tc_line2 bot10">
                                    <div><div class="check_left_padding">
                                        <input type="checkbox" name="usages[primary][external][outside_course][checkbox]" value="Outside Course"
                                        <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                                            isset($data['usages']['primary']['external']['outside_course']['checkbox']) &&
                                            $data['usages']['primary']['external']['outside_course']['checkbox'] ) ? ' checked="checked"' : ''); ?> />
                                        </div>
                                        <p class="tcl_p5">Outside Course</p>
                                    </div><div class="clear"></div>
                                    <div class="second_line">
                                        <p class="tcl_p w60">Name:</p>
                                        <input class="tcl_input" type="text" name="usages[primary][external][outside_course][name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50"
                                        value="<?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                                            isset($data['usages']['primary']['external']['outside_course']['name']) &&
                                            $data['usages']['primary']['external']['outside_course']['name'] ) ?
                                            $data['usages']['primary']['external']['outside_course']['name']: ''); ?>" />
                                        </div><div class="clear"></div>
                                    </div>
                                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
//-->
                                    <!--line-->
<!--                                    <div class="tc_line2 bot10">
                                        <div>
                                            <div class="check_left_padding">
                                                <input type="checkbox" name="usages[primary][external][resident_course][checkbox]" value="Resident Course"
                                                <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                                                    isset($data['usages']['primary']['external']['resident_course']['checkbox']) &&
                                                    $data['usages']['primary']['external']['resident_course']['checkbox'] ) ? ' checked="checked"' : ''); ?> />
                                                </div>
                                                <p class="tcl_p5">Resident Course</p>
                                            </div><div class="clear"></div>
                                            <div class="second_line">
                                                <p class="tcl_p w60">Name:</p>
                                                <input class="tcl_input" type="text" name="usages[primary][external][resident_course][name]" pattern="[a-zA-Z\s\-']{1,50}" maxlength="50"
                                                value="<?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                                                    isset($data['usages']['primary']['external']['resident_course']['name']) &&
                                                    $data['usages']['primary']['external']['resident_course']['name'] ) ?
                                                    $data['usages']['primary']['external']['resident_course']['name'] : ''); ?>" />
                                                </div><div class="clear"></div>
                                            </div>
                                            <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
//-->
                                            <!--line-->
<!--                                            <div class="tc_line">
                                                <div class="check_left_padding">
                                                    <input type="checkbox" name="usages[primary][external][other][checkbox]" value="Other"
                                                    <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                                                        isset($data['usages']['primary']['external']['other']['checkbox']) &&
                                                        $data['usages']['primary']['external']['other']['checkbox'] ) ? ' checked="checked"' : ''); ?> />
                                                    </div>
                                                    <p class="tcl_p5">Other</p>
                                                    <input class="tcl_input left20" type="text" name="usages[primary][external][other][name]" pattern="[a-zA-Z0-9\s\-\']{1,50}" maxlength="50"
                                                    value="<?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                                                        isset($data['usages']['primary']['external']['other']['name']) &&
                                                        $data['usages']['primary']['external']['other']['name'] ) ?
                                                        $data['usages']['primary']['external']['other']['name'] : ''); ?>" />
                                                    </div><div class="clear"></div>
                                                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
//-->
                                                    <!--line-->
<!--                                                    <div class="tc_line2">
                                                        <div class="check_left_padding">
                                                            <input type="checkbox" name="usages[primary][prosection_usage]" value="prosection_usage"
                                                            <?php echo ((is_array($data['usages']) && !empty($data['usages']) &&
                                                            isset($data['usages']['primary']['prosection_usage'])) ? ' checked="checked"' : ''); ?> />
                                                        </div>
                                                        <p class="tcl_p3">Prosection Usage</p>
                                                    </div><div class="clear"></div>
                                                    <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

//-->
         <div class="tc_line2">
             <p class="tcl_p w160">Another Courses</p>
             <p class="tcl_p3">&nbsp;</p>
         </div><div class="clear"></div>
         <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>

         <?php $additionalCourses = $data['usages']['primary']['additional'] ?>
         <?php if($additionalCourses): ?>
             <?php $i = 0; foreach($additionalCourses as $course): ?>
                 <div class="tc_line">
                     <div class="check_left_padding">
                         <input type="checkbox" name="usages[primary][additional][<?php echo $i ?>][checkbox]" <?php echo ($course['checkbox'])?'checked="checked"':''; ?> >
                     </div>
                     <p class="tcl_p5"  style="width: 250px;"><?php echo $course['title'] ?>:&nbsp;</p>
                     <input type="text" class="tcl_input" name="usages[primary][additional][<?php echo $i ?>][name]" value="<?php echo $course['name'] ?>">
                     <p class="tcl_p5">&nbsp;Date:&nbsp;</p>
                     <div class="tc_date">
                    <span>
                        <input style="margin-right:20px; width:100px;" class="tcl_input customDate" type="text" name="usages[primary][additional][<?php echo $i ?>][date]" onChange="isValidDate(this.value,'Wrong Date of Course');" value="<?php echo $course['date'] ?>"/>
                    </span>
                     </div>
                     <input type="hidden" name="usages[primary][additional][<?php echo $i ?>][title]" value="<?php echo $course['title'] ?>">
                 </div>
                 <?php $i++; endforeach;  ?>
         <?php endif; ?>

         <div id="additional-courses"></div>
         <div class="tc_line">
             <button id="add-course">Add Another Course</button>
         </div>
         <script>
             jQuery(document).ready(function($){
                 var startIndex =  <?php echo count($additionalCourses);?>;
                 $('#add-course').click(function(event){
                     event.preventDefault();
                     $('#additional-courses').append(
                         '<div class="tc_line">'+
                         '<div class="check_left_padding">'+
                         '<input type="checkbox" name="usages[primary][additional]['+startIndex+'][checkbox]">'+
                         '</div>'+
                         '<input type="text" style="width:200px; margin-right: 28px" class="tcl_input" name="usages[primary][additional]['+startIndex+'][title]">' +
                         '<input type="text" class="tcl_input" name="usages[primary][additional]['+startIndex+'][name]">' +
                         '<p class="tcl_p5">&nbsp;Date:&nbsp;</p>'+
                         '<div class="tc_date"><span>'+
                         '<input style="margin-right:20px; width:100px;" class="tcl_input customDate" type="text" name="usages[primary][additional]['+startIndex+'][date]"/>'+
                         '</span></div>'+

                         '</div>'
                     );
                     startIndex++;
                     initDatePickers();
                     $('input:checkbox').styler();
                 })
             })
         </script>

<div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<div class="clear"></div>
<?php
if ($data['usage_file']) {
?>
<div class="tc_line">
                        <p class="tcl_p">Existing File:</p>
    <input type="hidden" name="usage_file_exist" value="<?php echo $data['usage_file']; ?>" />
<p class="tcl_p3"><a target=_blank href='<?php echo WP_HOME; ?>/upload/<?php echo $data['usage_file']; ?>' ><?php echo $data['usage_file']; ?></a></p>
                    </div>

<?php
}
?>



                    <div class="tc_line">
                        <p class="tcl_p">Upload File:</p>
<input class="tcl_input" type="file" name="usage_file" id="usage_file" />
                    </div>



                                                    <div class="tc_submit">
                                                      <input type="hidden" name="donor_id" value="<?php echo ( $data['id'] ? $data['id'] : '' ); ?>" />
                                                      <!--input type="hidden" name="body_status" value="usage" /-->
                                                      <input type="hidden" name="form_usage" value="recept" />

                                                      <a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>>
                                                          <input type="button" value="cancel" name="" />
                                                      </a>
<input id="btn_body_usage_form" type="submit" value="save" name=""  />
<input id="btn_body_usage_finish" type="submit" value="finish" name=""  />
                                                  </div>
                                              </form>
                                          </span>
                                      </div>
                                  </div>
                              </dd>

<!-- script to save the data on the body usage form -->
<script type="text/javascript">
    // function to save the information to the database

    function saveBodyUsageFormFinish() {

        var $inputr = $('#register_form :input');
        var regNotFilled = false;
        $inputr.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    regNotFilled = true;
                }
            }
        });

        if (regNotFilled) {
            $("#register_donor_tab").trigger('click');
            $("#register_form").validate();
            return false;
        }

        else {



        if( $('#usage_form').find('input[type="checkbox"][checked="checked"]').length < 1 ){
            alert('Please select any position or cancel saving');
            return false;
        }

        // get the form values
        // get all the inputs into an array.
        var $inputs = $('#body_usage_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#body_usage_form');
        var formValues = new FormData(form[0]);
        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }

            if ($(this).attr('type') == "checkbox") {
                if ($(this).is(':checked')) {
//                   formValues[this.name] = $(this).val();
                }
            }
            else {
//                formValues[this.name] = $(this).val();
            }
        });

        if (requiredNotFilled) {
            $('#body_usage_form').validate();
            return false;
        }
        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
             window.location.replace("<?php bloginfo('url'); ?>/database");

                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
      }
    }







    function saveBodyUsageForm() {
        if( $('#usage_form').find('input[type="checkbox"][checked="checked"]').length < 1 ){
            alert('Please select any position or cancel saving');
            return false;
        }

        // get the form values
        // get all the inputs into an array.
        var $inputs = $('#body_usage_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#body_usage_form');
        var formValues = new FormData(form[0]);
        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }

            if ($(this).attr('type') == "checkbox") {
                if ($(this).is(':checked')) {
//                   formValues[this.name] = $(this).val();
                }
            }
            else {
//                formValues[this.name] = $(this).val();
            }
        });

        if (requiredNotFilled) {
            $('#body_usage_form').validate();
            return false;
        }
        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
                    // get the donor data from the save page
                    var donorArray = jQuery.parseJSON(dat).donor_data;

                    // update the global variables
                    globalDataArray = donorArray;
                    changeInForm = false;

                    $('#donor_name').attr("href", "<?php bloginfo('url'); ?>/info/?donor_id="+donorArray['id']+"&body_status="+donorArray['body_status']);
                    var display_status = donorArray['body_status'];
                    if (display_status == "reception") display_status="In Storage";
                    else if (display_status == "usage") display_status="In Use";
                    $('#donor_body_status').text("- " + display_status);
                    alert("Information successfully saved!");
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
    }

    // Set onclick method on the save button
    $('#btn_body_usage_form').click(saveBodyUsageForm);
    $('#btn_body_usage_finish').click(saveBodyUsageFormFinish);
</script>


<!-- FINAL DISPOSITION TAB -->
<dt id="final_disposition_tab" class="tabs_dt<?php echo ( (isset($_POST['form_final']) || ($next_status == 'departed')) ? ' selected' : '' ); ?>">Final disposition</dt>
<dd class="tabs_dd<?php echo ( (isset($_POST['form_final']) || ($next_status == 'departed')) ? ' selected' : '' ); ?>">

<div class="tab-content">
<div class="tc6">
    <form id="final_disposition_form" onsubmit="return false;">
        <!--line-->
        <div class="tc_line">
            <div class="check_left_padding">
                <input id="cremated" type="checkbox" name="final[cremated]" value="cremated" <?php echo isset($data['final']['cremated']) && $data['final']['cremated'] ? 'checked="checked"' : ''; ?> />
            </div>
            <p class="tcl_p5">Cremated</p>
        </div><div class="clear"></div>

        <!--line-->
        <div class="tc_line">
            <p class="tcl_p w60">Date:</p>
            <span>
                <input style="margin-right:20px;" class="tcl_input customDate" type="text" name="final[date]"   id='date_of_final' onChange="isValidDate(this.value,'Wrong Final Date');"
                value="<?php echo isset($data['final']['date']) ? $data['final']['date'] : ''; ?>" />
            </span>
                <input type="hidden"  id='date_of_final_sec' 
                value="<?php echo isset($data['final']['date']) ? $data['final']['date'] : ''; ?>" />

        </div><div class="clear"></div>





        <!--line-->
        <div class="tc_line left50">
            <div class="check_left_padding">
                <input type="radio" name="final[cremains_destination]" value="To Family" id="family_radio"
                <?php echo isset($data['final']['cremains_destination']) && $data['final']['cremains_destination'] == 'To Family' ? 'checked="checked"' : ''; ?> />
            </div>
            <p class="tcl_p5">To Family</p>
        </div><div class="clear"></div>

        <!--line-->
        <div class="tc_line left50">
            <div class="check_left_padding">
                <input type="radio" name="final[cremains_destination]" value="Interred" id="interred_radio"
                <?php echo isset($data['final']['cremains_destination']) && $data['final']['cremains_destination'] == 'Interred' ? 'checked="checked"' : ''; ?> />
            </div>
            <p class="tcl_p5">Interred</p>
        </div><div class="clear"></div>
        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>




        <!--line-->
        <div class="tc_line2">
            <p class="tcl_p w160">Notes:</p>
            <textarea class="textarea1" name="final[notes]"><?php echo isset($data['final']['notes']) ? $data['final']['notes'] : ''; ?></textarea>
        </div><div class="clear"></div>
        <div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>




<div class="clear"></div>
<div class="tc_sep"><div class="tc_sep_left"></div><div class="tc_sep_right"></div></div><div class="clear"></div>
<div class="clear"></div>
<?php
if ($data['final_file']) {
?>
<div class="tc_line">
                        <p class="tcl_p">Existing File:</p>
    <input type="hidden" name="final_file_exist" value="<?php echo $data['final_file']; ?>" />
<p class="tcl_p3"><a target=_blank href='<?php echo WP_HOME; ?>/upload/<?php echo $data['final_file']; ?>' ><?php echo $data['final_file']; ?></a></p>
                    </div>

<?php
}
?>



                    <div class="tc_line">
                        <p class="tcl_p">Upload File:</p>
<input class="tcl_input" type="file" name="final_file" id="final_file" />
                    </div>




        <div class="tc_submit">
            <input type="hidden" name="donor_id" value="<?php echo ( $data['id'] ? $data['id'] : '' ); ?>" />
            <!--input type="hidden" name="body_status" value="departed" /-->
            <input type="hidden" name="form_final" value="final" />

            <a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>>
                <input type="button" value="cancel" name="" />
            </a>
            <input id="btn_final_disposition_save" type="submit" value="save" name="" />
            <input id="btn_final_disposition_finish" type="submit" value="finish" name="" />
        </div>


<!-- script to save the data on the final disposition form -->
<script type="text/javascript">
    // function to save the information to the database

    function saveFinalDispositionFormFinish() {
        // get the form values
        // get all the inputs into an array.

        var $inputr = $('#register_form :input');
        var regNotFilled = false;
        $inputr.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    regNotFilled = true;
                }
            }
        });

        if (regNotFilled) {
            $("#register_donor_tab").trigger('click');
            $("#register_form").validate();
            return false;
        }

        else {


        var $inputs = $('#final_disposition_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#final_disposition_form');
        var formValues = new FormData(form[0]);
        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }

            if ($(this).attr('type') == "checkbox" || $(this).attr('type') == "radio") {
                if ($(this).is(':checked')) {
//                   formValues[this.name] = $(this).val();
                }
            }
            else {
//                formValues[this.name] = $(this).val();
            }
        });

        if (requiredNotFilled) {
            $('#final_disposition_form').validate();
            return false;
        }
        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
             window.location.replace("<?php bloginfo('url'); ?>/database");
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
      }
    }










    function saveFinalDispositionForm() {
        // get the form values
        // get all the inputs into an array.
        var $inputs = $('#final_disposition_form :input');

        // get an associative array of just the values.
        var requiredNotFilled = false;
        var form = $('#final_disposition_form');
        var formValues = new FormData(form[0]);
        $inputs.each(function() {
            if ($(this).prop('required')) {
                if ($(this).val().length == 0) {
                    requiredNotFilled = true;
                }
            }

            if ($(this).attr('type') == "checkbox" || $(this).attr('type') == "radio") {
                if ($(this).is(':checked')) {
//                   formValues[this.name] = $(this).val();
                }
            }
            else {
//                formValues[this.name] = $(this).val();
            }
        });

        if (requiredNotFilled) {
            $('#final_disposition_form').validate();
            return false;
        }
        // make the ajax call
        else {
            $.ajax({
                url : "<?php bloginfo('url'); ?>/save?donor_id=<?php echo $data['id']?>",
                type: "POST",
                data : formValues,
		cache: false,
                processData: false,
                contentType: false,

                success: function(dat)
                {
                    // get the donor data from the save page
                    var donorArray = jQuery.parseJSON(dat).donor_data;

                    // update the global variables
                    globalDataArray = donorArray;
                    changeInForm = false;

                    $('#donor_name').attr("href", "<?php bloginfo('url'); ?>/info/?donor_id="+donorArray['id']+"&body_status="+donorArray['body_status']);
                    var display_status = donorArray['body_status'];
                    if (display_status == "reception") display_status="In Storage";
                    else if (display_status == "usage") display_status="In Use";
                    $('#donor_body_status').text("- " + display_status);
                    alert("Information successfully saved!");
                },
                error: function (jqXHR, status, errorThrown)
                {
                    console.log("ERROR");
                }
            });
        }
    }

    // Set onclick method on the save button
    $('#btn_final_disposition_save').click(saveFinalDispositionForm);
    $('#btn_final_disposition_finish').click(saveFinalDispositionFormFinish);
</script>


<script>
    $(function(){
    /*  $('#btn_final_disposition_save').on('click', function(){
                if( $('#cremated').attr('checked') != 'checked' ){
                        alert('Please select "Cremated" or cancel saving');
                        return false;
                }
        });
    */
    });

////////////////////////////////////////

    $('input[class*="group"]').on('click', function(){
                //alert('zxzx');
       $(this).select();
    });

    function moveMainInp(id){
        var main_inp = $(id);
        var next_all = $(main_inp).nextAll('[class*="group"]');
        var next_inp = $(next_all)[0];
        var block_len = $(next_all).length;
        //console.log();
        if( block_len > 0 && $(next_inp).val().length > 0 ){
                $(next_all[block_len - 1]).after(main_inp);

                $(main_inp).removeClass('w35');
                $(main_inp).addClass('w45');
                $(main_inp).removeAttr('maxlength');
                $(main_inp).attr('maxlength', 4);
                $(main_inp).removeAttr('pattern');
                $(main_inp).attr('pattern', '[0-9]{1,6}');
        }
    }

    moveMainInp("#qwqw14");
    moveMainInp("#qwqw24");
    moveMainInp("#qwqw34");
    moveMainInp("#qwqw44");
    moveMainInp("#qwqw54");


    $('input[class*="group"]').on('keyup', function(){

        if( $(this).attr('id') != 'qwqw14' && $(this).attr('id') != 'qwqw24' && $(this).attr('id') != 'qwqw34' && $(this).attr('id') != 'qwqw44' && $(this).attr('id') != 'qwqw54' ){
            return false;
        }

        var current_value = $(this).val();
        var text_length = $(this).val().length;
        var this_maxlen = $(this).attr('maxlength');

        var next = $(this).nextAll('[class*="group"]')[0];
        if( next ){
            var next_maxlen = $(next).attr('maxlength');
            if( next_maxlen > 3 ){
                $(this).removeClass('w35');
                $(this).addClass('w45');
                $(this).removeAttr('maxlength');
                $(this).attr('maxlength', 4);
                $(this).removeAttr('pattern');
                $(this).attr('pattern', '[0-9]{1,6}');
            }

            if( text_length >= this_maxlen ){
                this.parentNode.insertBefore(next, this);
                var val = ($(next).val() || '').replace('/[^0-9]+/g', '');
                val = val.substring(0, next_maxlen);
                if(text_length > this_maxlen){
                    val = current_value.substring(this_maxlen, text_length);
                }

                current_value = current_value.substring(0, this_maxlen);
                text_length = current_value.length;
                $(next).val(current_value);
                $(this).val(val);
                $(this).select();
            }
        }
    });


    // Code to detect changes in the form and trigger events
    var changeInForm = false;

    // Helper function
    function setChangeToTrue() {
        changeInForm = true;
    }

    // Set trigger events on change in form input fields
    $('input').each(function() {
 var clsName = $(this).attr('class');
 if (clsName === undefined ) {
 $(this).change(setChangeToTrue); 
 } else if (clsName.search('customDate') == -1) {
 $(this).change(setChangeToTrue); 
}
});
    $('select').each(function() { $(this).change(setChangeToTrue); });
    $('textarea').each(function() { $(this).change(setChangeToTrue); });

    // Save prompts on changing tabs

    $('.tabs_dt, .tabs2_dt, .header_left, .nav_back, .nav_home').on('mousedown', function(){
    $('input').each(function() { $(this).blur(); });
    $('textarea').each(function() { $(this).blur(); });
        if( !$(this).hasClass('selected') ){
            if ($('#register_donor_tab').hasClass('selected')) {

if ( $("#date_of_birth_sec").val() != $("#date_of_birth").val()) {
changeInForm = true;
var asb = $("#date_of_birth").val();
$("#date_of_birth_sec").val(asb); 
}

                if (changeInForm) {
                    // give a save prompt to the user

                        saveRegisterDonorForm();
                }
                $(this).trigger('click');
                changeInForm = false;
            }
            else if ($('#accept_donation_tab').hasClass('selected')) {
                if ($('#donor_information_tab2').hasClass('selected')) {

if ( $("#date_of_death_sec").val() != $("#date_of_death").val()) {
changeInForm = true;
var asb = $("#date_of_death").val();
$("#date_of_death_sec").val(asb); 
}

                    if (changeInForm) {
                        // give a save prompt to the user
                            saveDonorInformationForm();
                    }
                    $(this).trigger('click');
                    changeInForm = false;
                }
                else if ($('#contacts_information_tab2').hasClass('selected')) {
                    if (changeInForm) {
                        // give a save prompt to the user
                            saveContactsInformationForm();
                    }
                    $(this).trigger('click');
                    changeInForm = false;
                }
                else if ($('#body_condition_tab2').hasClass('selected')) {
                    if (changeInForm) {
                        // give a save prompt to the user
                            saveBodyConditionForm();
                    }
                    $(this).trigger('click');
                    changeInForm = false;
                }
            }
            else if ($('#body_prep_tab').hasClass('selected')) {

if ( $("#date_of_reception_sec").val() != $("#date_of_reception").val()) {
changeInForm = true;
var asb = $("#date_of_reception").val();
$("#date_of_reception_sec").val(asb); 
}

if ( $("#date_of_preparation_sec").val() != $("#date_of_preparation").val()) {
changeInForm = true;
var asb = $("#date_of_preparation").val();
$("#date_of_preparation_sec").val(asb); 
}

if ( $("#date_of_preparation2_sec").val() != $("#date_of_preparation2").val()) {
changeInForm = true;
var asb = $("#date_of_preparatio2n").val();
$("#date_of_preparation2_sec").val(asb); 
}


                if (changeInForm) {
                    // give a save prompt to the user
                        saveBodyPrepForm();
                }
                $(this).trigger('click');
                changeInForm = false;
            }
            else if ($('#body_usage_tab').hasClass('selected')) {
                if (changeInForm) {
                    // give a save prompt to the user
                        saveBodyUsageForm();
                }
                $(this).trigger('click');
                changeInForm = false;
            }
            else if ($('#final_disposition_tab').hasClass('selected')) {

if ( $("#date_of_final_sec").val() != $("#date_of_final").val()) {
changeInForm = true;
var asb = $("#date_of_final").val();
$("#date_of_final_sec").val(asb); 
}

                if (changeInForm) {
                    // give a save prompt to the user
                        saveFinalDispositionForm();
                }
                $(this).trigger('click');
                changeInForm = false;
            }
        }

    });
</script>

</form>
</div>
</div>
</dd>
</dl>
</div>
</div>


<script>
    jQuery(document).ready(function($){

        function prepareFormOnStatus(status){
            if(status == 'rejected'){
                $('#wrap_checkbox').show();
                $('#accept_note').text('Reason for Decline:');
            }else{
                $('#wrap_checkbox').hide();
                $('#accept_note').text('Note:');
            }
        }

        var statusSelect = $('#status_select');

        statusSelect.change( function(){
            prepareFormOnStatus($(this).val());
        });

        prepareFormOnStatus(statusSelect.val());

//        -------------------
        $('input.with_note').change(function(){
            toggleNote($(this));
        });

        function toggleNote(checkbox){
            var checked = checkbox.prop('checked');
            var note = checkbox.parents('.tc_line').find('.tc_line2');
            if(checked){
                checkbox.parents('.tc_line').height('auto');
                note.show();
            }else{
                note.hide();
            }
        }

        $('input.with_note').each(function(){toggleNote($(this))})
    })
</script>

<?php get_footer(); ?>
