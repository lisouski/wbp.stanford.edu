<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">-->
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<meta name="format-detection" content="telephone=no" />

<meta name="viewport" content="user-scalable=yes, initial-scale=1.0, maximum-scale=1.0, width=device-width">
<!--meta name="viewport" content="width=device-width" /-->

<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/ie.css" />
<![endif]-->
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/img/ficon.png" />
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.formstyler.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.formstyler.min.js'></script><script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.groupinputs.js'></script>
	
	<!--Autocomplete + calendar-->
	<link rel="stylesheet" href="<?php bloginfo('url'); ?>/themes/base/jquery.ui.all.css">
	<script src="<?php bloginfo('url'); ?>/ui/jquery.ui.core.js"></script>
	<script src="<?php bloginfo('url'); ?>/ui/jquery.ui.widget.js"></script>
	<script src="<?php bloginfo('url'); ?>/ui/jquery.ui.position.js"></script>
	<script src="<?php bloginfo('url'); ?>/ui/jquery.ui.menu.js"></script>
	<script src="<?php bloginfo('url'); ?>/ui/jquery.ui.autocomplete.js"></script>

  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/MonthPicker.css" />
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/MonthPicker.js'></script><script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.groupinputs.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.maskedinput.min.js'></script><script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.groupinputs.js'></script>


<title><?php bloginfo('name'); wp_title(); ?></title>
<?php
wp_enqueue_style('wbp_style');

//wp_enqueue_script('wbp_script_formstyler');
//wp_enqueue_script('wbp_script_formstyler_min');
wp_head();
?>
<script>
    /*console.log(navigator.userAgent);
    console.log(navigator.appName);
    console.log(navigator.appVersion);
    console.log([
			jQuery(window).width(),
			jQuery(window).height(),
			jQuery(document).scrollLeft(),
			jQuery(document).scrollTop()
		]);*/
</script>
</head>
<body class="b_database">
<div id="loading"></div>