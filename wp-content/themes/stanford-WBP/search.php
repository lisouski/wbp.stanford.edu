<?php get_header(); ?>

<?php get_sidebar(); ?>

	<div id="content">

		<H1>SEARCH.PHP</H1>

		<?php if (have_posts()) : ?>

			<?php while (have_posts()) : the_post(); ?>

				<div class="post" id="post-<?php the_ID(); ?>">

					<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

					<small><?php the_time('F jS, Y'); ?> | <?php the_author() ?></small>

					<div class="entry">

						<?php the_content('Читать далее &raquo'); ?>

					</div>

					<p class="postmetadata">
						<!--Теги: <?php the_tags(); ?><br />
						Категории: <?php get_the_category_list(', '); ?><br />
						Ссылка на редактирование: <?php edit_post_link('Редактировать'); ?><br />-->
						Комментарии: <?php comments_popup_link('Без комментариев &#187;', '1 комментарий &#187;', ' Комментариев: % &#187;'); ?>
					</p>

				</div>

			<?php endwhile; ?>

		<?php else : ?>

			<h2 align="center">Не найдено</h2>
			<p align="center">Извините, ничего не найдено.</p>

		<?php endif; ?>

	</div>

<?php get_footer(); ?>