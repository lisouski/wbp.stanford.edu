<?php
/*
 * Template Name: Export CSV
 */

$result = $_SESSION['result'];
$filename = "report_".$_SESSION['report_name'].".csv";

$fp = fopen('php://output', 'w');
if ($fp && $result) {
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename='.$filename);
    header('Pragma: no-cache');
    header('Expires: 0');

    $headers = $result[0];
    fputcsv($fp, array_keys($headers));
    foreach ($result as $row) {
        fputcsv($fp, array_values($row));
    }
    die;
}

?>