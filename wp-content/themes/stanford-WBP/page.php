<?php get_header(); ?>

<?php //get_sidebar(); ?>

	<div id="content">

		<H1>PAGE.PHP</H1>
    
		<?php if (have_posts()) : ?>

			<?php while (have_posts()) : the_post(); ?>

				<div class="post" id="post-<?php the_ID(); ?>">

					<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

					<!--<small><?php the_time('F jS, Y'); ?> | <?php the_author() ?></small>-->

					<div class="entry">

						<?php the_content('Читать далее &raquo'); ?>

					</div>

				</div>

			<?php endwhile; ?>
<?php
			//comments_template();
?>
		<?php else : ?>

			<h2 align="center">Не найдено</h2>
			<p align="center">Извините, ничего не найдено.</p>

		<?php endif; ?>

	</div>

<?php get_footer(); ?>