<?php
//phpinfo();die;
/**
 * Start session and registering $session_errors (for plugin order-form)
 */

if(!session_id()){
	session_start();
	$session_errors = "";
}else{
	$session_errors = "Filed to create session";
}
//*/

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

/**
 * WP Menu (require class for menu and wp-admin design settings)
 */
require_once('classes/WalkerChildNavMenu.class.php');
require_once('classes/Configuration.class.php');

/**
 * Functions for get current date
 */
/*
function wbpGetCurrentDate(){
	//echo date("Y:m:d"); //die;
	$year = date("Y");
	$month = date("m");
	$day = date("d");
}
wbpGetCurrentDate();
//*/

/**
 * Functions for get header block with logo, and other links
 */
function wbpGetBigHeaderBlock(){
?>
	<div class="logo_big"><a href="<?php bloginfo('url'); ?>"></a></div>
	<div class="l_title"><?php bloginfo('name'); ?></div>
<?php
}
 
function wbpGetSmallHeaderBlock(){
?>
	<div class="header">
	    <div class="header_left"><a href="<?php bloginfo('url'); ?>"></a></div>
	    <div class="header_right">
		<a name="header"><div class="right_bd"></div></a>
		<div class="header_nav">
		    <div class="nav_back">
			<a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>></a>
		    </div>
		    <div class="nav_home"><a href="<?php bloginfo('url'); ?>"></a></div>
		</div>
	    </div>
	    <div class="header_center"><?php echo strtoupper(get_bloginfo('name')); ?></div>
	</div><div class="clear"></div>
<?php
}
 
/**
 * Get Real User IP
 */
/*
function getRealIpAddr(){
  if( !empty($_SERVER['HTTP_CLIENT_IP']) ){
	  $ip = $_SERVER['HTTP_CLIENT_IP'];
  }
  elseif( !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ){
	  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  }else{
	  $ip = $_SERVER['REMOTE_ADDR'];
  }
  return $ip;
}
//*/

/**
 * Sidebar Registration
 */
/*
if(function_exists('register_sidebar')) {
	register_sidebar(
		array(
		'name'=>'Sidebar',
		'before_widget' => '',
		'after_widget' => '<hr>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
		)
	);
}
//*/

/**
 * Register Menu
 */
/*
// add_action( 'init', 'register_my_menu' );
if(function_exists('add_theme_support')){
    add_theme_support('menus');
}//*/

// registered top menu
if (function_exists('register_nav_menus')) {
	register_nav_menus(
		array(
			'TopMenu' => 'Top Menu',
			'BotMenu' => 'Bot Menu'
		)
	);
}
//*/

/**
 * delete name generator 
 */
// remove_action( 'wp_head', 'feed_links_extra', 3 ); 
// remove_action( 'wp_head', 'feed_links', 2 );
// remove_action( 'wp_head', 'rsd_link' );
// remove_action( 'wp_head', 'wlwmanifest_link' );
// remove_action( 'wp_head', 'index_rel_link' );
// remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); 
// remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
// remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
// remove_action( 'wp_head', 'pingback_link' );
remove_action( 'wp_head', 'wp_generator' );

/**
 * registered style 
 */

wp_register_style(
    'wbp_style',
    '/wp-content/themes/stanford-WBP/css/style.css',
    null,
    '1.0.1'
);
wp_register_style(
    'wbp_style_formstyler',
    '/wp-content/themes/stanford-WBP/css/jquery.formstyler.css',
    null,
    '1.0.0'
);//*/
wp_register_style(
    'wbp_style_formstyler2',
    '/wp-content/themes/stanford-WBP/css/jquery.formstyler2.css',
    null,
    '1.0.0'
);
wp_register_style(
    'wbp_style_formstyler3',
    '/wp-content/themes/stanford-WBP/css/jquery.formstyler3.css',
    null,
    '1.0.0'
);
//*/

/**
 * registered script 
 */
/*
wp_register_script(
    'wbp_script_formstyler',
    'http://anat-content.stanford.edu/WBP-DB/wp-content/themes/stanford-WBP/js/jquery.formstyler.js',
    array('jquery'), 	//depends
    '1.0.0', 			//version
    false 				//execute in the footer
);
//*/

/**
 * Use always recent jQuery version
 */
if( !is_admin()){
	//wp_deregister_script('jquery');
	//wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, '');
	//wp_register_script('jquery', ("http://code.jquery.com/jquery-latest.min.js"), false, '');
	//wp_enqueue_script('jquery');
}

/**
 * Redefine function get_form_search - remove inscription before search form
 * 
 * @author Alex Bobylev
 * @param string $form
 * @return string 
 */
/*
function custom_search($form){
	$form = '<form role="search" method="get" id="searchform" action="' . esc_url( home_url( '/' ) ) . '" >
		<div><label class="screen-reader-text" for="s">' . __('') . '</label>
		<input type="text" value="' . get_search_query() . '" name="s" id="s" />
		<input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
		</div>
		</form>';
	return $form;
}
add_filter('get_search_form', 'custom_search');
//*/

?>
