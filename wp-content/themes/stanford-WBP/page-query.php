<?php

/*
 * Template Name: Query
 */
get_header();

?>
<div class="wrapper3">
    <div class="query_h_bd3">
        <div style="height:73px">
            <div class="header_left"><a href="<?php bloginfo('url'); ?>"></a></div>
            <div class="header_right">
            <a name="header"><div class="right_bd"></div></a>
            <div class="header_nav">
                <div class="nav_back">
                <a <?php echo ( isset($_SERVER['HTTP_REFERER']) ? 'onclick="history.back(); return false;"' : 'href="'.get_bloginfo('url').'"'); ?>></a>
                </div>
                <div class="nav_home"><a href="<?php bloginfo('url'); ?>"></a></div>
            </div>
            </div>
            <div class="header_center"><?php echo strtoupper(get_bloginfo('name')); ?></div>
        </div><div class="clear"></div>

<?php
// Function to display the body status
function display_status($status) {
    if ($status == "registered") return "Registered";
    else if ($status == "withdrew") return "Withdrew";
    else if ($status == "accepted") return "Accepted";
    else if ($status == "rejected") return "Rejected";
    else if ($status == "reception") return "In Storage";
    else if ($status == "usage") return "In Use";
    else if ($status == "departed") return "Departed";
    else return "";
}

// Function to display the gender
function display_gender($gender) {
    if ($gender == "male") return "Male";
    else if ($gender == "female") return "Female";
    else return "Unknown";
}

function get_rows_bodies_in_fridge($total_results, $result) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;

    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;
        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['unic_id']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['reception']['body_cond']['body_loc']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['reception']['body_cond']['slot_numb']."</span></td>";
        $rows .= "</tr>";
    }
    $rows .= '</table>';
    return $rows;
}

function get_rows_cremated($total_results, $result) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;

    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;
        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['unic_id']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['final']['date']."</span></td>";
        $rows .= "</tr>";
    }
    $rows .= '</table>';
    return $rows;
}

function get_rows_embalmed($total_results, $result) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;
    $total = 0;
    
    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;
        $status = display_status($value['body_status']);
        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['unic_id']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$status."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['reception']['preparation']['total']."</span></td>";
        $rows .= "</tr>";
        $total += $value['reception']['preparation']['total'];
    }
    $rows .= "<tr class='query_table_row' style='margin-top:10px'>";
    $rows .= "<td class='query_table_block'><span class='query_text3'></span></td>";
    $rows .= "<td class='query_table_block'><span class='query_text3'>TOTAL</span></td>";
    $rows .= "<td class='query_table_block'><span class='query_text3'></span></td>";
    $rows .= "<td class='query_table_block'><span class='query_text3'></span></td>";
    $rows .= "<td class='query_table_block'><span class='query_text3'>".$total."</span></td>";
    $rows .= "</tr>";
    $rows .= '</table>';
    return $rows;
}

function get_rows_withdrew($total_results, $result) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;

    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;
        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['date_registration']."</span></td>";
        $rows .= "</tr>";
    }
    $rows .= '</table>';
    return $rows;
}

function get_rows_registered($total_results, $result) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;

    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;
        $status = display_status($value['body_status']);
        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$status."</span></td>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['date_registration']."</span></td>";
        $rows .= "</tr>";
    }
    $rows .= '</table>';
    return $rows;
}

function get_rows_custom($total_results, $result, $customRows) {
    $rows = '';
    $i = 0;
    // $start = ($page-1)*$limit + 1;
    // $end = $page * $limit;
    // if ($end > $total_results) $end = $total_results;

    foreach ($result as $key => $value) {
        $i++;
        // if ($i < $start) continue; 
        // if ($i > $end) break;

        $rows .= "<tr class='query_table_row' style='text-align:center'>";
        $rows .= "<td class='query_table_block'><span class='query_text2'>".$i."</span></td>";

        foreach ($customRows as $index => $column) {
            if ($column == 'name') {
                $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['body_last_name']." ".$value['body_first_name']."</span></td>";
            }
            else if ($column == 'body_status') {
                $status = display_status($value['body_status']);
                $rows .= "<td class='query_table_block'><span class='query_text2'>".$status."</span></td>";
            }
            else if ($column == 'gender') {
                $gender = display_gender($value['gender']);
                $rows .= "<td class='query_table_block'><span class='query_text2'>".$gender."</span></td>";
            }
            else if ($column == 'phone') {
                $rows .= "<td class='query_table_block'><span class='query_text2'>"."(".$value['address']['first_phone'].")".$value['address']['sec_phone']."-".$value['address']['third_phone']."</span></td>";
            }
            else if ($column == 'email') {
                $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['address']['email']."</span></td>";
            }
            else if ($column == 'preparation') {
                $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['reception']['preparation']['select']."</span></td>";
            }
            else if ($column == 'body_location') {
                $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['reception']['body_cond']['body_loc']."</span></td>";
            }
            else if ($column == 'date_of_cremation') {
                $rows .= "<td class='query_table_block'><span class='query_text2'>".$value['final']['date']."</span></td>";
            }
            else {
                $rows .= "<td class='query_table_block'><span class='query_text2'>".$value[$column]."</span></td>";
            }
        }        
        $rows .= "</tr>";
    }
    $rows .= '</table>';
    return $rows;
}

function custom_header_name($name) {
    switch ($name) {
        case 'unic_id':
            return 'ID';
        case 'date_registration':
            return 'Registration Date';
        case 'body_status':
            return 'Status';
        case 'gender':
            return 'Gender';
        case 'date_of_death':
            return 'Date of Death';
        case 'cause_of_death':
            return 'Cause of Death';
        default:
            return $name;
    }
}

global $wpdb;
$table_data = 'wp_wbp_donors_data';

$sql = 'SELECT';
// $limit = 200;

// if (isset($_GET['page_number']) && $_GET['page_number']) {
//     $page = $_GET['page_number'];
// }
// else $page = 1;

if (isset($_POST['report']) && $_POST['report']) {
    switch($_POST['report']) {
        // Report for the bodies currently in fridge
        case 'bodies_in_fridge':
            $sql .= ' body_last_name, body_first_name, unic_id, reception';
            $sql .= ' FROM '.$table_data;
            $sql .= ' WHERE body_status = "reception"';

            $result = $wpdb->get_results($sql, ARRAY_A);
            $total_results = count($result);
            // $num_pages = ceil($total_results/$limit);
            // if ($page > $num_pages || $page < 1) $page = 1;

            foreach ($result as $key => $value) {
                $result[$key]['reception'] = unserialize(stripslashes($result[$key]['reception']));
            }
            usort($result, function($a, $b) {
                return $a['reception']['body_cond']['body_loc']- $b['reception']['body_cond']['body_loc'];
            });

            // Form a new array to export as CSV format
            $export = array();
            foreach ($result as $key => $value) {
                $export[$key]['Last Name'] = $value['body_last_name'];
                $export[$key]['First Name'] = $value['body_first_name'];
                $export[$key]['ID'] = $value['unic_id'];
                $export[$key]['Body Location'] = $value['reception']['body_cond']['body_loc'];
                $export[$key]['Slot Number'] = $value['reception']['body_cond']['slot_numb'];
            }
            $_SESSION['result'] = $export;
            $_SESSION['report_name'] = "bodies_in_fridge";

            $title = '';
            $message = '';

            // $display_page = '<div style="text-align:center; margin:20px"><h2 style="margin:20px; font-family:"NewsGothicSemi"">Page '.$page.' of '.$num_pages.'</h2>';
            $display_page = '';

            // table title       
            $title = '<div style="text-align:center; margin-top:20px; margin-bottom:20px">'.
                        '<table cellspacing="0" width="100%">'.
                            '<thead class="query_title" style="height:50px; margin:20px 50px 20px 50px; text-align:center; font-size:14pt">'.
                            '<tr>'.
                            '<td class="query_table_header" width="10%"><span class="query_text1">NO.</span></td>'.
                            '<td class="query_table_header" width="30%"><span class="query_text1">NAME</span></td>'.
                            '<td class="query_table_header" width="10%"><span class="query_text1">ID</span></td>'.
                            '<td class="query_table_header" width="30%"><span class="query_text1">BODY LOCATION</span></td>'.
                            '<td class="query_table_header" width="20%"><span class="query_text1">SLOT NUMBER</span></td>'.
                            '</tr>'.
                        '</thead>';

            // Get the rows for that page
            // $rows = get_rows_bodies_in_fridge($page, $num_pages, $limit, $total_results, $result);
            $rows = get_rows_bodies_in_fridge($total_results, $result);
            
            if (count($result) == 0) {
                $message = "<h2 style='text-align:center; margin:30pt'>NO RECORDS FOUND FOR THE REQUIRED REPORT</h2>";
            }
            $html .= $display_page.$title.$rows.$message.'</div>';
            echo $html;

            $_SESSION['html'] = $html;
            // $next_page = $page + 1;
            // $previous_page = $page - 1;
            ?>

            <div align="center">
            <button class="btn_query_export" onclick="window.location='<?php bloginfo('url') ?>/export-csv'">Export As CSV</button>
            </div>

            <?php
            break;
        
        // Report for cremated bodies sorted by descending order of date
        case 'cremated':
            $sql .= ' body_last_name, body_first_name, unic_id, final';
            $sql .= ' FROM '.$table_data;
            $sql .= ' WHERE body_status = "departed"';

            $result = $wpdb->get_results($sql, ARRAY_A);
            $total_results = count($result);
            // $num_pages = ceil($total_results/$limit);
            // if ($page > $num_pages || $page < 1) $page = 1;

            foreach ($result as $key => $value) {
                $result[$key]['final'] = unserialize(stripslashes($result[$key]['final']));
            }
            usort($result, function($a, $b) {
                return strtotime($b['final']['date'])- strtotime($a['final']['date']);
            });
            
            // Form a new array to export as CSV format
            $export = array();
            foreach ($result as $key => $value) {
                $export[$key]['Last Name'] = $value['body_last_name'];
                $export[$key]['First Name'] = $value['body_first_name'];
                $export[$key]['ID'] = $value['unic_id'];
                $export[$key]['Final Disposition'] = $value['final']['date'];
            }
            $_SESSION['result'] = $export;
            $_SESSION['report_name'] = "cremated";

            $title = '';
            $message = '';

            // $display_page = '<div style="text-align:center; margin:20px"><h2 style="margin:20px; font-family:"NewsGothicSemi"">Page '.$page.' of '.$num_pages.'</h2>';
            $display_page = '';

            // table title       
            $title = '<div style="text-align:center; margin-top:20px; margin-bottom:20px">'.
                        '<table cellspacing="0" width="100%">'.
                            '<thead class="query_title" style="height:50px; margin:20px 50px 20px 50px; text-align:center; font-size:14pt">'.
                            '<tr>'.
                            '<td class="query_table_header" width="10%"><span class="query_text1">NO.</span></td>'.
                            '<td class="query_table_header" width="40%"><span class="query_text1">NAME</span></td>'.
                            '<td class="query_table_header" width="10%"><span class="query_text1">ID</span></td>'.
                            '<td class="query_table_header" width="40%"><span class="query_text1">DATE OF CREMATION</span></td>'.
                            '</tr>'.
                        '</thead>';

            // Get the rows for that page
            // $rows = get_rows_cremated($page, $num_pages, $limit, $total_results, $result);
            $rows = get_rows_cremated($total_results, $result);

            if (count($result) == 0) {
                $message = "<h2 style='text-align:center; margin:30pt'>NO RECORDS FOUND FOR THE REQUIRED REPORT</h2>";
            }
            $html .= $display_page.$title.$rows.$message.'</div>';
            echo $html;

            $_SESSION['html'] = $html;
            // $next_page = $page + 1;
            // $previous_page = $page - 1;
            ?>

            <div align="center">
            <button class="btn_query_export" onclick="window.location='<?php bloginfo('url') ?>/export-csv'">Export As CSV</button>
            </div>

            <?php
            break;

        // Report for data about embalmed bodies
        case 'embalmed':
            $sql .= ' body_last_name, body_first_name, unic_id, body_status, reception';
            $sql .= ' FROM '.$table_data;
            $sql .= ' WHERE reception != ""';

            $result = $wpdb->get_results($sql, ARRAY_A);
            $total_results = count($result);
            // $num_pages = ceil($total_results/$limit);
            // if ($page > $num_pages || $page < 1) $page = 1;

            foreach ($result as $key => $value) {
                $result[$key]['reception'] = unserialize(stripslashes($result[$key]['reception']));
            }
            usort($result, function($a, $b) {
                return $a['reception']['preparation']['total']- $b['reception']['preparation']['total'];
            });
            
            // Form a new array to export as CSV format
            $export = array();
            foreach ($result as $key => $value) {
                $export[$key]['Last Name'] = $value['body_last_name'];
                $export[$key]['First Name'] = $value['body_first_name'];
                $export[$key]['ID'] = $value['unic_id'];
                $export[$key]['Body Status'] = display_status($value['body_status']);
                $export[$key]['Embalming Liquid (Gal)'] = $value['reception']['preparation']['total'];
            }
            $_SESSION['result'] = $export;
            $_SESSION['report_name'] = "embalmed";
            
            $title = '';
            $message = '';

            // $display_page = '<div style="text-align:center; margin:20px"><h2 style="margin:20px; font-family:"NewsGothicSemi"">Page '.$page.' of '.$num_pages.'</h2>';
            $display_page = '';

            // table title       
            $title = '<div style="text-align:center; margin-top:20px; margin-bottom:20px">'.
                        '<table cellspacing="0" width="100%">'.
                            '<thead class="query_title" style="height:50px; margin:20px 50px 20px 50px; text-align:center; font-size:14pt">'.
                            '<tr>'.
                            '<td class="query_table_header" width="10%"><span class="query_text1">NO.</span></td>'.
                            '<td class="query_table_header" width="30%"><span class="query_text1">NAME</span></td>'.
                            '<td class="query_table_header" width="10%"><span class="query_text1">ID</span></td>'.
                            '<td class="query_table_header" width="15%"><span class="query_text1">STATUS</span></td>'.
                            '<td class="query_table_header" width="35%"><span class="query_text1">EMBALMING LIQUID (GAL)</span></td>'.
                            '</tr>'.
                        '</thead>';

            // Get the rows for that page
            // $rows = get_rows_embalmed($page, $num_pages, $limit, $total_results, $result);
            $rows = get_rows_embalmed($total_results, $result);

            if (count($result) == 0) {
                $message = "<h2 style='text-align:center; margin:30pt'>NO RECORDS FOUND FOR THE REQUIRED REPORT</h2>";
            }
            $html .= $display_page.$title.$rows.$message.'</div>';
            echo $html;

            $_SESSION['html'] = $html;
            // $next_page = $page + 1;
            // $previous_page = $page - 1;
            ?>

            <div align="center">
            <button class="btn_query_export" onclick="window.location='<?php bloginfo('url') ?>/export-csv'">Export As CSV</button>
            </div>

            <?php
            break;

        // Report for data about drop outs
        case 'withdrew':
            $sql .= ' body_last_name, body_first_name, date_registration';
            $sql .= ' FROM '.$table_data;
            $sql .= ' WHERE body_status = "withdrew"';

            $result = $wpdb->get_results($sql, ARRAY_A);
            $total_results = count($result);
            // $num_pages = ceil($total_results/$limit);
            // if ($page > $num_pages || $page < 1) $page = 1;

            usort($result, function($a, $b) {
                return strtotime($b['date_registration'])- strtotime($a['date_registration']);
            });
            
            // Form a new array to export as CSV format
            $export = array();
            foreach ($result as $key => $value) {
                $export[$key]['Last Name'] = $value['body_last_name'];
                $export[$key]['First Name'] = $value['body_first_name'];
                $export[$key]['Registration Date'] = $value['date_registration'];
            }
            $_SESSION['result'] = $export;
            $_SESSION['report_name'] = "withdrew";
            
            $title = '';
            $message = '';

            // $display_page = '<div style="text-align:center; margin:20px"><h2 style="margin:20px; font-family:"NewsGothicSemi"">Page '.$page.' of '.$num_pages.'</h2>';
            $display_page = '';

            // table title       
            $title = '<div style="text-align:center; margin-top:20px; margin-bottom:20px">'.
                        '<table cellspacing="0" width="100%">'.
                            '<thead class="query_title" style="height:50px; margin:20px 50px 20px 50px; text-align:center; font-size:14pt">'.
                            '<tr>'.
                            '<td class="query_table_header" width="10%"><span class="query_text1">NO.</span></td>'.
                            '<td class="query_table_header" width="40%"><span class="query_text1">NAME</span></td>'.
                            '<td class="query_table_header" width="50%"><span class="query_text1">TIME OF REGISTRATION</span></td>'.
                            '</tr>'.
                        '</thead>';

            // Get the rows for that page
            // $rows = get_rows_withdrew($page, $num_pages, $limit, $total_results, $result);
            $rows = get_rows_withdrew($total_results, $result);

            if (count($result) == 0) {
                $message = "<h2 style='text-align:center; margin:30pt'>NO RECORDS FOUND FOR THE REQUIRED REPORT</h2>";
            }
            $html .= $display_page.$title.$rows.$message.'</div>';
            echo $html;

            $_SESSION['html'] = $html;
            // $next_page = $page + 1;
            // $previous_page = $page - 1;
            ?>

            <div align="center">
            <button class="btn_query_export" onclick="window.location='<?php bloginfo('url') ?>/export-csv'">Export As CSV</button>
            </div>

            <?php
            break;

        // Report for data about registrations
        case 'registered':
            $sql .= ' body_last_name, body_first_name, body_status, date_registration';
            $sql .= ' FROM '.$table_data;
            $sql .= ' WHERE body_status != "withdrew"';    

            $result = $wpdb->get_results($sql, ARRAY_A);
            $total_results = count($result);
            // $num_pages = ceil($total_results/$limit);
            // if ($page > $num_pages || $page < 1) $page = 1;

            usort($result, function($a, $b) {
                return strtotime($b['date_registration'])- strtotime($a['date_registration']);
            });

            // Form a new array to export as CSV format
            $export = array();
            foreach ($result as $key => $value) {
                $export[$key]['Last Name'] = $value['body_last_name'];
                $export[$key]['First Name'] = $value['body_first_name'];
                $export[$key]['Registration Date'] = $value['date_registration'];
                $export[$key]['Body Status'] = display_status($value['body_status']);
            }
            $_SESSION['result'] = $export;
            $_SESSION['report_name'] = "registered";

            $title = '';
            $rows = '';
            $message = '';

            // $display_page = '<div style="text-align:center; margin:20px"><h2 style="margin:20px; font-family:"NewsGothicSemi"">Page '.$page.' of '.$num_pages.'</h2>';
            $display_page = '';

            // table title       
            $title = '<div style="text-align:center; margin-top:20px; margin-bottom:20px">'.
                        '<table cellspacing="0" width="100%">'.
                            '<thead class="query_title" style="height:50px; margin:20px 50px 20px 50px; text-align:center; font-size:14pt">'.
                            '<tr>'.
                            '<td class="query_table_header" width="10%"><span class="query_text1">NO.</span></td>'.
                            '<td class="query_table_header" width="30%"><span class="query_text1">NAME</span></td>'.
                            '<td class="query_table_header" width="20%"><span class="query_text1">BODY STATUS</span></td>'.
                            '<td class="query_table_header" width="40%"><span class="query_text1">TIME OF REGISTRATION</span></td>'.
                            '</tr>'.
                        '</thead>';

            // Get the rows for that page
            // $rows = get_rows_registered($page, $num_pages, $limit, $total_results, $result);
            $rows = get_rows_registered($total_results, $result);

            if (count($result) == 0) {
                $message = "<h2 style='text-align:center; margin:30pt'>NO RECORDS FOUND FOR THE REQUIRED REPORT</h2>";
            }
            $html .= $display_page.$title.$rows.$message.'</div>';
            echo $html;

            $_SESSION['html'] = $html;
            // $next_page = $page + 1;
            // $previous_page = $page - 1;
            ?>

            <div align="center">
            <button class="btn_query_export" onclick="window.location='<?php bloginfo('url') ?>/export-csv'">Export As CSV</button>
            </div>

            <?php
            break;
        
        // Custom report
        case 'custom':
            $headerRow = '<td class="query_table_header"><span class="query_text1">No.</span></td>';
            $customRows = array();
            $exists = false;
            if (isset($_POST['custom_name']) && $_POST['custom_name']) {
                if ($exists) $sql .= ',';
                $sql .= ' body_last_name, body_first_name';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Name</span></td>';
                $customRows[] = 'name';
                $exists = true;
            }
            if (isset($_POST['custom_date_registration']) && $_POST['custom_date_registration']) {
                if ($exists) $sql .= ',';
                $sql .= ' date_registration';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Registration Date</span></td>';
                $customRows[] = 'date_registration';
                $exists = true;
            }
            if (isset($_POST['custom_body_status']) && $_POST['custom_body_status']) {
                if ($exists) $sql .= ',';
                $sql .= ' body_status';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Status</span></td>';
                $customRows[] = 'body_status';
                $exists = true;
            }
            if (isset($_POST['custom_unic_id']) && $_POST['custom_unic_id']) {
                if ($exists) $sql .= ',';
                $sql .= ' unic_id';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">ID</span></td>';
                $customRows[] = 'unic_id';
                $exists = true;
            }
            if (isset($_POST['custom_gender']) && $_POST['custom_gender']) {
                if ($exists) $sql .= ',';
                $sql .= ' gender';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Gender</span></td>';
                $customRows[] = 'gender';
                $exists = true;
            }
            if (isset($_POST['custom_phone']) && $_POST['custom_phone']) {
                if ($exists) $sql .= ',';
                $sql .= ' address';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Phone</span></td>';
                $customRows[] = 'phone';
                $exists = true;
            }
            if (isset($_POST['custom_email']) && $_POST['custom_email']) {
                if ($exists) $sql .= ',';
                $sql .= ' address';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Email</span></td>';
                $customRows[] = 'email';
                $exists = true;
            }
            if (isset($_POST['custom_date_of_death']) && $_POST['custom_date_of_death']) {
                if ($exists) $sql .= ',';
                $sql .= ' date_of_death';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Date of Death</span></td>';
                $customRows[] = 'date_of_death';
                $exists = true;
            }
            if (isset($_POST['custom_cause_of_death']) && $_POST['custom_cause_of_death']) {
                if ($exists) $sql .= ',';
                $sql .= ' cause_of_death';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Cause of Date</span></td>';
                $customRows[] = 'cause_of_death';
                $exists = true;
            }
            if (isset($_POST['custom_preparation']) && $_POST['custom_preparation']) {
                if ($exists) $sql .= ',';
                $sql .= ' reception';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Preparation</span></td>';
                $customRows[] = 'preparation';
                $exists = true;
            }
            if (isset($_POST['custom_body_location']) && $_POST['custom_body_location']) {
                if ($exists) $sql .= ',';
                $sql .= ' reception';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Body Location</span></td>';
                $customRows[] = 'body_location';
                $exists = true;
            }
            if (isset($_POST['custom_date_of_cremation']) && $_POST['custom_date_of_cremation']) {
                if ($exists) $sql .= ',';
                $sql .= ' final';
                $headerRow .= '<td class="query_table_header"><span class="query_text1">Date of Cremation</span></td>';
                $customRows[] = 'date_of_cremation';
                $exists = true;
            }

            $sql .= ' FROM '.$table_data;

            $filter = false;
            if (isset($_POST['filter_body_status']) && $_POST['filter_body_status']){
                $filter_body_status = $_POST['filter_body_status'];
                if ($filter_body_status != 'all') {
                    $sql .= ' WHERE body_status="'.$filter_body_status.'"';
                    $filter = true;
                }
            }

            if (isset($_POST['filter_gender']) && $_POST['filter_gender']){
                $filter_gender = $_POST['filter_gender'];
                if ($filter_gender != 'all') {
                    if ($filter) {
                        $sql .= ' AND gender="'.$filter_gender.'"';
                    }
                    else {
                        $sql .= ' WHERE gender="'.$filter_gender.'"';
                    }
                    $filter = true;
                }
            }

            $result = $wpdb->get_results($sql, ARRAY_A);
            $total_results = count($result);
            // $num_pages = ceil($total_results/$limit);
            // if ($page > $num_pages || $page < 1) $page = 1;

            // Form a new array to export as CSV format
            $export = array();
            $address_array = false;
            $preparation_array = false;
            foreach ($result as $key => $value) {
                foreach ($customRows as $hkey => $hvalue) {
                    if ($hvalue == 'name') {
                        $export[$key]['Last Name'] = $value['body_last_name'];
                        $export[$key]['First Name'] = $value['body_first_name'];
                    }
                    else if ($hvalue == 'body_status') {
                        $export[$key]['Body Status'] = display_status($value[$hvalue]);
                    }
                    else if ($hvalue == 'gender') {
                        $export[$key]['Gender'] = display_gender($value[$hvalue]);
                    }
                    else if ($hvalue == 'phone') {
                        $result[$key]['address'] = unserialize(stripslashes($result[$key]['address']));
                        $address_array = true;
                        $export[$key]['Phone'] = "(".$result[$key]['address']['first_phone'].")".$result[$key]['address']['sec_phone']."-".$result[$key]['address']['third_phone'];
                    }
                    else if ($hvalue == 'email') {
                        if (!$address_array) $result[$key]['address'] = unserialize(stripslashes($result[$key]['address']));
                        $export[$key]['Email'] = $result[$key]['address']['email'];
                    }
                    else if ($hvalue == 'preparation') {
                        $result[$key]['reception'] = unserialize(stripslashes($result[$key]['reception']));
                        $preparation_array = true;
                        $export[$key]['Preparation'] = $result[$key]['reception']['preparation']['select'];
                    }
                    else if ($hvalue == 'body_location') {
                        if (!$preparation_array) $result[$key]['reception'] = unserialize(stripslashes($result[$key]['reception']));
                        $export[$key]['Body Location'] = $result[$key]['reception']['body_cond']['body_loc'];
                    }
                    else if ($hvalue == 'date_of_cremation') {
                        $result[$key]['final'] = unserialize(stripslashes($result[$key]['final']));
                        $export[$key]['Date of Cremation'] = $result[$key]['final']['date'];
                    }
                    else {
                        $export[$key][custom_header_name($hvalue)] = $value[$hvalue];
                    }
                }
            }
            $_SESSION['result'] = $export;
            $_SESSION['report_name'] = "custom";

            $title = '';
            $rows = '';
            $message = '';

            // $display_page = '<div style="text-align:center; margin:20px"><h2 style="margin:20px; font-family:"NewsGothicSemi"">Page '.$page.' of '.$num_pages.'</h2>';
            $display_page = '';

            // table title       
            $title = '<div style="text-align:center; margin-top:20px; margin-bottom:20px">'.
                        '<table cellspacing="0" width="100%">'.
                            '<thead class="query_title" style="height:50px; margin:20px 50px 20px 50px; text-align:center; font-size:14pt">'.
                            '<tr>'.
                            $headerRow.
                            '</tr>'.
                        '</thead>';

            // Get the rows for that page
            // $rows = get_rows_custom($page, $num_pages, $limit, $total_results, $result, $customRows);
            $rows = get_rows_custom($total_results, $result, $customRows);

            if (count($result) == 0) {
                $message = "<h2 style='text-align:center; margin:30pt'>NO RECORDS FOUND FOR THE REQUIRED REPORT</h2>";
            }
            $html .= $display_page.$title.$rows.$message.'</div>';
            echo $html;

            $_SESSION['html'] = $html;
            // $next_page = $page + 1;
            // $previous_page = $page - 1;
            ?>

            <div align="center">
            <button class="btn_query_export" onclick="window.location='<?php bloginfo('url') ?>/export-csv'">Export As CSV</button>
            </div>

            <?php
            break;

        // Default case => No report
        default:
            echo "<h2 style='text-align:center; margin:30pt'>NO REPORT TO BE GENERATED</h2>";
    }
}
else {
    echo "<h2 style='text-align:center; margin:30pt'>NO REPORT TO BE GENERATED</h2>";
}
?>

    </div>
</div>

<?php
get_footer();
?>