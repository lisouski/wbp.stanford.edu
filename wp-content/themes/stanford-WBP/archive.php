<?php get_header(); ?>

<?php get_sidebar(); ?>

    <div id="content">

	<!--<H1>ARCHIVE.PHP</H1>-->
	<h2><?php $category = get_the_category(); echo $category[0]->cat_name; ?></h2>
<?php
if (have_posts()) {

	while (have_posts()) {
			
		the_post();
?>
		<div class="post" id="post-<?php the_ID(); ?>">
			<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
		</div>
<?php
	}
}else {
?>
		<h2 align="center">Не найдено</h2>
		<p align="center">Извините, ничего не найдено.</p>
<?php
}
?>

    </div>

<?php get_footer(); ?>